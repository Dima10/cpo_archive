<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer/src'),
    'yii\\jui\\' => array($vendorDir . '/yiisoft/yii2-jui'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii/src'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap/src'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'Zend\\Stdlib\\' => array($vendorDir . '/zendframework/zend-stdlib/src'),
    'Zend\\Escaper\\' => array($vendorDir . '/zendframework/zend-escaper/src'),
    'PhpOffice\\PhpWord\\' => array($vendorDir . '/phpoffice/phpword/src/PhpWord'),
    'PhpOffice\\Common\\' => array($vendorDir . '/phpoffice/common/src/Common'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/EmailValidator'),
);
