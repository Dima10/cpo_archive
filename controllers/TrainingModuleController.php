<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
                TrainingModule,
                TrainingModuleSearch
                };
use yii\web\{
    Controller,
    NotFoundHttpException,
    UploadedFile
};
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * TrainingModuleController implements the CRUD actions for TrainingModule model.
 */
class TrainingModuleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'actions' => ['download-a', 'download-af', 'download-b', 'download-bf']
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TrainingModule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingModuleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'module' => ArrayHelper::map(TrainingModule::find()->where(['trm_trm_id' => null])->all(), 'trm_id', 'trm_name'),
        ]);
    }

    /**
     * Displays a single TrainingModule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrainingModule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrainingModule();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'trp_dataA')) != null) {
                $model->trm_dataA_name = $file->name;
                $model->trm_dataA = file_get_contents($file->tempName);
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataB')) != null) {
                $model->trm_dataB_name = $file->name;
                $model->trm_dataB = file_get_contents($file->tempName);
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->trm_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trm_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'plan' => ArrayHelper::map(TrainingPlan::find()->all(), 'trp_id', 'trp_name'),
            ]);
        }
        */
    }

    /**
     * Creates a new child TrainingModule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateChild($pid)
    {
        $parent = TrainingModule::findOne($pid);

        $model = new TrainingModule();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'trp_dataA')) != null) {
                $model->trm_dataA_name = $file->name;
                $model->trm_dataA = file_get_contents($file->tempName);
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataB')) != null) {
                $model->trm_dataB_name = $file->name;
                $model->trm_dataB = file_get_contents($file->tempName);
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->trm_id]);
            } else {
                $model->trm_trm_id = $parent->trm_id;
                return $this->render('create_child', [
                    'model' => $model,
                    'parent' => $parent,
                ]);
            }
        } else {
            $model->trm_trm_id = $parent->trm_id;
            return $this->render('create_child', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }

    }

    /**
     * Updates an existing TrainingModule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
         $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'trm_dataA')) != null) {
                $model->trm_dataA_name = $file->name;
                $model->trm_dataA = file_get_contents($file->tempName);
            } else {
                $model->trm_dataA_name = $model->oldAttributes['trm_dataA_name'];
                $model->trm_dataA = $model->oldAttributes['trm_dataA'];
            }
            if (($file = UploadedFile::getInstance($model, 'trm_dataB')) != null) {
                $model->trm_dataB_name = $file->name;
                $model->trm_dataB = file_get_contents($file->tempName);
            } else {
                $model->trm_dataB_name = $model->oldAttributes['trm_dataB_name'];
                $model->trm_dataB = $model->oldAttributes['trm_dataB'];
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->trm_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trm_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'plan' => ArrayHelper::map(TrainingPlan::find()->all(), 'trp_id', 'trp_name'),
            ]);
        }
        */
    }

    /**
     * Deletes an existing TrainingModule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrainingModule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingModule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrainingModule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadA($id) { return $this->redirect(['download-af', 'id' => $id]); }
    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAf($id) { $model = TrainingModule::findOne($id); return Yii::$app->response->sendContentAsFile($model->trm_dataA, $model->trm_dataA_name); }
    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadB($id) { return $this->redirect(['download-bf', 'id' => $id]); }
    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadBf($id) { $model = TrainingModule::findOne($id); return Yii::$app->response->sendContentAsFile($model->trm_dataB, $model->trm_dataB_name); }


}
