<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
    Agreement,
    AgreementSearch,
    AgreementStatus,
    Entity,
    Company
};

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AgreementController implements the CRUD actions for Agreement model.
 */
class AgreementController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Agreement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgreementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'entity' => ArrayHelper::map(Entity::find()->select('ent_id, ent_name')->all(), 'ent_id', 'ent_name'),
            'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
        ]);
    }

    /**
     * Displays a single Agreement model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agreement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agreement();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agr_data')) != null) {
                $model->agr_fdata = $file->name;
                $model->agr_data = file_get_contents($file->tempName);
            }

            if (($file = UploadedFile::getInstance($model, 'agr_data_sign')) != null) {
                $model->agr_fdata_sign = $file->name;
                $model->agr_data_sign = file_get_contents($file->tempName);
            }


            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agr_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agr_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'addrbook' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            ]);
        }
        */
    }

    /**
     * Updates an existing Agreement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agr_data')) != null) {
                $model->agr_fdata = $file->name;
                $model->agr_data = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata = $model->oldAttributes['agr_fdata'];
                $model->agr_data = $model->oldAttributes['agr_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'agr_data_sign')) != null) {
                $model->agr_fdata_sign = $file->name;
                $model->agr_data_sign = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata_sign = $model->oldAttributes['agr_fdata_sign'];
                $model->agr_data_sign = $model->oldAttributes['agr_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agr_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agr_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'addrbook' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            ]);
        }
        */
    }

    /**
     * Deletes an existing Agreement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadF($id)
    {
        $model = Agreement::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->agr_data, $model->agr_fdata);

    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadSign($id)
    {
        return $this->redirect(['download-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadSignF($id)
    {
        $model = Agreement::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->agr_data_sign, $model->agr_fdata_sign);

    }



    /**
     * Finds the Agreement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agreement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agreement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
