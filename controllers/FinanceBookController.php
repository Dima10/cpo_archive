<?php

namespace app\controllers;

use app\models\AgreementAcc;
use Yii;
use app\models\FinanceBook;
use app\models\FinanceBookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IFinanceBookController implements the CRUD actions for FinanceBook model.
 */
class FinanceBookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all FinanceBook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceBook model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new FinanceBook model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FinanceBook();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fb_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FinanceBook model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fb_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FinanceBook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceBook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceBook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceBook::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Lists FinanceBook models.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatePay($id)
    {
        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(['FinanceBookSearch' => ['fb_aga_id' => $id]]);

        $account = AgreementAcc::findOne($id);

        return $this->render('update_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'account' => $account,
        ]);
    }

    /**
     * Updates fb_pay an existing FinanceBook model.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionUpdatePaySum($id, $value)
    {
        $model = $this->findModel($id);
        $model->fb_pay = $value;
        if ($model->save()) {
            return $model->fb_pay;
        } else {
            return "";
        }
    }

    /**
     * Updates fb_payment an existing FinanceBook model.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionUpdatePayment($id, $value)
    {
        $model = $this->findModel($id);
        $model->fb_payment = $value;
        if ($model->save()) {
            return $model->fb_payment;
        } else {
            return "";
        }
    }

    /**
     * Updates fb_comment an existing FinanceBook model.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionUpdateComment($id, $value)
    {
        $model = $this->findModel($id);
        $model->fb_comment = $value;
        if ($model->save()) {
            return $model->fb_comment;
        } else {
            return "";
        }
    }

    /**
     * Check sum pay.
     * @param string $value
     * @return mixed
     */
    public function actionPayCheckSum($value)
    {
        $value = floatval($value);
        return $this->renderAjax('_ajax_pay_check_sum', [
            //'searchModel' => $searchModel,
            'value' => $value,
        ]);
    }

    /**
     * Check sum pay.
     * @param integer $id
     * @param string $value
     * @param string $basis
     * @return mixed
     */
    public function actionPayCalculate($id, $value, $basis)
    {
        $line = FinanceBook::find()->where(['fb_aga_id' => $id])->all();
        $pay = floatval($value);

        foreach ($line as $key => $obj) {
            $obj->fb_payment = $basis;
            if (!isset($obj->fb_pay)) {

                // Сумма оплаты больше необходимого
                if (floatval($obj->fb_sum) <= $pay) {
                    $obj->fb_pay = $obj->fb_sum;
                }

                // Сумма оплаты меньше необходимого
                if (floatval($obj->fb_sum) > $pay) {
                    $obj->fb_pay = $pay;
                }

                if ((floatval($obj->fb_sum) > floatval($obj->fb_pay))) {

                    $p = $pay;
                    if ($obj->fb_tax > 0.001) {
                        $p_tax = round($pay/1.18 * 0.18, 2);
                    } else {
                        $p_tax = 0;
                    }

                    $fb = new FinanceBook();
                    $fb->fb_fb_id = $obj->fb_id;
                    $fb->fb_aca_id = $obj->fb_aca_id;
                    $fb->fb_svc_id = $obj->fb_svc_id;
                    $fb->fb_fbt_id = $obj->fb_fbt_id;
                    $fb->fb_ab_id = $obj->fb_ab_id;
                    $fb->fb_comp_id = $obj->fb_comp_id;
                    $fb->fb_sum = $obj->fb_sum - $p;
                    $fb->fb_tax = $obj->fb_tax - $p_tax;
                    $fb->fb_aga_id = $obj->fb_aga_id;
                    $fb->fb_acc_id = $obj->fb_acc_id;
                    $fb->fb_fbs_id = $obj->fb_fbs_id;
                    $fb->fb_date = $obj->fb_date;

                    $obj->fb_sum = $p;
                    $obj->fb_tax = $p_tax;

                    if ($fb->save()) {
                        $obj->save(false);
                    }

                }

                $pay -= floatval($obj->fb_pay);
            }
            $obj->save(false);
            if ($pay <= 0.001)
                break;
        }

        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(['FinanceBookSearch' => ['fb_aga_id' => $id]]);

        return $this->renderAjax('_grid_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


    }

}
