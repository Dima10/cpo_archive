<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\ApplRequestContent;
use Yii;
use app\models\ApplMain;
use app\models\ApplMainSearch;
use app\models\Company;
use app\models\Entity;
use app\models\Person;
use app\models\TrainingType;
use app\models\Svc;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ApplMainController implements the CRUD actions for ApplMain model.
 */
class ApplMainController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplMain models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplMainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApplMain model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplMain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ApplMain();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applm_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'ab' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'training_type' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            ]);
        }
    }

    /**
     * Updates an existing ApplMain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applm_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'ab' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'training_type' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            ]);
        }
    }

    /**
     * Deletes an existing ApplMain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplMain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplMain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplMain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
