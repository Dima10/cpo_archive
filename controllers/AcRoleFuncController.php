<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */


namespace app\controllers;

use app\models\AcRoleFunc;
use app\models\AcRoleFuncSearch;
use app\models\AcFunc;
use app\models\AcFuncSearch;
use app\models\AcRole;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AcRoleFuncController implements the CRUD actions for AcRoleFunc model.
 */
class AcRoleFuncController extends Controller
{
    private static $masterPage = 'ac-role/index';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all AcRoleFunc models.
     * @return mixed
     */
    public function actionIndex($acr_id)
    {
        if (!$role = AcRole::findOne($acr_id)) {
            return $this->redirect(\Yii::$app->urlManager->createUrl(self::$masterPage));
        }

        $searchModel = new AcFuncSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'acRole' => $role,
        ]);
    }

    /**
     * Displays a single AcRoleFunc model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AcRoleFunc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AcRoleFunc();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->acrf_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AcRoleFunc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->acrf_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AcRoleFunc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Add new AcRoleFunc model.
     * If creation is successful, the browser will be show the ajax 'add' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAdd($acr_id, $acf_id)
    {
        if ($model = AcRoleFunc::find()->where(['acrf_acr_id' => $acr_id, 'acrf_acf_id' => $acf_id, ])->one()) {
            return $this->renderAjax('add', [
                'save' => true,
                'model' => $model,
            ]);
        } else {
            $model = new AcRoleFunc();
            $model->acrf_acr_id = $acr_id;
            $model->acrf_acf_id = $acf_id;
            if ($model->save()) {
                return $this->renderAjax('add', [
                    'save' => true,
                    'model' => $model,
                ]);
            } else {
                return $this->renderAjax('add', [
                    'save' => false,
                    'model' => $model,
                ]);
            }
        }
    }


    /**
     * Delete exists AcRoleFunc model.
     * If delete is successful, the browser will be show the ajax 'add' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($acr_id, $acf_id)
    {
        if ($model = AcRoleFunc::find()->where(['acrf_acr_id' => $acr_id, 'acrf_acf_id' => $acf_id, ])->one()) {
            if ($model->delete()) {
                return $this->renderAjax('add', [
                    'save' => true,
                    'model' => null,
                ]);
            } else {
                return $this->renderAjax('add', [
                    'save' => false,
                    'model' => $model,
                ]);
            }
        } else {
            return $this->renderAjax('add', [
                'save' => false,
                'model' => null,
            ]);
        }
    }

    /**
     * Finds the AcRoleFunc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AcRoleFunc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AcRoleFunc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
