ALTER TABLE `payment_to` add
  `payt_create_user` varchar(64) DEFAULT NULL;
ALTER TABLE `payment_to` add
  `payt_create_time` datetime DEFAULT NULL;
ALTER TABLE `payment_to` add
  `payt_create_ip` varchar(64) DEFAULT NULL;
ALTER TABLE `payment_to` add
  `payt_update_user` varchar(64) DEFAULT NULL;
ALTER TABLE `payment_to` add
  `payt_update_time` datetime DEFAULT NULL;
ALTER TABLE `payment_to` add
  `payt_update_ip` varchar(64) DEFAULT NULL;

