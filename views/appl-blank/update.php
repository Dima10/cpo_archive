<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplBlank */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Appl Blank',
]) . $model->applblk_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Blanks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applblk_id, 'url' => ['view', 'id' => $model->applblk_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-blank-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
