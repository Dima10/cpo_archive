<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tasks_id') ?>

    <?= $form->field($model, 'tasks_name') ?>

    <?= $form->field($model, 'tasks_note') ?>

    <?= $form->field($model, 'tasks_create_user') ?>

    <?= $form->field($model, 'tasks_create_time') ?>

    <?php // echo $form->field($model, 'tasks_create_ip') ?>

    <?php // echo $form->field($model, 'tasks_update_user') ?>

    <?php // echo $form->field($model, 'tasks_update_time') ?>

    <?php // echo $form->field($model, 'tasks_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
