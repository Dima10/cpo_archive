<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskStatus */

$this->title = $model->tasks_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-status-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->tasks_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->tasks_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tasks_id',
            'tasks_name',
            'tasks_note:ntext',
            'tasks_create_user',
            'tasks_create_time',
            'tasks_create_ip',
            'tasks_update_user',
            'tasks_update_time',
            'tasks_update_ip',
        ],
    ]) ?>

</div>
