<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); ?>

    <?= $form->field($model, 'tag_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tag_comment')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'tag_create_user')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'tag_create_time')->textInput() ?>

    <?php //echo $form->field($model, 'tag_create_ip')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'tag_update_user')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'tag_update_time')->textInput() ?>

    <?php //echo $form->field($model, 'tag_update_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
