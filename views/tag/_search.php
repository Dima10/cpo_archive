<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TagSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tag_id') ?>

    <?= $form->field($model, 'tag_name') ?>

    <?= $form->field($model, 'tag_comment') ?>

    <?= $form->field($model, 'tag_create_user') ?>

    <?= $form->field($model, 'tag_create_time') ?>

    <?php // echo $form->field($model, 'tag_create_ip') ?>

    <?php // echo $form->field($model, 'tag_update_user') ?>

    <?php // echo $form->field($model, 'tag_update_time') ?>

    <?php // echo $form->field($model, 'tag_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
