<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingAnswerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-answer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tra_id') ?>

    <?= $form->field($model, 'tra_trq_id') ?>

    <?= $form->field($model, 'tra_answer') ?>

    <?= $form->field($model, 'tra_variant') ?>

    <?= $form->field($model, 'tra_create_user') ?>

    <?php // echo $form->field($model, 'tra_create_time') ?>

    <?php // echo $form->field($model, 'tra_create_ip') ?>

    <?php // echo $form->field($model, 'tra_update_user') ?>

    <?php // echo $form->field($model, 'tra_update_time') ?>

    <?php // echo $form->field($model, 'tra_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
