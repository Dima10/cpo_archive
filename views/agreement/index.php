<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $company array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Agreements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['site/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Agreement'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'entity' => $entity,
        'company' => $company,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
