<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $company array */
/* @var $agreement_status array */

Pjax::begin(); 
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'agr_id',
                'options' => ['width' => '100'],
            ],
            'agr_number',
            'agr_date',

            [
                'attribute' => 'agr_ast_id',
                'label' => Yii::t('app', 'Agr Ast ID'),
                'value' => function ($data) { return $data->agrAst->ast_name; },
                'filter' => $agreement_status,
            ],
            [
                'attribute' => 'agr_ab_id',
                'label' => Yii::t('app', 'Agr Ab ID'),
                'value' => function ($data) { return $data->agrAb->ab_name; },
                'filter' => $entity,
            ],
            [
                'attribute' => 'agr_comp_id',
                'label' => Yii::t('app', 'Agr Comp ID'),
                'value' => function ($data) { return $data->agrComp->comp_name; },
                'filter' => $company,
            ],

            'agr_sum',
            'agr_tax',

            [
                'attribute' => 'agr_fdata',
                'label' => Yii::t('app', 'Agr Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->agr_fdata, yii\helpers\Url::toRoute(['download', 'id' => $data->agr_id]), ['target' => '_blank']);
                            },
            ],

            [
                'attribute' => 'agr_fdata_sign',
                'label' => Yii::t('app', 'Agr Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->agr_fdata_sign, yii\helpers\Url::toRoute(['download-sign', 'id' => $data->agr_id]), ['target' => '_blank']);
                            },
            ],

            [
                'attribute' => 'agr_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
        ],
    ]);
Pjax::end();
