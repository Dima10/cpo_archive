<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonVisitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="person-visit-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'pv_id',
            'pv_prs_id',
            [
                'attribute' => 'prs_full_name',
                'value' => function ($data) {return $data->pvPrs->prs_full_name ?? '';},
                'label' => Yii::t('app', 'Prs Full Name'),
            ],
            [
                'attribute' => 'prs_connect_user',
                'value' => function ($data) {return $data->pvPrs->prs_connect_user ?? '';},
                'label' => Yii::t('app', 'Prs Connect User'),
            ],
            'pv_addr',
            'pv_time',
            'pv_create_time',
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
