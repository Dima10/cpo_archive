<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonVisit */

$this->title = Yii::t('app', 'Create Person Visit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person Visits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-visit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
