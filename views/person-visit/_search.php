<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonVisitSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-visit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pv_id') ?>

    <?= $form->field($model, 'pv_prs_id') ?>

    <?= $form->field($model, 'pv_addr') ?>

    <?= $form->field($model, 'pv_create_user') ?>

    <?= $form->field($model, 'pv_create_time') ?>

    <?php // echo $form->field($model, 'pv_update_user') ?>

    <?php // echo $form->field($model, 'pv_update_time') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
