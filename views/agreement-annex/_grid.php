<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAnnexSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $comp array */
/* @var $agreement_status array */

Pjax::begin();
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'agra_id',
                'options' => ['width' => '100'],
            ],

            'agra_number',
            'agra_date',

            [
                'attribute' => 'agra_ast_id',
                'label' => Yii::t('app', 'Agra Ast ID'),
                'value' => function ($data) { return $data->agraAst->ast_name; },
                'filter' => $agreement_status,
            ],

            [
                'attribute' => 'agra_ab_id',
                'label' => Yii::t('app', 'Agr Ab ID'),
                'value' => function ($data) { return $data->agraAgr->agrAb->entity->entEntt->entt_name_short.' '.$data->agraAgr->agrAb->ab_name; },
                'filter' => $entity,
            ],
            [
                'attribute' => 'agra_ab_id',
                'label' => Yii::t('app', 'Agr Comp ID'),
                'value' => function ($data) { return $data->agraAgr->agrAb->entity->entEntt->entt_name_short.' '.$data->agraAgr->agrAb->ab_name; },
                'filter' => $comp,
            ],

            [
                'attribute' => 'agra_fdata',
                'label' => Yii::t('app', 'Basis (agreement)'),
                'format' => 'raw',
                'value' => 'agraAgr.agr_number',
                    /*
                    function ($data) {
                    return Html::a($data->agra_fdata, yii\helpers\Url::toRoute(['download', 'id' => $data->agra_id]), ['target' => '_blank']);
                },
                    */
            ],


            [
                'attribute' => 'agra_fdata',
                'label' => Yii::t('app', 'Agra Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->agra_fdata, yii\helpers\Url::toRoute(['download', 'id' => $data->agra_id]), ['target' => '_blank']);
                            },
            ],

            [
                'attribute' => 'agra_fdata_sign',
                'label' => Yii::t('app', 'Agra Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->agra_fdata_sign, yii\helpers\Url::toRoute(['download-sign', 'id' => $data->agra_id]), ['target' => '_blank']);
                            },
            ],

            'agra_sum',
            'agra_tax',

            [
                'attribute' => 'agra_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
Pjax::end();
