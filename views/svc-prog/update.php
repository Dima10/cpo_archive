<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SvcProg */
/* @var $svc array */
/* @var $prog array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Svc Prog'),
]) . $model->sprog_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Progs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sprog_id, 'url' => ['view', 'id' => $model->sprog_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="svc-prog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'svc' => $svc,
        'prog' => $prog,
    ]) ?>

</div>
