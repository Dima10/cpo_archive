<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $addressType array */
/* @var $addrbook array */
/* @var $country array */
/* @var $region array */
/* @var $city array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Address'),
]) . $model->add_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->add_id, 'url' => ['view', 'id' => $model->add_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="address-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'addrbook' => $addrbook,
        'addressType' => $addressType,
        'country' => $country,
        'region' => $region,
        'city' => $city,
    ]) ?>

</div>
