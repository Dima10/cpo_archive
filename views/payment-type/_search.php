<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pt_id') ?>

    <?= $form->field($model, 'pt_name') ?>

    <?= $form->field($model, 'pt_create_user') ?>

    <?= $form->field($model, 'pt_create_ip') ?>

    <?= $form->field($model, 'pt_create_time') ?>

    <?php // echo $form->field($model, 'pt_update_user') ?>

    <?php // echo $form->field($model, 'pt_update_ip') ?>

    <?php // echo $form->field($model, 'pt_update_time') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
