<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="payment-type-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'pt_id',
            'pt_name',
            'pt_create_user',
            'pt_create_ip',
            'pt_create_time',
            'pt_update_user',
            'pt_update_ip',
            'pt_update_time',

        ],
    ]);

Pjax::end();


?>

</div>
