<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplShSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $trt array */
/* @var $trp array */

$this->title = Yii::t('app', 'Appl Shs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Appl Sh'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'ab' => $ab,
        'trt' => $trt,
        'trp' => $trp,
    ]) ?>

</div>
