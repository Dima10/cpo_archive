<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSh */
/* @var $form yii\widgets\ActiveForm */
/* @var $person array */
/* @var $program array */

?>

<div class="appl-sh-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'appls_number')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'appls_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <?php echo $form->field($model, 'appls_prs_id')->dropDownList($person); ?>

    <?php echo $form->field($model, 'appls_trp_id')->dropDownList($program); ?>

    <?php echo $form->field($model, 'appls_score_max')->textInput(); ?>

    <?php echo $form->field($model, 'appls_score')->textInput(); ?>

    <?php echo $form->field($model, 'appls_passed')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
