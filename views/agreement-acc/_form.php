<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAcc */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $company array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

?>

<div class="agreement-acc-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'aga_number')->textInput(['maxlength' => true]); ?>

    <?=  $form->field($model, 'aga_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ])
    ?>

    <?= $form->field($model, 'aga_ast_id')->dropDownList($agreement_status) ?>

    <?php echo $form->field($model, 'aga_ab_id')->dropDownList($entity); ?>

    <?php echo $form->field($model, 'aga_comp_id')->dropDownList($company); ?>

    <?php echo $form->field($model, 'aga_agr_id')->dropDownList($agreement); ?>

    <?php echo $form->field($model, 'aga_agra_id')->dropDownList($agreement_annex); ?>

    <?php echo $form->field($model, 'aga_sum')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'aga_tax')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'aga_data')->fileInput(); ?>

    <?php echo $form->field($model, 'aga_data_sign')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
