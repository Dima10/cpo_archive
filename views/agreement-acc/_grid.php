<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $company array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

?>
<div class="agreement-acc-index-grid">


<?php

    Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 100,
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {payment}',
                'buttons' => [
                    'view' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                    'payment' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-euro"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'FinanceBook Pay'),
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = yii\helpers\Url::to(['view', 'id' => $key]);
                            return $url;
                        }
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update', 'id' => $key]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete', 'id' => $key]);
                            return $url;
                        }
                        if ($action === 'payment') {
                            $url = yii\helpers\Url::to(['finance-book/update-pay', 'id' => $model->aga_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],
            //['class' => 'yii\grid\ActionColumn'],

            'aga_id',
            'aga_number',
            'aga_date',

            [
                'attribute' => 'aga_ast_id',
                'label' => Yii::t('app', 'Aga Ast ID'),
                'value' => function ($data) {
                    return $data->agaAst->ast_name;
                },
                'filter' => $agreement_status,
            ],

            [
                //'attribute' => 'aga_ab_id',
                'attribute' => 'ent_name',
                'label' => Yii::t('app', 'Aga Ab ID'),
                'value' => function ($data) {
                    return $data->agaAb->ab_name;
                },
                //'filter' => $entity,
            ],

            [
                'attribute' => 'ab_name',
                'label' => Yii::t('app', 'Ent Agent ID'),
                'value' => function ($data) {
                    return $data->agaAb->entity->entAgent->ab_name;
                },
            ],


            [
                'attribute' => 'aga_comp_id',
                'label' => Yii::t('app', 'Aga Comp ID'),
                'value' => function ($data) {
                    return $data->agaComp->comp_name;
                },
                'filter' => $company,
            ],

            [
                //'attribute' => 'aga_agr_id',
                'attribute' => 'agr_number',
                'label' => Yii::t('app', 'Aga Agr ID'),
                'value' => function ($data) {
                    return $data->agaAgr->agr_number;
                },
                //'filter' => $agreement,
            ],

            [
                //'attribute' => 'aga_agra_id',
                'attribute' => 'agra_number',
                'label' => Yii::t('app', 'Aga Agra ID'),
                'value' => function ($data) {
                    return isset($data->agaAgra) ? $data->agaAgra->agra_number : '';
                },
                //'filter' => $agreement_annex,
            ],


            'aga_sum',
            'aga_tax',
            'aga_comment',

            [
                'attribute' => 'aga_fdata',
                'label' => Yii::t('app', 'Aga Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->aga_fdata, yii\helpers\Url::toRoute(['download', 'id' => $data->aga_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'aga_fdata_sign',
                'label' => Yii::t('app', 'Aga Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->aga_fdata_sign, yii\helpers\Url::toRoute(['download-sign', 'id' => $data->aga_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'aga_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();

?>

</div>
