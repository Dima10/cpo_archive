<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAccSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-acc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'aga_id') ?>

    <?= $form->field($model, 'aga_number') ?>

    <?= $form->field($model, 'aga_date') ?>

    <?= $form->field($model, 'aga_ab_id') ?>

    <?= $form->field($model, 'aga_comp_id') ?>

    <?php // echo $form->field($model, 'aga_arg_id') ?>

    <?php // echo $form->field($model, 'aga_agra_id') ?>

    <?php // echo $form->field($model, 'aga_sum') ?>

    <?php // echo $form->field($model, 'aga_data') ?>

    <?php // echo $form->field($model, 'aga_fdata') ?>

    <?php // echo $form->field($model, 'aga_data_sign') ?>

    <?php // echo $form->field($model, 'aga_fdata_sign') ?>

    <?php // echo $form->field($model, 'aga_create_user') ?>

    <?php // echo $form->field($model, 'aga_create_time') ?>

    <?php // echo $form->field($model, 'aga_create_ip') ?>

    <?php // echo $form->field($model, 'aga_update_user') ?>

    <?php // echo $form->field($model, 'aga_update_time') ?>

    <?php // echo $form->field($model, 'aga_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
