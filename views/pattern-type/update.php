<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatternType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Pattern Type'),
]) . $model->patt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['site/setting']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pattern Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->patt_id, 'url' => ['view', 'id' => $model->patt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pattern-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
