<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgModule */
/* @var $prog array */
/* @var $module array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Training Prog Module',
]) . $model->trpl_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Prog Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->trpl_id, 'url' => ['view', 'id' => $model->trpl_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="training-prog-module-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'prog' => $prog,
        'module' => $module,
    ]) ?>

</div>
