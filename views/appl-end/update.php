<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplEnd */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl End'),
]) . $model->apple_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Ends'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->apple_id, 'url' => ['view', 'id' => $model->apple_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-end-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
