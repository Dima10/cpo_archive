<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplEndSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-end-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'apple_id') ?>

    <?= $form->field($model, 'apple_applsx_id') ?>

    <?= $form->field($model, 'apple_number') ?>

    <?= $form->field($model, 'apple_date') ?>

    <?= $form->field($model, 'apple_create_user') ?>

    <?php // echo $form->field($model, 'apple_create_time') ?>

    <?php // echo $form->field($model, 'apple_create_ip') ?>

    <?php // echo $form->field($model, 'apple_update_user') ?>

    <?php // echo $form->field($model, 'apple_update_time') ?>

    <?php // echo $form->field($model, 'apple_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
