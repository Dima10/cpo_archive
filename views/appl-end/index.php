<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplEndSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */

$this->title = Yii::t('app', 'Appl Ends');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-end-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Appl End'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'trt' => $trt,
    ]) ?>

</div>
