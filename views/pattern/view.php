<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pattern */

$this->title = $model->pat_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['site/setting']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Patterns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pattern-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pat_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pat_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pat_id',
            'pat_name',
            'patPatt.patt_name',
            'pat_code',
            'pat_fname',
            'pat_note:ntext',
            'pat_create_user',
            'pat_create_time',
            'pat_create_ip',
            'pat_update_user',
            'pat_update_time',
            'pat_update_ip',
        ],
    ]) ?>

</div>
