<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pattern */
/* @var $pattern_type array */
/* @var $pattern_svdt array */
/* @var $pattern_trt array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Pattern'),
]) . $model->pat_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['site/setting']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Patterns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pat_id, 'url' => ['view', 'id' => $model->pat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pattern-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pattern_type' => $pattern_type,
        'pattern_svdt' => $pattern_svdt,
        'pattern_trt' => $pattern_trt,
    ]) ?>

</div>
