<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ast_id') ?>

    <?= $form->field($model, 'ast_name') ?>

    <?= $form->field($model, 'ast_create_time') ?>

    <?= $form->field($model, 'ast_create_ip') ?>

    <?= $form->field($model, 'ast_update_user') ?>

    <?php // echo $form->field($model, 'ast_update_time') ?>

    <?php // echo $form->field($model, 'ast_update_ip') ?>

    <?php // echo $form->field($model, 'ast_create_user') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
