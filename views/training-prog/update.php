<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProg */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Training Prog'),
]) . $model->trp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Progs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->trp_id, 'url' => ['view', 'id' => $model->trp_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="training-prog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
