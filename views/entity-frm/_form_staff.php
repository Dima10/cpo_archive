<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $person array */

$js = '
        $("#prsSearch").click(
            function() {
                var prsVal = $("#prsText").val();
                $.get("'.\yii\helpers\Url::to(['/person/ajax-search']).'",
                    {
                      id: "person_id", 
                      text : prsVal
                    },
                    function (data) {
                        $("#person_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);

?>

<div class="entity-frm-staff-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ($model->isNewRecord ?
                                        ['create-staff', 'ent_id' => array_keys($entity)[0]]
                                        :
                                        ['update-staff', 'ent_id' => array_keys($entity)[0], 'id' => $model->stf_id ]
                            ),
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]);
        echo $form->errorSummary($model);

    ?>

    <?= $form->field($model, 'stf_ent_id')->dropDownList($entity, ['ReadOnly' => true]) ?>


    <div class="row">
        <div class="col-sm-2">
            <?php//= Html::label(Yii::t('app', 'Stf Prs ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'prsText', '', ['id' => 'prsText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'prsSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    <?= $form->field($model, 'stf_prs_id')->dropDownList($person, ['id' => 'person_id']) ?>

    <?= $form->field($model, 'stf_position')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
