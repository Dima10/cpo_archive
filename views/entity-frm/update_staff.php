<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $entity array */
/* @var $person array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Staff'),
]) . $model->stf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-frm-staff-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_staff', [
        'model' => $model,
        'entity' => $entity,
        'person' => $person,
    ]) ?>

</div>
