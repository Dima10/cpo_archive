<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentToSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $account_from array */
/* @var $account_to array */

?>
<div class="entity-frm-payment-to-index-grid">

<?php

$link1 = yii::$app->urlManager->createUrl(['payment-to/update-qty']);
$link2 = yii::$app->urlManager->createUrl(['payment-to/update-price']);
$link3 = yii::$app->urlManager->createUrl(['payment-to/update-from-account']);
$link4 = yii::$app->urlManager->createUrl(['payment-to/update-to-account']);
$link5 = yii::$app->urlManager->createUrl(['payment-to/update-to-ab']);
$link6 = yii::$app->urlManager->createUrl(['payment-to/update-date-pay']);
$link7 = yii::$app->urlManager->createUrl(['payment-to/update-payment-type']);
$linkS = yii::$app->urlManager->createUrl(['payment-to/update-sum']);
$linkA = yii::$app->urlManager->createUrl(['payment-to/get-to-account']);

$js = <<<EOL

function updateSum(id) {
        $.get(
            '$linkS',
            {
                id: id
            },
            function (data) {
                $("#sum_" +id).val(data); 
            }
        );
}

function getToAccount(id) {
        $.get(
            '$linkA',
            {
                id: "toAccount_" + id,
                payt_id: id
            },
            function (data) {
                $("#toAccount_" + id).html(data);
            }
        );
}

function onChangeEvent (obj, v) {

    if (v==1) {
        $.get(
            '$link1',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.value = data; 
                updateSum(obj.id);
            }
        );
    }

    if (v==2) {
        $.get(
            '$link2',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.value = data; 
                updateSum(obj.id);
            }
        );
    }

    if (v==3) {
        $.get(
            '$link3',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.innerHTML = data; 
            }
        );
    }
    if (v==4) {
        $.get(
            '$link4',
            {
                id: obj.id,
                payt_id: obj.getAttribute("payt_id"),
                value: obj.value
            },
            function (data) {
                obj.innerHTML = data; 
            }
        );
    }

    if (v==5) {
        $.get(
            '$link5',
            {
                id: obj.id,
                payt_id: obj.getAttribute("payt_id"),
                value: obj.value
            },
            function (data) {
                obj.innerHTML = data;
                getToAccount(obj.getAttribute("payt_id"));
            }
        );
    }

    if (v==6) {
        $.get(
            '$link6',
            {
                id: obj.getAttribute("payt_id"),
                value: obj.value
            },
            function (data) {
                obj.value = data;
            }
        );
    }

    if (v==7) {
        $.get(
            '$link7',
            {
                id: obj.id,
                payt_id: obj.getAttribute("payt_id"),
                value: obj.value
            },
            function (data) {
                //obj.innerHTML = data;
            }
        );
    }

}
EOL;

$this->registerJs($js, \yii\web\View::POS_END);


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if (isset($model->financeBook)) {
                return ['class' => 'info'];
            } else {
                return ['class' => 'danger'];
            }
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                            ]);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-payment-to-cache', 'id' => $model->payt_id]);
                            return $url;
                        }
                    }

            ],

            'payt_id',
            [
                'attribute' => 'paytFromAb.ab_name',
                'label' => Yii::t('app', 'Payt From Ab ID'),
            ],
            [
                'attribute' => 'paytFromAcc.acc_number',
                'label' => Yii::t('app', 'Payt From Acc ID'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        return Html::dropDownList(
                                $data->payt_id,
                                $data->payt_from_acc_id,
                                \yii\helpers\ArrayHelper::map($data->fromAccounts, 'acc_id', 'acc_number'),
                                [
                                    'id' => $data->payt_id,
                                    'payt_id' => $data->payt_id,
                                    'onChange' => 'onChangeEvent(this, 3)',
                                ]
                        );

                    },
            ],
            [
                'attribute' => 'paytToAb.ab_name',
                'label' => Yii::t('app', 'Payt To Ab ID'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        return Html::dropDownList(
                            $data->payt_id,
                            $data->payt_to_ab_id,
                            \yii\helpers\ArrayHelper::map($data->toAbs, 'ab_id', 'ab_name'),
                            [
                                'id' => $data->payt_to_ab_id,
                                'payt_id' => $data->payt_id,
                                'onChange' => 'onChangeEvent(this, 5)',
                            ]
                        );
                    },
            ],
            [
                'attribute' => 'paytToAcc.acc_number',
                'label' => Yii::t('app', 'Payt To Acc ID'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::dropDownList(
                            $data->payt_id,
                            $data->payt_to_acc_id,
                            \yii\helpers\ArrayHelper::map($data->toAccounts, 'acc_id', 'acc_number'),
                            [
                                'id' => 'toAccount_'.$data->payt_id,
                                'payt_id' => $data->payt_id,
                                'onChange' => 'onChangeEvent(this, 4)',
                            ]
                        );
                        return $s;
                    },
            ],
            [
                'attribute' => 'paytFbs.fbs_name',
                'label' => Yii::t('app', 'Payt Fbs ID'),
            ],
            [
                'attribute' => 'paytPt.pt_name',
                'label' => Yii::t('app', 'Payt Pt ID'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::dropDownList(
                            $data->payt_id,
                            $data->payt_pt_id,
                            \yii\helpers\ArrayHelper::map(\app\models\PaymentType::find()->all(), 'pt_id', 'pt_name'),
                            [
                                'id' => 'pt_'.$data->payt_pt_id,
                                'payt_id' => $data->payt_id,
                                'onChange' => 'onChangeEvent(this, 7)',
                            ]
                        );
                        return $s;
                    },
            ],
            [
                'attribute' => 'paytSvc.svc_name',
                'label' => Yii::t('app', 'Payt Svc ID'),
            ],

            [
                'attribute' => 'payt_svc_qty',
                'label' => Yii::t('app', 'Payt Svc Qty'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::textInput(
                                $data->payt_id,
                                $data->payt_svc_qty,
                                [
                                    'id' => $data->payt_id,
                                    'payt_id' => $data->payt_id,
                                    'onChange' => 'onChangeEvent(this, 1)',
                                    'size' => '6'
                                ]
                        );
                        return $s;
                    },
            ],
            [
                'attribute' => 'payt_svc_price',
                'label' => Yii::t('app', 'Payt Svc Price'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::textInput(
                            $data->payt_id,
                            $data->payt_svc_price,
                            [
                                'id' => $data->payt_id,
                                'payt_id' => $data->payt_id,
                                'onChange' => 'onChangeEvent(this, 2)',
                                'size' => '6'
                            ]
                        );
                        return $s;
                    },
            ],

            [
                'attribute' => 'payt_svc_sum',
                'label' => Yii::t('app', 'Payt Svc Sum'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::textInput(
                            $data->payt_id,
                            $data->payt_svc_sum,
                            [
                                'id' => 'sum_'.$data->payt_id,
                                'payt_id' => $data->payt_id,
                                'size' => '6',
                                'ReadOnly' => true
                            ]
                        );
                        return $s;
                    },
            ],

            [
                'attribute' => 'payt_date_pay',
                'label' => Yii::t('app', 'Payt Date Pay'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s =
                            \yii\jui\DatePicker::widget([
                                'name'  => 'payt_date_pay',
                                'value'  => $data->payt_date_pay,
                                //'language' => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options' => [
                                    'payt_id' => $data->payt_id,
                                    'onChange' => 'onChangeEvent(this, 6)',
                                ]
                            ]);
                        return $s;
                    },
            ],

            [
                'attribute' => 'payt_create_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],

        ],
    ]);

?>

</div>
