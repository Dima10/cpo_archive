<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $entityClass array */

$link1 = yii::$app->urlManager->createUrl(['entity-frm/create-class-link']);
$link2 = yii::$app->urlManager->createUrl(['entity-frm/delete-class-link']);

?>


<div id="class-link-id">

    <hr>

    <?php
    echo Html::dropDownList('', [], $entityClass, ['id' => 'class-id']);

    $s = Html::a('', '#', [
            'class' => 'glyphicon glyphicon-plus',
            'onClick' => "
                $.get(
                    '$link1',
                    {
                        id: $model->ent_id,
                        value: $('#class-id').val()
                    },
                    function (data) {
                        $('#class-link-id').html(data);
                    }
                );    
    
            "
        ]
    );
    $s .= '<ul>';
    foreach ($model->entityClassLnks as $key => $obj) {
        $s .= '<li>'.
            Html::a('', '#', [
                'class' => 'glyphicon glyphicon-trash',
                'onClick' => "
                    $.get(
                        '$link2',
                        {
                            id: $obj->entcl_id
                        },
                        function (data) {
                            $('#class-link-id').html(data);
                        }
                    );            
                "
            ])
            .' '.
            $obj->entclEntc->entc_name
            .'</li>';
    }
    $s .= '</ul>';
    echo $s;

    ?>
</div>

