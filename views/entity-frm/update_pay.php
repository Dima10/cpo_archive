<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $account app\models\AgreementAcc */


$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Acc'),
]) . $model->aga_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="agreementa-acc-update-payment">


    <h1><?= Html::encode($this->title) ?></h1>

    <input type="button" class="btn btn-success"  value="<?= Yii::t('app', 'Back'); ?>" onclick="window.history.go(-1);"/>

    <h2><?= Yii::t('app', 'Agreement Acc'); ?></h2>

    <?= DetailView::widget([
        'model' => $account,
        'attributes' => [
            'aga_id',
            'aga_number',
            'aga_date',
            'agaAb.ab_name',
            'agaComp.comp_name',
            'agaAgr.agr_number',
            //'agaAst.ast_name',
            //'agaAgra.agra_number',
            'aga_sum',
            'aga_tax',
        ],
    ]) ?>

    <div>

        <?= Html::label(Yii::t('app', 'Fb Payment'), ['fb_payment']) ?>
        <?= Html::textInput(Yii::t('app', 'Fb Payment'), '', ['id' => 'fb_payment']) ?>

        <?= Html::label(Yii::t('app', 'Fb Date'), ['fb_date']) ?>

        <?php
            echo DatePicker::widget([
                'name'  => 'fb_date',
                'id' => 'fb_date',
                'value' => date('Y-m-d'),
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ])
        ?>

        <?= Html::label(Yii::t('app', 'Fb Pay'), ['fb_sum_pay']) ?>
        <?= Html::textInput(Yii::t('app', 'Fb Pay'), '', ['id' => 'fb_sum_pay', 'onChange' => 'onChangeEventPay(this, 1)']) ?>
        <?= Html::button(Yii::t('app', 'Calculate Pay'), ['id' => 'fb_sum_pay_link', 'onClick' => 'onChangeEventPay(this, 2)', 'class' => 'btn btn-primary']) ?>

        <?= Html::button(Yii::t('app', 'Save Pay'), ['id' => 'fb_sum_pay_save', 'onClick' => 'onChangeEventPay(this, 3)', 'class' => 'btn btn-danger']) ?>
        &nbsp;        &nbsp;
        &nbsp;        &nbsp;
        <?= Html::button(Yii::t('app', 'Cancel Pay'), ['id' => 'fb_sum_pay_cancel', 'onClick' => 'onChangeEventPay(this, 4)', 'class' => 'btn btn-warning']) ?>

    </div>
    <hr>


    <?= $this->render('_grid_pay', [
        'dataProvider' => $dataProvider,
    ]) ?>

</div>

<?php
$link1 = yii::$app->urlManager->createUrl(['entity-frm/pay-check-sum']);
$link2 = yii::$app->urlManager->createUrl(['entity-frm/pay-calculate']);
$link3 = yii::$app->urlManager->createUrl(['entity-frm/save-calculate']);
$link4 = yii::$app->urlManager->createUrl(['entity-frm/pay-cancel']);

$js = <<<EOL
function onChangeEventPay (obj, v) {

    if (v==1) {
        $.get(
            '$link1',
            {
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }

    if (v==2) {
        $.get(
            '$link2',
            {
                id: {$account->aga_id},
                value: $('#fb_sum_pay').val(),
                basis: $('#fb_payment').val()
            },
            function (data) {
                $('#grid').html(data); 
            }
        );
    }

    if (v==3) {
        $.get(
            '$link3',
            {
                id: {$account->aga_id},
                basis: $('#fb_payment').val(),
                date: $('#fb_date').val(),
            },
            function (data) {
                $('#grid').html(data);
                $('#fb_payment').val('');
                $('#fb_sum_pay').val('');
            }
        );
    }

    if (v==4) {
        $.get(
            '$link4',
            {
                id: {$account->aga_id}
            },
            function (data) {
                $('#grid').html(data);
                $('#fb_payment').val('');
                $('#fb_sum_pay').val('');
            }
        );
    }

}
EOL;

$this->registerJs($js, \yii\web\View::POS_END);

?>