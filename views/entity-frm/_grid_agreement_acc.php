<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */
/* @var $company array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

echo Html::a(Yii::t('app', 'Create Agreement Acc'), ['create-agreement-acc', 'ent_id' => $entity->ent_id], ['class' => 'btn btn-success']);


Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 100,
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {payment} {paymentTo} {paymentRet}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                    'payment' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-euro"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'FinanceBook Pay'),
                                ]);
                        },
                    'paymentTo' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-share"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Payment To'),
                                ]);
                        },
                    'paymentRet' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-check"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Payment Ret'),
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-agreement-acc', 'ent_id' => $entity->ent_id, 'id' => $model->aga_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-agreement-acc', 'id' => $model->aga_id]);
                            return $url;
                        }
                        if ($action === 'payment') {
                            //$url = yii\helpers\Url::to(['finance-book/update-pay', 'id' => $model->aga_id]);
                            $url = yii\helpers\Url::to(['update-pay', 'id' => $model->aga_id]);
                            return $url;
                        }
                        if ($action === 'paymentTo') {
                            $url = yii\helpers\Url::to(['create-payment-to', 'id' => $model->aga_id]);
                            return $url;
                        }
                        if ($action === 'paymentRet') {
                            $url = yii\helpers\Url::to(['create-payment-ret', 'id' => $model->aga_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'aga_id',
            'aga_number',
            'aga_date',

            [
                'attribute' => 'aga_ast_id',
                'label' => Yii::t('app', 'Aga Ast ID'),
                'value' => function ($data) {
                    return $data->agaAst->ast_name;
                },
                'filter' => $agreement_status,
            ],
            /*
            [
                'attribute' => 'aga_ab_id',
                'label' => Yii::t('app', 'Aga Ab ID'),
                'value' => function ($data) {
                    return $data->agaAb->ab_name;
                },
                'filter' => $entity,
            ],
            */
            [
                'attribute' => 'aga_comp_id',
                'label' => Yii::t('app', 'Aga Comp ID'),
                'value' => function ($data) {
                    return $data->agaComp->comp_name;
                },
                'filter' => $company,
            ],

            [
                'attribute' => 'aga_agr_id',
                'label' => Yii::t('app', 'Aga Agr ID'),
                'value' => function ($data) {
                    return $data->agaAgr->agr_number;
                },
                'filter' => $agreement,
            ],

            [
                'attribute' => 'aga_agra_id',
                'label' => Yii::t('app', 'Aga Agra ID'),
                'value' => function ($data) {
                    return isset($data->agaAgra) ? $data->agaAgra->agra_number : '';
                },
                'filter' => $agreement_annex,
            ],

            'aga_sum',
            'aga_tax',
            'aga_comment',

            [
                'attribute' => 'aga_fdata',
                'label' => Yii::t('app', 'Aga Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->aga_fdata, yii\helpers\Url::toRoute(['download-agreement-acc', 'id' => $data->aga_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'aga_fdata_sign',
                'label' => Yii::t('app', 'Aga Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->aga_fdata_sign, yii\helpers\Url::toRoute(['download-agreement-acc-sign', 'id' => $data->aga_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'aga_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
}
Pjax::end();

?>

