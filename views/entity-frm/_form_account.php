<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Account */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $bank array */

$js = '
jQuery(document).ready(
    function() {
        $("#bicSearch").click(
            function() {
                var bicVal = $("#bicText").val();
                $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-bank-search']).'",
                    {bic: bicVal},
                    function (data) {
                        $("#bank_id").html(data);
                    }
                );
            }
        );
    }
);
';

$this->registerJs($js, \yii\web\View::POS_END);

?>

<div class="entity-frm-account-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ($model->isNewRecord ?
                                        ['create-account', 'ent_id' => array_keys($entity)[0]]
                                        :
                                        ['update-account', 'ent_id' => array_keys($entity)[0], 'id' => $model->acc_id ]
                            ),
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
        echo $form->errorSummary($model);
    ?>

    <?= $form->field($model, 'acc_ab_id')->dropDownList($entity, ['ReadOnly' => true]) ?>

    <div class="row">
        <div class="col-sm-2">
            <?= Html::label(Yii::t('app', 'Bank Bic')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'bic', '', ['id' => 'bicText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'bicSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>

    <?= $form->field($model, 'acc_bank_id')->dropDownList($bank, ['id' => 'bank_id']) ?>

    <?= $form->field($model, 'acc_number')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
