<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentRetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="entity-frm-payment-ret-index-grid">

<?php

$linkA = yii::$app->urlManager->createUrl(['payment-ret/get-account']);
$link1 = yii::$app->urlManager->createUrl(['payment-ret/update-account']);
$link2 = yii::$app->urlManager->createUrl(['payment-ret/update-payment-type']);
$link3 = yii::$app->urlManager->createUrl(['payment-ret/update-sum']);
$link4 = yii::$app->urlManager->createUrl(['payment-ret/update-tax']);
$link5 = yii::$app->urlManager->createUrl(['payment-ret/update-date-pay']);

$js = <<<"EOL"

function getAccount(id) {
        $.get(
            '$linkA',
            {
                id: "Account_" + id,
                ptr_id: id
            },
            function (data) {
                $("#Account_" + id).html(data);
            }
        );
}

function onChangeEvent (obj, v) {

    if (v==1) {
        $.get(
            '$link1',
            {
                id: obj.id,
                ptr_id: obj.getAttribute("ptr_id"),
                value: obj.value
            },
            function (data) {
                obj.innerHTML = data; 
            }
        );
    }

    if (v==2) {
        $.get(
            '$link2',
            {
                id: obj.id,
                ptr_id: obj.getAttribute("ptr_id"),
                value: obj.value
            },
            function (data) {
                obj.value = data; 
                //obj.innerHTML = data; 
            }
        );
    }

    if (v==3) {
        $.get(
            '$link3',
            {
                id: obj.id,
                ptr_id: obj.getAttribute("ptr_id"),
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }
    if (v==4) {
        $.get(
            '$link4',
            {
                id: obj.id,
                ptr_id: obj.getAttribute("ptr_id"),
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }

    if (v==5) {
        $.get(
            '$link5',
            {
                id: obj.id,
                ptr_id: obj.getAttribute("ptr_id"),
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }


}
EOL;

$this->registerJs($js, \yii\web\View::POS_END);


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if (isset($model->financeBook)) {
                return ['class' => 'info'];
            } else {
                return ['class' => 'danger'];
            }
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                            ]);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-payment-ret-cache', 'id' => $model->ptr_id]);
                            return $url;
                        }
                    }

            ],

            'ptr_id',
            [
                'attribute' => 'ptrAb.ab_name',
                'label' => Yii::t('app', 'Ptr Ab ID'),
            ],
            [
                'attribute' => 'ptrAcc.acc_number',
                'label' => Yii::t('app', 'Ptr Acc ID'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        return Html::dropDownList(
                                $data->ptr_id,
                                $data->ptr_acc_id,
                                \yii\helpers\ArrayHelper::map($data->accounts, 'acc_id', 'acc_number'),
                                [
                                    'id' => $data->ptr_id,
                                    'ptr_id' => $data->ptr_id,
                                    'onChange' => 'onChangeEvent(this, 1)',
                                ]
                        );

                    },
            ],

            /*
            [
                'attribute' => 'ptrAb.ab_name',
                'label' => Yii::t('app', 'Ptr Ab ID'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        return Html::dropDownList(
                            $data->ptr_id,
                            $data->ptr_ab_id,
                            \yii\helpers\ArrayHelper::map($data->abs, 'ab_id', 'ab_name'),
                            [
                                'id' => $data->ptr_ab_id,
                                'payt_id' => $data->ptr_id,
                                'onChange' => 'onChangeEvent(this, 5)',
                            ]
                        );
                    },
            ],
            */

            [
                'attribute' => 'ptrFbs.fbs_name',
                'label' => Yii::t('app', 'Ptr Fbs ID'),
            ],
            [
                'attribute' => 'ptrPt.pt_name',
                'label' => Yii::t('app', 'Ptr Pt ID'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::dropDownList(
                            $data->ptr_id,
                            $data->ptr_pt_id,
                            \yii\helpers\ArrayHelper::map(\app\models\PaymentType::find()->all(), 'pt_id', 'pt_name'),
                            [
                                'id' => 'pt_'.$data->ptr_pt_id,
                                'ptr_id' => $data->ptr_id,
                                'onChange' => 'onChangeEvent(this, 2)',
                            ]
                        );
                        return $s;
                    },
            ],
            [
                'attribute' => 'ptrSvc.svc_name',
                'label' => Yii::t('app', 'Ptr Svc ID'),
            ],

            [
                'attribute' => 'ptr_svc_sum',
                'label' => Yii::t('app', 'Ptr Svc Sum'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::textInput(
                            $data->ptr_id,
                            $data->ptr_svc_sum,
                            [
                                'id' => 'sum_'.$data->ptr_id,
                                'ptr_id' => $data->ptr_id,
                                'onChange' => 'onChangeEvent(this, 3)',
                                'size' => '6'
                            ]
                        );
                        return $s;
                    },
            ],

            [
                'attribute' => 'ptr_svc_tax',
                'label' => Yii::t('app', 'Ptr Svc Tax'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::textInput(
                            $data->ptr_id,
                            $data->ptr_svc_tax,
                            [
                                'id' => 'tax_'.$data->ptr_id,
                                'ptr_id' => $data->ptr_id,
                                'onChange' => 'onChangeEvent(this, 4)',
                                'size' => '6',
                            ]
                        );
                        return $s;
                    },
            ],

            [
                'attribute' => 'ptr_date_pay',
                'label' => Yii::t('app', 'Ptr Date Pay'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s =
                            \yii\jui\DatePicker::widget([
                                'name'  => 'ptr_date_pay',
                                'value'  => $data->ptr_date_pay,
                                //'language' => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options' => [
                                    'ptr_id' => $data->ptr_id,
                                    'onChange' => 'onChangeEvent(this, 5)',
                                ]
                            ]);
                        return $s;
                    },
            ],

            [
                'attribute' => 'payt_create_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],

        ],
    ]);

?>

</div>
