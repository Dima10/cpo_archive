<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $msg string */
/* @var $entity app\models\Entity */
?>

<h4>
    <?= $msg; ?>
</h4>

<?php
    if (isset($model)) {
        $form = ActiveForm::begin(
            [
                'action' => [
                    $model->isNewRecord ? 'create' : 'update',
                    'id' => $model->isNewRecord ? '': $model->applf_id,
                    'ent_id' => $entity->ent_id,
                    'method' => 'post'
                ],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]

        );

        echo $form->errorSummary($model);
        ActiveForm::end();
    }

?>

<?= Html::a(Yii::t('app', 'Вернуться к просмотру'), ['entity-frm/view', 'id' => $entity->ent_id], ['class' => 'btn btn-success']) ?>


