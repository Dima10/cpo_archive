<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentRetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="entity-frm-payment-to-index-grid">

<?php

$link1 = yii::$app->urlManager->createUrl(['payment-ret/update-date-pay']);

$js = <<<EOL


function onChangeEvent (obj, v) {
    if (v==1) {
        $.get(
            '$link1',
            {
                id: obj.getAttribute("ptr_id"),
                value: obj.value
            },
            function (data) {
                obj.value = data;
            }
        );
    }

}
EOL;

$this->registerJs($js, \yii\web\View::POS_END);


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if (isset($model->financeBook)) {
                return ['class' => 'info'];
            } else {
                return ['class' => 'danger'];
            }
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ptr_id',
            [
                'attribute' => 'ptrAb.ab_name',
                'label' => Yii::t('app', 'Ptr Ab ID'),
            ],
            [
                'attribute' => 'ptrAcc.acc_number',
                'label' => Yii::t('app', 'Ptr Acc ID'),
            ],
            [
                'attribute' => 'ptrFbs.fbs_name',
                'label' => Yii::t('app', 'Ptr Fbs ID'),
            ],
            [
                'attribute' => 'ptrPt.pt_name',
                'label' => Yii::t('app', 'Ptr Pt ID'),
            ],
            [
                'attribute' => 'ptrSvc.svc_name',
                'label' => Yii::t('app', 'Ptr Svc ID'),
            ],

            [
                'attribute' => 'ptr_svc_sum',
                'label' => Yii::t('app', 'Ptr Svc Sum'),
            ],
            [
                'attribute' => 'ptr_svc_tax',
                'label' => Yii::t('app', 'Ptr Svc Sum'),
            ],

            [
                'attribute' => 'ptr_date_pay',
                'label' => Yii::t('app', 'Ptr Date Pay'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s =
                            \yii\jui\DatePicker::widget([
                                'name'  => 'prt_date_pay',
                                'value'  => $data->ptr_date_pay,
                                //'language' => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options' => [
                                    'payt_id' => $data->ptr_id,
                                    'onChange' => 'onChangeEvent(this, 1)',
                                ]
                            ]);
                        return $s;
                    },
            ],

            [
                'attribute' => 'ptr_create_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_create_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_update_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_update_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],

        ],
    ]);

?>

</div>
