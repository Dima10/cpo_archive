<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentToSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $account_from array */
/* @var $account_to array */

?>
<div class="entity-frm-payment-to-index-grid">

<?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if (isset($model->financeBook)) {
                return ['class' => 'info'];
            } else {
                return ['class' => 'danger'];
            }
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {pay}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                            ]);
                        },
                    'pay' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-euro"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'FinanceBook Pay'),
                                ]);
                        },

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-payment-to', 'id' => $model->payt_id]);
                            return $url;
                        }
                        if ($action === 'pay') {
                            $url = yii\helpers\Url::to(['pay-payment-to', 'id' => $model->payt_id]);
                            return $url;
                        }
                    }

            ],

            'payt_id',
            [
                'attribute' => 'paytFromAb.ab_name',
                'label' => Yii::t('app', 'Payt From Ab ID'),
            ],
            [
                'attribute' => 'paytFromAcc.acc_number',
                'label' => Yii::t('app', 'Payt From Acc ID'),
            ],
            [
                'attribute' => 'paytToAb.ab_name',
                'label' => Yii::t('app', 'Payt To Ab ID'),
            ],
            [
                'attribute' => 'paytToAcc.acc_number',
                'label' => Yii::t('app', 'Payt To Acc ID'),
            ],
            [
                'attribute' => 'paytFbs.fbs_name',
                'label' => Yii::t('app', 'Payt Fbs ID'),
            ],
            [
                'attribute' => 'paytPt.pt_name',
                'label' => Yii::t('app', 'Payt Pt ID'),
            ],
            [
                'attribute' => 'paytSvc.svc_name',
                'label' => Yii::t('app', 'Payt Svc ID'),
            ],

            [
                'attribute' => 'payt_svc_qty',
                'label' => Yii::t('app', 'Payt Svc Qty'),
            ],
            [
                'attribute' => 'payt_svc_price',
                'label' => Yii::t('app', 'Payt Svc Price'),
            ],

            [
                'attribute' => 'payt_svc_sum',
                'label' => Yii::t('app', 'Payt Svc Sum'),
            ],

            [
                'attribute' => 'payt_date_pay',
                'label' => Yii::t('app', 'Payt Date Pay'),
            ],

            [
                'attribute' => 'payt_create_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],

        ],
    ]);

?>

</div>
