<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<?php

echo Yii::t('app', 'Listener');

Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{add}',
                'buttons' => [
                    'add' =>
                        function ($url, $model) {
                                if (($model->applrcApplr->applr_flag == 1) && ($model->applrcApplr->applr_ast_id == 10)) {
                                    if (is_null($model->applrc_applf_id))
                                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, ['title' => Yii::t('yii', 'Insert')]);
                                    else return '';
                                } else {
                                    return '';
                                }
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'add') {
                            $url = yii\helpers\Url::to(['create-appl-final', 'ent_id' => $entity->ent_id, 'id' => $model->applrc_id]);
                            return $url;
                        }
                        return '#';
                    }
            ],

            [
                'attribute' => 'applrc_prs_id',
                'label' => Yii::t('app', 'Prs Last Name'),
                'value' => function ($data) {
                    return $data->applrcPrs->prs_last_name;
                },
            ],
            [
                'attribute' => 'applrc_prs_id',
                'label' => Yii::t('app', 'Prs First Name'),
                'value' => function ($data) {
                    return $data->applrcPrs->prs_first_name;
                },
            ],
            [
                'attribute' => 'applrc_prs_id',
                'label' => Yii::t('app', 'Prs Middle Name'),
                'value' => function ($data) {
                    return $data->applrcPrs->prs_middle_name;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applrc Applr ID'),
                'value' => function ($data) {
                    return $data->applrcApplr->applr_number;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Date'),
                'value' => function ($data) {
                    return $data->applrcApplr->applr_date;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Ast ID'),
                'value' => function ($data) {
                    return $data->applrcApplr->applrAst->ast_name;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Reestr'),
                'value' => function ($data) {
                    return \app\models\Constant::reestr_val()[$data->applrcApplr->applr_reestr];
                },
            ],
            [
                'attribute' => 'applrc_svc_id',
                'label' => Yii::t('app', 'Applrc Svc ID'),
                'value' => function ($data) {
                    return $data->applrcSvc->svc_name;
                },
            ],
            [
                'attribute' => 'applrc_trp_id',
                'label' => Yii::t('app', 'Applrc Trp ID'),
                'value' => function ($data) {
                    return $data->applrcTrp->trp_name;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Flag'),
                'value' =>
                    function ($data) {
                        try {
                            return \app\models\Constant::YES_NO[$data->applrcApplr->applr_flag];
                        } catch (Exception $e){
                            return $data->applrcApplr->applr_flag;
                        }
                    },
            ],


            [
                'attribute' => 'applrc_create_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_create_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_update_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_update_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
Pjax::end();

