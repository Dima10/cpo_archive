<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\AgreementAnnex */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $pattern array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $company array */
/* @var $annex_a array */
/* @var $dataProvider yii\data\ArrayDataProvider */

?>

<div class="entity-frm-agreement-acc-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ['create-agreement-acc', 'ent_id' => array_keys($entity)[0]],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);

        echo isset($modelA) ? $form->errorSummary($modelA) : '';

    ?>

    <?=  $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])->label(Yii::t('app', 'Aga Date'))
    ?>

    <?=  $form->field($model, 'sum')->textInput(['id' => 'sum', 'ReadOnly' => true])->label(Yii::t('app', 'Aga Sum'))  ?>

    <?=  $form->field($model, 'tax')->textInput(['id' => 'tax', 'ReadOnly' => true])->label(Yii::t('app', 'Aga Tax'))  ?>

    <?= $form->field($model, 'ent_id')->dropDownList($entity, ['ReadOnly' => true])->label(Yii::t('app', 'Entity')) ?>

    <?= $form->field($model, 'comp_id')->dropDownList($company, ['ReadOnly' => true])->label(Yii::t('app', 'Company')) ?>

    <?= $form->field($model, 'agr_id')->dropDownList($agreement, ['id' => 'agr_id'])->label(Yii::t('app', 'Agreement')) ?>

    <?php //= $form->field($model, 'agra_id')->dropDownList($agreement_annex)->label(Yii::t('app', 'Agreement Annex')) ?>

    <?= $form->field($model, 'pat_id')->dropDownList($pattern)->label(Yii::t('app', 'Pattern')) ?>




    <hr>

    <?= $this->render('_ajax_agreement_acc_a_grid', [
        'dataProvider' => $dataProvider,
    ]) ?>


    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svcs'), 'svc'); ?></div>
        <div class="col-sm-6"><?= Html::dropDownList('annex', '', $annex_a, ['id' => 'annex_a', 'class'=> 'form-control']) ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Aca Price'), 'price'); ?></div>
        <div class="col-sm-6"><?= Html::textInput('price', '', ['id' => 'price', 'class'=> 'form-control']); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Aca Qty'), 'qty'); ?></div>
        <div class="col-sm-6"><?= Html::input('number', 'qty', '', ['id' => 'qty', 'class'=> 'form-control']); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6"></div>
        <div class="col-sm-4"><?= Html::a(Yii::t('app', 'Add Svc'), '#', ['id' => 'add_svc', 'class' => 'btn btn-success']); ?></div>
    </div>

    <hr>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>


</div>


<?php

$script = '
$(function() {
    $("#add_svc").click(function(e) {
        var _id = $("#annex_a").val();
        var _price = $("#price").val();
        var _qty = $("#qty").val();

        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a-create']).'",
                    {
                      ana_id : _id,
                      price : _price,
                      qty : _qty,
                    },
                    function (data) {
                        $("#grid_acc").html(data);
                        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a-sum']).'",
                            {
                            },
                            function (data) {
                                $("#sum").val(data);
                            }
                        );
                        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a-tax']).'",
                            {
                            },
                            function (data) {
                                $("#tax").val(data);
                            }
                        );
                    }
                );

    });
    
    $("#agr_id").change(function() {
        var agr_id = this.value;
        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a']).'",
                    {
                      agr_id : agr_id,
                    },
                    function (data) {
                        $("#annex_a").html(data);
                    }
        );
                
                
        $("#annex_a").change(function() {
            var ana_id = this.value;
            $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a-price']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#price").val(data);
                    }
            );

            $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a-qty']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#qty").val(data);
                    }
            );
        });
    });
});
';
$this->registerJs($script, yii\web\View::POS_END);


?>
