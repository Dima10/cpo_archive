<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplFinalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */

?>
<div class="appl-final-index-grid">

<?php

echo Yii::t('app', 'Appl Final');

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applf_id',
            'applf_name_first',
            'applf_name_last',
            'applf_name_middle',
            'applf_name_full',
            [
                'attribute' => 'applfSvdt.svdt_name',
                'label' => Yii::t('app', 'Applf Svdt ID'),
            ],
            [
                'attribute' => 'applfTrt.trt_name',
                'label' => Yii::t('app', 'Applf Trt ID'),
            ],
            [
                'attribute' => 'applfTrp.trp_name',
                'label' => Yii::t('app', 'Applf Trp ID'),
            ],


            'applf_trp_hour',
            'applf_cmd_date',
            'applf_end_date',
            'applf_number',

            [
                'attribute' => 'applf_file_name',
                'label' => Yii::t('app', 'Applf File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applf_file_name, yii\helpers\Url::toRoute(['appl-final/download', 'id' => $data->applf_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applf_file0_name',
                'label' => Yii::t('app', 'Applf File0 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applf_file0_name, yii\helpers\Url::toRoute(['appl-final/download0', 'id' => $data->applf_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applf_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
