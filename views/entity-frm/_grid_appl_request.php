<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $reestr array */
/* @var $entity app\models\Entity */
/* @var $training_type array */
/* @var $agreement_status array */

?>
<div class="entity-frm-appl-request-index-grid">
<h4>
    <?= Html::a(Yii::t('app', 'Create Appl Request'), ['appl-request/create', 'ab_id' => $entity->ent_id], ['class' => 'btn btn-success']) ?>
</h4>

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 100,
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['appl-request/update', 'id' => $model->applr_id, 'ent_id' => $entity->ent_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['appl-request/delete', 'id' => $model->applr_id, 'ent_id' => $entity->ent_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'applr_id',
            [
                'attribute' => 'applr_reestr',
                'value' => function ($data) use ($reestr) {
                    return $reestr[$data->applr_reestr];
                },
                'label' => Yii::t('app', 'Applr Reestr'),
                'filter' => $reestr,
            ],
            'applr_number',
            'applr_date',
            [
                'attribute' => 'applr_flag',
                'label' => Yii::t('app', 'Applr Flag'),
                'value' => function ($data) {
                    return  $data->applr_flag == 1 ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                },
                'filter' => [0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')]
            ],
            /*
            [
                'attribute' => 'applrAb.ab_name',
                'label' => Yii::t('app', 'Applr Ab ID'),
            ],
            */
            [
                'attribute' => 'applr_comp_id',
                'label' => Yii::t('app', 'Applr Comp ID'),
                'value' => function ($data) {
                    return $data->applrComp->comp_name ?? '';
                },
                'filter' => $comp,
            ],

            [
                'attribute' => 'agra_number',
                'label' => Yii::t('app', 'Applr Agra ID'),
                'value' => function ($data) {
                    return $data->applrAgra->agra_number ?? '';
                }
            ],

            [
                'attribute' => 'applr_trt_id',
                'label' => Yii::t('app', 'Applr Trt ID'),
                'value' => function ($data) {
                    return $data->applrTrt->trt_name;
                },
                'filter' => $training_type,
            ],

            [
                'attribute' => 'applr_ast_id',
                'label' => Yii::t('app', 'Applr Ast ID'),
                'value' => function ($data) {
                    return $data->applrAst->ast_name;
                },
                'filter' => $agreement_status,

            ],


            [
                'attribute' => 'applr_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
