<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $company array */
/* @var $agreement array */
/* @var $pattern array */
/* @var $modelAdd app\models\AgreementAdd */

$annex_a = [];
?>

<div class="entity-frm-agreement-add-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ['create-agreement-add', 'ent_id' => array_keys($entity)[0]],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);

        echo isset($modelAdd) ? $form->errorSummary($modelAdd) : '';

    ?>

    <?=  $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])->label(Yii::t('app', 'Agadd Date'))
    ?>

    <?= $form->field($model, 'ent_id')->dropDownList($entity, ['ReadOnly' => true])->label(Yii::t('app', 'Entity')) ?>

    <?= $form->field($model, 'agr_id')->dropDownList($agreement, ['id' => 'agr_id'])->label(Yii::t('app', 'Agadd Agr ID')) ?>

    <?= $form->field($model, 'pat_id')->dropDownList($pattern)->label(Yii::t('app', 'Pattern')) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

