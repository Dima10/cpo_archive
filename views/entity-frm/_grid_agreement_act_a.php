<?php

use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="entity-frm-agreement-act-a-index-grid">

<?php

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            [
                'attribute' => 'acta_id',
                'label' => Yii::t('app', 'Acta ID'),
                'value' => function ($data) {
                    return $data->acta_id;
                },
            ],

            [
                'attribute' => 'acta_ana_id',
                'label' => Yii::t('app', 'Acta Ana ID'),
                'value' => function ($data) {
                    if (isset($data->actaAna)) {

                        return ($data->actaAna->agreementAccAs[0]->acaAga->aga_number ?? '')
                            .' *** '.

                            (isset($data->actaAna->anaAgra) ? $data->actaAna->anaAgra->agra_number : '') . ' / ' .
                            (isset($data->actaAna->anaSvc) ? $data->actaAna->anaSvc->svc_name : '') . ' / ' .
                            (isset($data->actaAna->anaTrp) ? $data->actaAna->anaTrp->trp_name : '');
                    }
                    return '';
                },
            ],

            'acta_qty',
            'acta_price',
            [
                'attribute' => 'acta_tax',
                'label' => Yii::t('app', 'Acta Tax'),
                'value' => function ($data) {
                    return isset($data->aca_tax) ? $data->aca_tax : Yii::t('app', 'Without Tax');
                },
            ],


            [
                'attribute' => 'acta_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'acta_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}


?>

</div>
