<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplEndContent */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl End Content'),
]) . $model->applec_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl End Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applec_id, 'url' => ['view', 'id' => $model->applec_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-end-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
