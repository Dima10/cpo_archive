<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingModulePersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="training-module-person-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'tmp_id',
            'tmp_prs_id',
            'tmp_trm_id',
            'tmp_create_user',
            'tmp_create_time',
            'tmp_create_ip',
            'tmp_update_user',
            'tmp_update_time',
            'tmp_update_ip',

        ],
    ]);

Pjax::end();


?>

</div>
