<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'event_ab_id')->textInput() ?>

    <?= $form->field($model, 'event_evt_id')->textInput() ?>

    <?= $form->field($model, 'event_evrt_id')->textInput() ?>

    <?= $form->field($model, 'event_date')->textInput() ?>

    <?= $form->field($model, 'event_user_id')->textInput() ?>

    <?= $form->field($model, 'event_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'event_create_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_create_time')->textInput() ?>

    <?= $form->field($model, 'event_create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_update_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_update_time')->textInput() ?>

    <?= $form->field($model, 'event_update_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
