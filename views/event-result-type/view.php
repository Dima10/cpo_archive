<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EventResultType */

$this->title = $model->evrt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Result Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-result-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->evrt_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->evrt_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'evrt_id',
            'evrt_name',
            'evrt_note:ntext',
            'evrt_create_user',
            'evrt_create_time',
            'evrt_create_ip',
            'evrt_update_user',
            'evrt_update_time',
            'evrt_update_ip',
        ],
    ]) ?>

</div>
