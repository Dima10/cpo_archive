<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplEnd */

$this->title = Yii::t('app', 'Create Appl End');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Ends'), 'url' => ['appl-end/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-end-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_appl_end', [
        'model' => $model,
    ]) ?>

</div>
