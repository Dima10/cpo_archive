<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommand */

$this->title = $model->applcmd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Commands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-command-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applcmd_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applcmd_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applcmd_id',
            'applcmd_number',
            'applcmd_date',
            'applcmd_ab_id',
            'applcmd_comp_id',
            'applcmd_create_user',
            'applcmd_create_time',
            'applcmd_create_ip',
            'applcmd_update_user',
            'applcmd_update_time',
            'applcmd_update_ip',
        ],
    ]) ?>

</div>
