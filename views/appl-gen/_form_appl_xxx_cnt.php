<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplXxxContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-xxx-content-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applxxxc_applxxx_id')->textInput(); ?>

    <?php echo $form->field($model, 'applxxxc_prs_id')->textInput(); ?>

    <?php echo $form->field($model, 'applxxxc_trp_id')->textInput(); ?>

    <?php echo $form->field($model, 'applxxxc_ab_id')->textInput(); ?>

    <?php echo $form->field($model, 'applxxxc_passed')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
