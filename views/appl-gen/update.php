<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommand */
/* @var $ab array */
/* @var $comp array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Command'),
]) . $model->applcmd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Commands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applcmd_id, 'url' => ['view', 'id' => $model->applcmd_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-command-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab' => $ab,
        'comp' => $comp,
    ]) ?>

</div>
