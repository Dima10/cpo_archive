<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommand */
/* @var $ab array */
/* @var $comp array */

$this->title = Yii::t('app', 'Create Appl Gen');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Gens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-gen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_appl_all', [
        'model' => $model,
    ]) ?>

</div>
