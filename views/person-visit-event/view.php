<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PersonVisitEvent */

$this->title = $model->pve_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person Visit Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-visit-event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pve_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pve_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pve_id',
            'pve_name',
        ],
    ]) ?>

</div>
