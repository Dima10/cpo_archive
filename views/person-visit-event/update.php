<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersonVisitEvent */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Person Visit Event',
]) . $model->pve_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person Visit Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pve_id, 'url' => ['view', 'id' => $model->pve_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="person-visit-event-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
