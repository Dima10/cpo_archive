<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonVisitEventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="person-visit-event-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'pve_id',
            'pve_name',

        ],
    ]);

Pjax::end();


?>

</div>
