<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ab */

$this->title = Yii::t('app', 'Create Ab');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Abs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ab-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
