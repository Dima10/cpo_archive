<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AbSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ab-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ab_id') ?>

    <?= $form->field($model, 'ab_name') ?>

    <?= $form->field($model, 'ab_type') ?>

    <?= $form->field($model, 'ab_create_user') ?>

    <?= $form->field($model, 'ab_create_time') ?>

    <?php // echo $form->field($model, 'ab_create_ip') ?>

    <?php // echo $form->field($model, 'ab_update_user') ?>

    <?php // echo $form->field($model, 'ab_update_time') ?>

    <?php // echo $form->field($model, 'ab_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
