<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommand */
/* @var $ab array */
/* @var $comp array */
/* @var $trt array */
/* @var $svdt array */
/* @var $pat array */
/* @var $reestr array */
/* @var $reestr array */

$this->title = Yii::t('app', 'Create Appl Command');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Commands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-command-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        //'ab' => $ab,
        'comp' => $comp,
        'trt' => $trt,
        'svdt' => $svdt,
        'pat' => $pat,
        'reestr' => $reestr,
    ]) ?>

</div>
