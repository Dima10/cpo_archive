<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommand */
/* @var $form yii\widgets\ActiveForm */
/* @var $ab array */
/* @var $comp array */
/* @var $trt array */
/* @var $svdt array */
/* @var $pat array */
/* @var $reestr array */
?>

<div class="appl-command-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applcmd_reestr')->radioList($reestr); ?>

    <?php echo $form->field($model, 'applcmd_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <?php //echo $form->field($model, 'applcmd_ab_id')->dropDownList($ab); ?>

    <?php echo $form->field($model, 'applcmd_comp_id')->dropDownList($comp); ?>

    <?php echo $form->field($model, 'applcmd_trt_id')->dropDownList($trt); ?>

    <?php echo $form->field($model, 'applcmd_svdt_id')->dropDownList($svdt); ?>

    <?php
        if (!$model->isNewRecord)
            echo $form->field($model, 'applcmd_pat_id')->dropDownList($pat);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
