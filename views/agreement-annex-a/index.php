<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAnnexASearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $svc array */

$this->title = Yii::t('app', 'Agreement Annex As');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-annex-a-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Agreement Annex A'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'svc' => $svc,
    ]) ?>

</div>
