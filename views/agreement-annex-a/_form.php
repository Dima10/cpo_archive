<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnexA */
/* @var $form yii\widgets\ActiveForm */
/* @var $svc array */
?>

<div class="agreement-annex-a-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'ana_agra_id')->textInput(); ?>

    <?php echo $form->field($model, 'ana_svc_id')->dropDownList($svc); ?>

    <?php echo $form->field($model, 'ana_qty')->textInput(); ?>

    <?php echo $form->field($model, 'ana_price')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'ana_tax')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
