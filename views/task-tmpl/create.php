<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaskTmpl */

$this->title = Yii::t('app', 'Create Task Tmpl');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Tmpls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-tmpl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
