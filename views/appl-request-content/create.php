<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestContent */
/* @var $person array */
/* @var $svc array */

$this->title = Yii::t('app', 'Create Appl Request Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Request Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'person' => $person,
        'svc' => $svc,
    ]) ?>

</div>
