<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $reestr array */

?>
<div class="appl-request-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],

            'applr_id',
            [
                'attribute' => 'applr_reestr',
                'value' => function ($data) use ($reestr) {
                    return $reestr[$data->applr_reestr];
                },
                'label' => Yii::t('app', 'Applr Reestr'),
                'filter' => $reestr,
            ],
            'applr_number',
            'applr_date',
            'applr_flag',
            [
                'attribute' => 'applrAb.ab_name',
                'label' => Yii::t('app', 'Applr Ab ID'),
            ],
            [
                'attribute' => 'applrComp.comp_name',
                'label' => Yii::t('app', 'Applr Comp ID'),
            ],

            [
                'attribute' => 'applrAgra.agra_number',
                'label' => Yii::t('app', 'Applr Agra ID'),
            ],

            [
                'attribute' => 'applrTrt.trt_name',
                'label' => Yii::t('app', 'Applr Trt ID'),
            ],

            [
                'attribute' => 'applrAst.ast_name',
                'label' => Yii::t('app', 'Applr Ast ID'),
            ],


            [
                'attribute' => 'applr_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
