<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequest */

$this->title = $model->applr_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applr_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applr_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applr_id',
            'applr_number',
            'applr_date',
            'applr_ab_id',
            'applr_comp_id',
            'applr_agra_id',
            'applr_create_user',
            'applr_create_time',
            'applr_create_ip',
            'applr_update_user',
            'applr_update_time',
            'applr_update_ip',
        ],
    ]) ?>

</div>
