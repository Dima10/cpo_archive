<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestContent */
/* @var $applRequest app\models\ApplRequest */
/* @var $person array */
/* @var $svc array */
/* @var $prog array */
/* @var $applRequests array */

$this->title = Yii::t('app', 'Create Appl Request Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Request Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Request'), 'url' => ['update', 'id' => $applRequest->applr_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_content', [
        'model' => $model,
        'person' => $person,
        'svc' => $svc,
        'prog' => $prog,
        'applRequests' => $applRequests,
    ]) ?>

</div>
