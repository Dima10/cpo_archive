<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequest */
/* @var $searchModelC app\models\ApplRequestContentSearch */
/* @var $dataProviderC yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $astatus array */
/* @var $trt array */
/* @var $pattern array */
/* @var $reestr array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Request'),
]) . $model->applr_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applr_id, 'url' => ['update', 'id' => $model->applr_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab'  => $ab,
        'comp' => $comp,
        'agra' => $agra,
        'astatus' => $astatus,
        'trt' => $trt,
        'pattern' => $pattern,
        'reestr' => $reestr,
    ]) ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Person'), ['create-person', 'applr_id' => $model->applr_id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Create Appl Request Content'), ['create-content', 'applr_id' => $model->applr_id], ['class' => 'btn btn-success']) ?>
    </p>


    <?= $this->render('_grid_c', [
        'dataProvider' => $dataProviderC,
        'searchModel' => $searchModelC,
        'applr' => $model,
    ]) ?>


</div>
