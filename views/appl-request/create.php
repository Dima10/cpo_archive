<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplRequest */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $astatus array */
/* @var $trt array */
/* @var $pattern array */
/* @var $reestr array */

$this->title = Yii::t('app', 'Create Appl Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab'  => $ab,
        'comp' => $comp,
        'agra' => $agra,
        'astatus' => $astatus,
        'trt' => $trt,
        'pattern' => $pattern,
        'reestr' => $reestr,
    ]) ?>

</div>
