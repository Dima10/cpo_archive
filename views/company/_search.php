<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'comp_id') ?>

    <?= $form->field($model, 'comp_ent_id') ?>

    <?= $form->field($model, 'comp_note') ?>

    <?= $form->field($model, 'comp_create_user') ?>

    <?= $form->field($model, 'comp_create_time') ?>

    <?php // echo $form->field($model, 'comp_create_ip') ?>

    <?php // echo $form->field($model, 'comp_update_user') ?>

    <?php // echo $form->field($model, 'comp_update_time') ?>

    <?php // echo $form->field($model, 'comp_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
