<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplMain */
/* @var $ab array */
/* @var $comp array */
/* @var $svc array */
/* @var $person array */
/* @var $training_type */

$this->title = Yii::t('app', 'Create Appl Main');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Mains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-main-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab' => $ab,
        'comp' => $comp,
        'svc' => $svc,
        'person' => $person,
        'training_type' => $training_type,
    ]) ?>

</div>
