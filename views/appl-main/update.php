<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplMain */
/* @var $ab array */
/* @var $comp array */
/* @var $svc array */
/* @var $person array */
/* @var $training_type */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Appl Main',
]) . $model->applm_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Mains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applm_id, 'url' => ['view', 'id' => $model->applm_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-main-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab' => $ab,
        'comp' => $comp,
        'svc' => $svc,
        'person' => $person,
        'training_type' => $training_type,
    ]) ?>

</div>
