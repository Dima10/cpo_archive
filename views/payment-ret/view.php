<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentRet */

$this->title = $model->ptr_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Rets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-ret-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ptr_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ptr_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ptr_id',
            'ptr_ab_id',
            'ptr_comp_id',
            'ptr_aga_id',
            'ptr_sum',
            'ptr_tax',
            'ptr_comment',
            'ptr_create_user',
            'ptr_create_time',
            'ptr_create_ip',
            'ptr_update_user',
            'ptr_update_time',
            'ptr_update_ip',
        ],
    ]) ?>

</div>
