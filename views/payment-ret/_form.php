<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentRet */
/* @var $form yii\widgets\ActiveForm */
/* @var $ab array */
/* @var $comp array */
/* @var $aga array */
/* @var $acc array */


$js = '
        $("#abSearch").click(
            function() {
                var abVal = $("#abText").val();
                $.get("'.\yii\helpers\Url::to(['/ab/ajax-search']).'",
                    {
                      id: "ab_id", 
                      text : abVal
                    },
                    function (data) {
                        $("#ab_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);


?>

<div class="payment-ret-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);

    ?>

    <div class="row">
        <div class="col-sm-2">
            <?php//= Html::label(Yii::t('app', 'Ent Agent ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'abText', '', ['id' => 'abText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'abSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>

    <?php echo $form->field($model, 'ptr_ab_id')->dropDownList($ab  , ['id' => 'ab_id', 'onChange' => 'changeAb(this);']); ?>

    <?php echo $form->field($model, 'ptr_comp_id')->dropDownList($comp); ?>

    <?php echo $form->field($model, 'ptr_aga_id')->dropDownList($aga, ['id' => 'aga_id']); ?>

    <?php echo $form->field($model, 'ptr_acc_id')->dropDownList($acc, ['id' => 'acc_id', ]); ?>

    <?php echo $form->field($model, 'ptr_sum')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'ptr_tax')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'ptr_comment')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php

$script  = '
function changeAb(val) {
    $.get(
        "'.\yii\helpers\Url::to(['/account/ajax-search']).'",
        { 
            id: "acc_id",
            ab_id: val.value,
            acc_id: $("#acc_id").val()
        },
        function(data){
            $("#acc_id").html(data);
        }
    );
    
    $.get("'.\yii\helpers\Url::to(['/agreement-acc/ajax-search']).'",
        {
        id: "aga_id", 
        text : val.value
        },
        function (data) {
            $("#aga_id").html(data);
        }
    );
    
}

';

$this->registerJs($script, \yii\web\View::POS_BEGIN);

?>