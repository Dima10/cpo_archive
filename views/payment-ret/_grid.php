<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentRetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="payment-ret-index-grid">

<?php


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'ptr_id',
            [
                'attribute' => 'ptrAb.ab_name',
                'label' => Yii::t('app', 'Ptr Ab ID'),
            ],
            [
                'attribute' => 'ptrComp.comp_name',
                'label' => Yii::t('app', 'Ptr Comp ID'),
            ],
            [
                'attribute' => 'ptrAga.aga_number',
                'label' => Yii::t('app', 'Ptr Aga ID'),
            ],

            'ptr_sum',
            'ptr_tax',
            'ptr_comment',
            [
                'attribute' => 'ptr_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ptr_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ptr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ptr_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ptr_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ptr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

?>

</div>
