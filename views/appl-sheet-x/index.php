<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplSheetXSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */

$this->title = Yii::t('app', 'Appl Sheet Xes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sheet-x-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Appl Sheet X'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'trt' => $trt,
    ]) ?>

</div>
