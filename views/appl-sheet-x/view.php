<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSheetX */

$this->title = $model->applsx_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheet Xes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sheet-x-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applsx_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applsx_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applsx_id',
            'applsx_number',
            'applsx_date',
            'applsx_create_user',
            'applsx_create_time',
            'applsx_create_ip',
            'applsx_update_user',
            'applsx_update_time',
            'applsx_update_ip',
        ],
    ]) ?>

</div>
