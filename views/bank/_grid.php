<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

Pjax::begin(); 
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'bank_id',
                'options' => ['width' => '100'],
            ],

            'bank_name',
            'bank_bic',
            'bank_account',

            [
                'attribute' => 'bank_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'bank_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'bank_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'bank_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'bank_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'bank_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
Pjax::end();

