<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'task_ab_id')->textInput() ?>

    <?= $form->field($model, 'task_taskt_id')->textInput() ?>

    <?= $form->field($model, 'task_date_end')->textInput() ?>

    <?= $form->field($model, 'task_tasks_id')->textInput() ?>

    <?= $form->field($model, 'task_user_id')->textInput() ?>

    <?= $form->field($model, 'task_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'task_event_id')->textInput() ?>

    <?= $form->field($model, 'task_create_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'task_create_time')->textInput() ?>

    <?= $form->field($model, 'task_create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'task_update_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'task_update_time')->textInput() ?>

    <?= $form->field($model, 'task_update_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
