<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAccA */

$this->title = $model->aca_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Acc As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-acc-a-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->aca_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->aca_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'aca_id',
            'aca_ana_id',
            'aca_aga_id',
            'aca_qty',
            'aca_price',
            'aca_tax',
            'aca_create_user',
            'aca_create_time',
            'aca_create_ip',
            'aca_update_user',
            'aca_update_time',
            'aca_update_ip',
        ],
    ]) ?>

</div>
