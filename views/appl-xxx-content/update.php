<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplXxxContent */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Xxx Content'),
]) . $model->applxxxc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Xxx Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applxxxc_id, 'url' => ['view', 'id' => $model->applxxxc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-xxx-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
