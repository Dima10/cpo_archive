<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplXxxContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-xxx-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applxxxc_id') ?>

    <?= $form->field($model, 'applxxxc_applxxx_id') ?>

    <?= $form->field($model, 'applxxxc_prs_id') ?>

    <?= $form->field($model, 'applxxxc_trp_id') ?>

    <?= $form->field($model, 'applxxxc_score') ?>

    <?php // echo $form->field($model, 'applxxxc_passed') ?>

    <?php // echo $form->field($model, 'applxxxc_create_user') ?>

    <?php // echo $form->field($model, 'applxxxc_create_time') ?>

    <?php // echo $form->field($model, 'applxxxc_create_ip') ?>

    <?php // echo $form->field($model, 'applxxxc_update_user') ?>

    <?php // echo $form->field($model, 'applxxxc_update_time') ?>

    <?php // echo $form->field($model, 'applxxxc_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
