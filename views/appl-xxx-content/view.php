<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplXxxContent */

$this->title = $model->applxxxc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Xxx Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-xxx-content-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applxxxc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applxxxc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applxxxc_id',
            'applxxxc_applxxx_id',
            'applxxxc_prs_id',
            'applxxxc_ab_id',
            'applxxxc_position',
            'applxxxc_passed',
            'applxxxc_create_user',
            'applxxxc_create_time',
            'applxxxc_create_ip',
            'applxxxc_update_user',
            'applxxxc_update_time',
            'applxxxc_update_ip',
        ],
    ]) ?>

</div>
