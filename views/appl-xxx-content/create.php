<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplXxxContent */

$this->title = Yii::t('app', 'Create Appl Xxx Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Xxx Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-xxx-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
