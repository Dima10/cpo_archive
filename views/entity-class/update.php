<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EntityClass */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Entity Class'),
]) . $model->entc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->entc_id, 'url' => ['view', 'id' => $model->entc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-class-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
