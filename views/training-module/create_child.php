<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainingModule */
/* @var $parent app\models\TrainingModule */

$this->title = Yii::t('app', 'Create Training Sub Module');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $parent->trm_id, 'url' => ['view', 'id' => $parent->trm_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-module-create-child">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_child', [
        'model' => $model,
        'parent' => $parent,
    ]) ?>

</div>
