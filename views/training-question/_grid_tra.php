<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?= GridView::widget([
        'id' => 'tra_grid',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('', '#',
                                [
                                    'class' => 'glyphicon glyphicon-trash',
                                    'onclick' => '
                                        if (confirm("'.Yii::t('app', 'Are you sure you want to delete this item?').'")) {
                                            $.post("'.\yii\helpers\Url::toRoute(['delete-answer', 'trq_id' => $model->tra_trq_id, 'id' => $model->tra_id]).'",
                                                function(data) {
                                                    $( \'#tra_grid\' ).html(data);
                                                }
                                            );
                                        }',

                                ]
                            );
                        }

                ],
            ],
            /*
            'tra_id',
            [
                'attribute' => 'tra_trq_id',
                'value' => 'traTrq.trq_question',
            ],
            */
            'tra_answer:ntext',
            'tra_variant',
            [
                'attribute' => 'tra_file_name',
                'format' => 'raw',
                'value' =>  function ($data) {
                    if (isset($data->tra_file_name)) {
                        return Html::img(yii\helpers\Url::to(['training-answer/image', 'id' => $data->tra_id]), ['width' => '270px']);
                    } else {
                        return null;
                    }
                },
            ],

            [
                'attribute' => 'tra_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'tra_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]); ?>
