<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingQuestion */
/* @var $form yii\widgets\ActiveForm */
/* @var $module array */

$js = '
        $("#moduleSearch").click(
            function() {
                var modVal = $("#moduleText").val();
                $.get("'.\yii\helpers\Url::to(['/training-question/ajax-training-module']).'",
                    {
                      val : modVal
                    },
                    function (data) {
                        $("#module_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, \yii\web\View::POS_READY);


?>

<div class="training-question-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); ?>

    <?php echo $form->field($model, 'trq_question')->textarea(['rows' => 2]); ?>

    <div class="row">
        <div class="col-sm-2">
            <?= Html::label(Yii::t('app', 'Trq Trm ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'module', '', ['id' => 'moduleText', 'class' => 'form-control']) ?>
        </div>
        <div class="col-sm-4">
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'moduleSearch']) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'trq_trm_id')->dropDownList($module, ['id' => 'module_id']); ?>

    <?php echo $form->field($model, 'trq_fdata')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
