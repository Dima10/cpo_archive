<?php


if (Yii::$app->user->identity->level >= 70) {
    echo yii\widgets\Menu::widget([
            'items'=> [
                ['label' => Yii::t('app', 'Training Progs'), 'url' => ['/training-prog']],
                ['label' => Yii::t('app', 'Training Modules'), 'url' => ['/training-module']],
                ['label' => Yii::t('app', 'Training Questions'), 'url' => ['/training-question']],
                ['label' => Yii::t('app', 'Training Types'), 'url' => ['/training-type']],
                ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['/appl-request']],
                ['label' => Yii::t('app', 'Appl Commands'), 'url' => ['/appl-command']],
                ['label' => Yii::t('app', 'Appl Shs'), 'url' => ['/appl-sh']],
                ['label' => Yii::t('app', 'Appl Sh Xes'), 'url' => ['/appl-sh-x']],
                ['label' => Yii::t('app', 'Appl Sheets'), 'url' => ['/appl-sheet']],
                ['label' => Yii::t('app', 'Appl Sheet Xes'), 'url' => ['/appl-sheet-x']],
                ['label' => Yii::t('app', 'Appl Xxxes'), 'url' => ['/appl-xxx']],
                ['label' => Yii::t('app', 'Appl Ends'), 'url' => ['/appl-end']],
                ['label' => Yii::t('app', 'Appl Mains'), 'url' => ['/appl-main']],
            ],

    ]);
}


