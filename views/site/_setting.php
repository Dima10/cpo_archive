<?php

if (isset(Yii::$app->user->identity) ? Yii::$app->user->identity->level >= 70 : false) {
    echo yii\widgets\Menu::widget([
            'items'=> [
                //['label' => Yii::t('app', 'Tags'), 'url' => ['/tag']],
                ['label' => Yii::t('app', 'Pattern Types'), 'url' => ['/pattern-type']],
                ['label' => Yii::t('app', 'Patterns'), 'url' => ['/pattern']],
            ],

    ]);
}


