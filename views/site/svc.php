<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Svc');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-svc">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_svc', []) ?>

</div>
