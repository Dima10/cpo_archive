<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Frm');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-frm">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_frm', []) ?>

</div>
