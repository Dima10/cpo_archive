<?php

/* @var $this yii\web\View */

use yii\bootstrap\Collapse;
use yii\helpers\Html;
use \yii\helpers\Url;

$this->title = Yii::t('app', 'Application');

?>

<div class="site-index">
    <div class="body-content">
    </div>
</div>

        <!---
        <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#frm"><?php echo Yii::t('app', 'Frm')?></button>
        </img>

        <div class="col-*-*">
            <ul class="nav nav-pills nav-stacked">
                <li><button type="button" class="btn btn-link" data-toggle="collapse" data-target="#frm"><?php echo Yii::t('app', 'Frm')?></button></li>
                    <div id="frm" class="collapse" >
                        <?= $this->render('_frm', []) ?>
                    </div>
                <li><button type="button" class="btn btn-link" data-toggle="collapse" data-target="#core"><?php echo Yii::t('app', 'Core')?></button></li>
                    <div id="core" class="collapse" >
                        <?= $this->render('_core', []) ?>
                    </div>
                <li><button type="button" class="btn btn-link" data-toggle="collapse" data-target="#setting"><?php echo Yii::t('app', 'Setting')?></button></li>
                    <div id="setting" class="collapse" >
                        <?= $this->render('_setting', []) ?>
                    </div>
            </ul>
        </div>

        <div class="col-*-*">

<? Collapse::widget([
    'items' => [
        [
            'label' => Yii::t('app', 'Frm'),
            'content' => $this->render('_frm', []),
            // Открыто по-умолчанию
            'contentOptions' => [
                'class' => 'in'
            ]
        ],
        [
            'label' => Yii::t('app', 'Core'),
            'content' => $this->render('_core', []),
            'contentOptions' => [],
            'options' => []
        ],
        [
            'label' => Yii::t('app', 'Setting'),
            'content' => $this->render('_setting', []),
            'contentOptions' => [],
            'options' => []
        ]
    ]
    ]);
?>
        </div>

        </div>
-->

