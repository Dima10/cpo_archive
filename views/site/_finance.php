<?php

if (Yii::$app->user->identity->level >= 70) {
    echo yii\widgets\Menu::widget([
            'items'=> [
                ['label' => Yii::t('app', 'Banks'), 'url' => ['/bank']],
                ['label' => Yii::t('app', 'Accounts'), 'url' => ['/account']],
                [
                    'label' => Yii::t('app', 'Documents'),
                    'items' => [
                        ['label' => Yii::t('app', 'Agreements'), 'url' => ['/agreement']],
                        ['label' => Yii::t('app', 'Agreement Annexes'), 'url' => ['/agreement-annex']],
                        ['label' => Yii::t('app', 'Agreement Acts'), 'url' => ['/agreement-act']],
                        ['label' => Yii::t('app', 'Agreement Accs'), 'url' => ['/agreement-acc']],
                        ['label' => Yii::t('app', 'Agreement Statuses'), 'url' => ['/agreement-status']],
                     ]
                ],
                ['label' => Yii::t('app', 'Finance Books'), 'url' => ['/finance-book']],
                ['label' => Yii::t('app', 'Finance Book Statuses'), 'url' => ['/finance-book-status']],
                ['label' => Yii::t('app', 'Calendar'), 'url' => ['/calendar']],
            ],

    ]);
}


