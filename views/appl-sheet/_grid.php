<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplSheetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $trt array */
/* @var $trp array */

?>
<div class="appl-sheet-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'appls_id',
            'appls_number',
            [
                'attribute' => 'appls_number',
                'label' => Yii::t('app', 'Link'),
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->appls_magic)) {
                        return Html::a(Yii::$app->urlManager->createAbsoluteUrl(['appl-sheet/test', 'uniq' => $data->appls_magic]), Yii::$app->urlManager->createAbsoluteUrl(['appl-sheet/test', 'uniq' => $data->appls_magic]), ['target' => '_blank']);
                    } else {
                        return '';
                    }
                },
            ],
            'appls_date',
            [
                'attribute' => 'appls_prs_id',
                'value' =>
                    function ($data) {
                        return $data->applsPrs->prs_full_name;
                    },
                'label' => Yii::t('app', 'Appls Prs ID'),
                'filter' => $ab,
            ],
            [
                'attribute' => 'appls_trp_id',
                'value' =>
                    function ($data) {
                        return $data->applsTrp->trp_name;
                    },
                'label' => Yii::t('app', 'Appls Trp ID'),
                'filter' => $trp,
            ],

            'appls_score_max',
            'appls_score',
            'appls_passed',
            [
                'attribute' => 'appls_trt_id',
                'label' => Yii::t('app', 'Appls Trt ID'),
                'value' =>
                    function ($data) {
                        return $data->applsTrt->trt_name;
                    },
                'filter' => $trt,
            ],

            [
                'attribute' => 'appls_file_name',
                'label' => Yii::t('app', 'Appls File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->appls_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->appls_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'appls_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();


?>

</div>
