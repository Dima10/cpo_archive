<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSheet */
/* @var $person array */
/* @var $program array */
/* @var $searchModel app\models\ApplSheetContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Sheet'),
]) . $model->appls_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->appls_id, 'url' => ['view', 'id' => $model->appls_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-sheet-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'person' => $person,
        'program' => $program,
    ]) ?>


    <?= $this->render('_grid_detail', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>

</div>
