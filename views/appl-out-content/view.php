<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOutContent */

$this->title = $model->apploutc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Out Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-out-content-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->apploutc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->apploutc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'apploutc_id',
            'apploutc_applout_id',
            'apploutc_prs_id',
            'apploutc_trp_id',
            'apploutc_ab_id',
            'apploutc_applcmd_id',
            'apploutc_applsx_id',
            'apploutc_apple_id',
            'apploutc_number_upk',
            'apploutc_create_user',
            'apploutc_create_time',
            'apploutc_create_ip',
            'apploutc_update_user',
            'apploutc_update_time',
            'apploutc_update_ip',
        ],
    ]) ?>

</div>
