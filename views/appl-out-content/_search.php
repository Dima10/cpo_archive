<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOutContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-out-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'apploutc_id') ?>

    <?= $form->field($model, 'apploutc_applout_id') ?>

    <?= $form->field($model, 'apploutc_prs_id') ?>

    <?= $form->field($model, 'apploutc_trp_id') ?>

    <?= $form->field($model, 'apploutc_ab_id') ?>

    <?php // echo $form->field($model, 'apploutc_applcmd_id') ?>

    <?php // echo $form->field($model, 'apploutc_applsx_id') ?>

    <?php // echo $form->field($model, 'apploutc_apple_id') ?>

    <?php // echo $form->field($model, 'apploutc_number_upk') ?>

    <?php // echo $form->field($model, 'apploutc_create_user') ?>

    <?php // echo $form->field($model, 'apploutc_create_time') ?>

    <?php // echo $form->field($model, 'apploutc_create_ip') ?>

    <?php // echo $form->field($model, 'apploutc_update_user') ?>

    <?php // echo $form->field($model, 'apploutc_update_time') ?>

    <?php // echo $form->field($model, 'apploutc_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
