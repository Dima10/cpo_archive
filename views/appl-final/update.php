<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplFinal */
/* @var $pattern array */
/* @var $trt array */
/* @var $trp array */
/* @var $svdt array */
/* @var $ent_id integer */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Appl Final',
]) . $model->applf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Finals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applf_id, 'url' => ['view', 'id' => $model->applf_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-final-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pattern' => $pattern,
        'trt' => $trt,
        'trp' => $trp,
        'svdt' => $svdt,
        'ent_id' => $ent_id,
    ]) ?>

</div>
