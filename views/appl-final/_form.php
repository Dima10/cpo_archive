<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplFinal */
/* @var $form yii\widgets\ActiveForm */
/* @var $pattern array */
/* @var $trt array */
/* @var $trp array */
/* @var $svdt array */
/* @var $ent_id integer */
?>

<div class="appl-final-form">

    <?php $form = ActiveForm::begin(
        [
            'action' => [
                $model->isNewRecord ? 'create' : 'update',
                'id' => $model->isNewRecord ? '': $model->applf_id,
                'ent_id' => $ent_id,
                'method' => 'post'
            ],
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applf_name_first')->textInput(['maxlength' => true, 'ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_name_last')->textInput(['maxlength' => true, 'ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_name_middle')->textInput(['maxlength' => true, 'ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_svdt_id')->dropDownList($svdt, ['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_trt_id')->dropDownList($trt, ['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_trp_id')->dropDownList($trp, ['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_trp_hour')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_cmd_date')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_end_date')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_number')->textInput(['maxlength' => true, 'ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'applf_pat_id')->dropDownList($pattern); ?>

    <?php //echo $form->field($model, 'applf_file_data')->fileInput(); ?>

    <?php //echo $form->field($model, 'applf_file0_data')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
