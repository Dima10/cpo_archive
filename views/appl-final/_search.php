<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplFinalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-final-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applf_id') ?>

    <?= $form->field($model, 'applf_name_first') ?>

    <?= $form->field($model, 'applf_name_last') ?>

    <?= $form->field($model, 'applf_name_middle') ?>

    <?= $form->field($model, 'applf_name_full') ?>

    <?php // echo $form->field($model, 'applf_svdt_id') ?>

    <?php // echo $form->field($model, 'applf_trt_id') ?>

    <?php // echo $form->field($model, 'applf_trp_id') ?>

    <?php // echo $form->field($model, 'applf_trp_hour') ?>

    <?php // echo $form->field($model, 'applf_cmd_date') ?>

    <?php // echo $form->field($model, 'applf_end_date') ?>

    <?php // echo $form->field($model, 'applf_number') ?>

    <?php // echo $form->field($model, 'applf_file_data') ?>

    <?php // echo $form->field($model, 'applf_file_name') ?>

    <?php // echo $form->field($model, 'applf_file0_data') ?>

    <?php // echo $form->field($model, 'applf_file0_name') ?>

    <?php // echo $form->field($model, 'applf_create_user') ?>

    <?php // echo $form->field($model, 'applf_create_time') ?>

    <?php // echo $form->field($model, 'applf_create_ip') ?>

    <?php // echo $form->field($model, 'applf_update_user') ?>

    <?php // echo $form->field($model, 'applf_update_time') ?>

    <?php // echo $form->field($model, 'applf_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
