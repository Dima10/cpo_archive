<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplFinal */

$this->title = $model->applf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Finals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-final-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applf_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applf_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'applf_id',
                'applf_name_first',
                'applf_name_last',
                'applf_name_middle',
                'applf_name_full',
                'applf_svdt_id',
                'applf_trt_id',
                'applf_trp_id',
                'applf_trp_hour',
                'applf_cmd_date',
                'applf_end_date',
                'applf_number',
                'applf_file_name',
                'applf_file0_name',
                'applf_create_user',
                'applf_create_time',
                'applf_create_ip',
                'applf_update_user',
                'applf_update_time',
                'applf_update_ip',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    ?>

</div>
