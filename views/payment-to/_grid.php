<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentToSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="payment-to-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'payt_id',
            [
                'attribute' => 'paytFromAb.ab_name',
                'label' => Yii::t('app', 'Payt From Ab ID'),
            ],
            [
                'attribute' => 'paytFromAcc.acc_name',
                'label' => Yii::t('app', 'Payt From Acc ID'),
            ],
            [
                'attribute' => 'paytToAb.ab_name',
                'label' => Yii::t('app', 'Payt To Ab ID'),
            ],
            [
                'attribute' => 'paytToAcc.acc_name',
                'label' => Yii::t('app', 'Payt To Acc ID'),
            ],
            [
                'attribute' => 'paytFbs.fbs_name',
                'label' => Yii::t('app', 'Payt Fbs ID'),
            ],
            [
                'attribute' => 'paytSvc.svc_name',
                'label' => Yii::t('app', 'Payt Svc ID'),
            ],

            'payt_svc_price',
            'payt_svc_qty',
            'payt_svc_sum',
            'payt_date_pay',
            [
                'attribute' => 'payt_create_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_create_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'payt_update_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],

        ],
    ]);

Pjax::end();


?>

</div>
