<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SvcSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $doc_type array */
/* @var $ab array */

$this->title = Yii::t('app', 'Svcs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Svc'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'doc_type' => $doc_type,
        'ab' => $ab,
    ]) ?>

</div>
