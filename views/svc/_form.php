<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Svc */
/* @var $form yii\widgets\ActiveForm */
/* @var $doc_type array */
/* @var $ab array */


?>

<div class="svc-form">

    <?php
        $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],
        ]);

        echo $form->errorSummary($model);
    ?>

    <?= $form->field($model, 'svc_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'svc_svdt_id')->dropDownList($doc_type) ?>

    <?= $form->field($model, 'svc_skill')->textInput() ?>

    <?= $form->field($model, 'svc_hour')->textInput() ?>

    <?= $form->field($model, 'svc_price')->textInput() ?>

    <?= $form->field($model, 'svc_cost')->textInput() ?>

    <?= $form->field($model, 'svc_cost_ext')->textInput() ?>

    <?= $form->field($model, 'svc_tax_flag')->checkbox([],false)->label(Yii::t('app', 'Svc Tax Flag')) ?>

    <?= $form->field($model, 'svc_payment_flag')->checkbox([], false)->label(Yii::t('app', 'Svc Payment Flag')) ?>

    <?= $form->field($model, 'svc_ab_id')->dropDownList($ab) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
