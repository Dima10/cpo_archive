<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserMail */
/* @var $sysmail array */
/* @var $reestr array */
/* @var $type array */

$this->title = Yii::t('app', 'Create User Mail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Mails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-mail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sysmail' => $sysmail,
        'reestr' => $reestr,
        'type' => $type,
    ]) ?>

</div>
