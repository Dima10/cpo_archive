<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-sh-content-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applsc_appls_id')->textInput(); ?>

    <?php echo $form->field($model, 'applsc_trq_id')->textInput(); ?>

    <?php echo $form->field($model, 'applsc_trq_question')->textarea(['rows' => 6]); ?>

    <?php echo $form->field($model, 'applsc_tra_id')->textInput(); ?>

    <?php echo $form->field($model, 'applsc_tra_answer')->textarea(['rows' => 6]); ?>

    <?php echo $form->field($model, 'applsc_tra_variant')->textInput(); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
