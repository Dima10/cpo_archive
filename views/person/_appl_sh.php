<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
//use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplSheetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $sh app\models\ApplSh */
/* @var $trp_id integer */

?>

<h4>

    Промежуточное тестирование проводится в процессе освоения программы.<br><br>
    После завершения освоения части лекционного материала, слушателю будет открыт доступ к  промежуточному тестированию.<br><br>
    Количество попыток прохождения промежуточного тестирования � <?= isset($sh) ? $sh->appls_try : 500 ?>.<br><br>

    Если у Вас не осталось попыток для прохождения тестирования или появились вопросы, просим обратиться в администрацию ООО "ЦПО" для пояснения дальнейших действия и предоставления справки о пройденном курсе обучения, без выдачи удостоверения по тел. 8 (925) 017-95-36 или по электронной почте: info@procpo.ru<br><br>

    Компьютерное промежуточное тестирование считается пройденным с положительным результатом в случае, когда процент правильных ответов за каждый тест составляет 60 и более процентов.<br><br>

</h4>

<?php


    echo Html::a(Yii::t('app', 'Начать тестирование'), Url::toRoute(['appl-sh', 'trp_id' => $trp_id]), ['class' => 'btn btn-primary'])

/*
Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'appls_id',
            'appls_number',
            [
                'attribute' => 'appls_number',
                'label' => Yii::t('app', 'Link'),
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->appls_magic)) {
                        return Html::a(Yii::$app->urlManager->createAbsoluteUrl(['appl-sh/test', 'uniq' => $data->appls_magic]), Yii::$app->urlManager->createAbsoluteUrl(['appl-sh/test', 'uniq' => $data->appls_magic]), ['target' => '_blank']);
                    } else {
                        return '';
                    }

                },
            ],

            'appls_date',
            [
                'attribute' => 'applsPrs.prs_full_name',
                'label' => Yii::t('app', 'Appls Prs ID'),
            ],
            [
                'attribute' => 'applsTrp.trp_name',
                'label' => Yii::t('app', 'Appls Trp ID'),
            ],

            'appls_score_max',
            'appls_score',
            'appls_passed',
            [
                'attribute' => 'applsTrt.trt_name',
                'label' => Yii::t('app', 'Appls Trt ID'),
            ],

            [
                'attribute' => 'appls_file_name',
                'label' => Yii::t('app', 'Appls File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->appls_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->appls_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'appls_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();
*/

?>

