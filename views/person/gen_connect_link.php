<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->prs_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-gen-connect-link">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'View'), ['view', 'id' => $model->prs_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Gen Connect Link'), ['gen-connect-link', 'id' => $model->prs_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to generate connect link for connect?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        $link = \Yii::$app->urlManager->hostInfo . Url::toRoute(['/person/connect-link', 'id' => $model->prs_connect_link]);
        echo Html::a($link, $link, ['target' => '_blank']);
    ?>

</div>
