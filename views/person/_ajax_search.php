<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $id string */
/* @var $data array */

?>

<?= Html::dropDownList($id, isset($data[0]) ? $data[0] : '', $data, ['id' => $id, 'class' => 'form-control']) ?>


