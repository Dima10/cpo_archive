<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-form-connect">

<p>
    Если Вы еще не зарегистрированы на портале электронного обучения, просим обратиться в администрацию ООО "ЦПО" для получения доступа по тел. 8 (925) 017-95-36 или по электронной почте: info@procpo.ru
</p>


    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-2">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
    ?>

    <?= $form->field($model, 'user')->textInput(['autofocus' => true])->label(Yii::t('app', 'User Name')) ?>

    <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('app', 'User Pwd')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
