<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'evt_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evt_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'evt_create_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evt_create_time')->textInput() ?>

    <?= $form->field($model, 'evt_create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evt_update_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evt_update_time')->textInput() ?>

    <?= $form->field($model, 'evt_update_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
