<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SystemMail */


$this->title = Yii::t('app', 'Create System Mail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Mails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-mail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
