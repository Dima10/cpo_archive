<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAct */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Act'),
]) . $model->act_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Acts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->act_id, 'url' => ['view', 'id' => $model->act_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agreement-act-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'agreement' => $agreement,
        'agreement_annex' => $agreement_annex,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
