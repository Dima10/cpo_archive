<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementActSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Agreement Acts');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-act-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Agreement Act'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'entity' => $entity,
        'agreement' => $agreement,
        'agreement_annex' => $agreement_annex,
        'agreement_status' => $agreement_status,
    ]) ?>

    <p>
        <?= Html::a(Yii::t('app', 'Export Agreement Act'), ['export'], ['class' => 'btn btn-success']) ?>
    </p>

</div>

