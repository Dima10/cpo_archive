<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementActSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-act-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'act_id') ?>

    <?= $form->field($model, 'act_number') ?>

    <?= $form->field($model, 'act_agr_id') ?>

    <?= $form->field($model, 'act_agra_id') ?>

    <?= $form->field($model, 'act_sum') ?>

    <?php // echo $form->field($model, 'act_data') ?>

    <?php // echo $form->field($model, 'act_data_sign') ?>

    <?php // echo $form->field($model, 'act_create_user') ?>

    <?php // echo $form->field($model, 'act_create_time') ?>

    <?php // echo $form->field($model, 'act_create_ip') ?>

    <?php // echo $form->field($model, 'act_update_user') ?>

    <?php // echo $form->field($model, 'act_update_time') ?>

    <?php // echo $form->field($model, 'act_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
