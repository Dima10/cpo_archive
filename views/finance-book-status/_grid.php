<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceBookStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="finance-book-status-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'fbs_id',
            'fbs_name',
            'fbs_create_user',
            'fbs_create_ip',
            'fbs_create_time',
            'fbs_update_user',
            'fbs_update_ip',
            'fbs_update_time',

        ],
    ]);

Pjax::end();


?>

</div>
