<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use yii\helpers\Url;

use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<script>
    var distance = 300;
    var x = setInterval(
        function()
        {
            $.get('<?= yii::$app->urlManager->createUrl(['site/time-on']) ?>');

            distance = distance - 1;
            if (distance < 0) {
                clearInterval(x);
            }

        }
        , 60000);
</script>


<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('app', 'My Company'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    try {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
                //['label' => Yii::t('app', 'About'), 'url' => ['/site/about']],
                //['label' => Yii::t('app', 'Contact'), 'url' => ['/site/contact']],

                ['label' => Yii::t('app', 'Core'),
                    'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => [
                        ['label' => Yii::t('app', 'Countries'), 'url' => ['/country']],
                        ['label' => Yii::t('app', 'Regions'), 'url' => ['/region']],
                        ['label' => Yii::t('app', 'Cities'), 'url' => ['/city']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Address Types'), 'url' => ['/address-type']],
                        ['label' => Yii::t('app', 'Addresses'), 'url' => ['/address']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Contact Types'), 'url' => ['/contact-type']],
                        ['label' => Yii::t('app', 'Contacts'), 'url' => ['/contact']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Entity Types'), 'url' => ['/entity-type']],
                        ['label' => Yii::t('app', 'Entity Classes'), 'url' => ['/entity-class']],
                        ['label' => Yii::t('app', 'Entities'), 'url' => ['/entity']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Companies'), 'url' => ['/company']],
                        ['label' => Yii::t('app', 'Persons'), 'url' => ['/person']],
                        ['label' => Yii::t('app', 'Staff'), 'url' => ['/staff']],
                    ],
                ],

                ['label' => Yii::t('app', 'Finance'),
                    'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => [
                        ['label' => Yii::t('app', 'Banks'), 'url' => ['/bank']],
                        ['label' => Yii::t('app', 'Accounts'), 'url' => ['/account']],
                        ['label' => Yii::t('app', 'Agreements'), 'url' => ['/agreement']],
                        ['label' => Yii::t('app', 'Agreement Annexes'), 'url' => ['/agreement-annex']],
                        ['label' => Yii::t('app', 'Agreement Acts'), 'url' => ['/agreement-act']],
                        ['label' => Yii::t('app', 'Agreement Accs'), 'url' => ['/agreement-acc']],
                        ['label' => Yii::t('app', 'Agreement Statuses'), 'url' => ['/agreement-status']],
                        ['label' => Yii::t('app', 'Agreement Adds'), 'url' => ['/agreement-add']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Finance Books'), 'url' => ['/finance-book']],
                        ['label' => Yii::t('app', 'Finance Book Statuses'), 'url' => ['/finance-book-status']],
                        ['label' => Yii::t('app', 'Calendar'), 'url' => ['/calendar']],
                    ],
                ],

                ['label' => Yii::t('app', 'Service'),
                    'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => [
                        ['label' => Yii::t('app', 'Svc Doc Types'), 'url' => ['/svc-doc-type']],
                        ['label' => Yii::t('app', 'Svcs'), 'url' => ['/svc']],
                        ['label' => Yii::t('app', 'Svc Props'), 'url' => ['/svc-prop']],
                        ['label' => Yii::t('app', 'Svc Values'), 'url' => ['/svc-value']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Training Progs'), 'url' => ['/training-prog']],
                        ['label' => Yii::t('app', 'Training Modules'), 'url' => ['/training-module']],
                        ['label' => Yii::t('app', 'Training Questions'), 'url' => ['/training-question']],
                        ['label' => Yii::t('app', 'Training Types'), 'url' => ['/training-type']],
                    ],

                ],

                ['label' => Yii::t('app', 'Documents'),
                    'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => [
                        ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['/appl-request']],
                        ['label' => Yii::t('app', 'Appl Commands'), 'url' => ['/appl-command']],
                        ['label' => Yii::t('app', 'Appl Shs'), 'url' => ['/appl-sh']],
                        ['label' => Yii::t('app', 'Appl Sh Xes'), 'url' => ['/appl-sh-x']],
                        ['label' => Yii::t('app', 'Appl Sheets'), 'url' => ['/appl-sheet']],
                        ['label' => Yii::t('app', 'Appl Sheet Xes'), 'url' => ['/appl-sheet-x']],
                        ['label' => Yii::t('app', 'Appl Xxxes'), 'url' => ['/appl-xxx']],
                        ['label' => Yii::t('app', 'Appl Ends'), 'url' => ['/appl-end']],
                        ['label' => Yii::t('app', 'Appl Finals'), 'url' => ['/appl-final']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Appl Blanks'), 'url' => ['/appl-blank']],
                        ['label' => Yii::t('app', 'Appl Outs'), 'url' => ['/appl-out']],
                    ],

                ],

                ['label' => Yii::t('app', 'Setting'),
                    'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => [
                        //['label' => Yii::t('app', 'Tags'), 'url' => ['/tag']],
                        ['label' => Yii::t('app', 'Pattern Types'), 'url' => ['/pattern-type']],
                        ['label' => Yii::t('app', 'Patterns'), 'url' => ['/pattern']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Appl Mains'), 'url' => ['/appl-main']],
                        ['label' => Yii::t('app', 'Person Visits'), 'url' => ['/person-visit']],
                        '<li class="divider"></li>',
                        ['label' => Yii::t('app', 'Системная почта'), 'url' => ['/system-mail']],
                        ['label' => Yii::t('app', 'Управление уведомлениями'), 'url' => ['/user-mail']],
                    ],

                ],


                ['label' => Yii::t('app', 'acl'), //'url' => ['site/acl'],
                    'visible' => isset(Yii::$app->user->identity) ? Yii::$app->user->identity->level >= 70 : false,
                    'items' => [
                        ['label' => Yii::t('app', 'Users'), 'url' => ['/user']],
                        ['label' => Yii::t('app', 'Ac Roles'), 'url' => ['/ac-role']],
                        //['label' => Yii::t('app', 'Ac Funcs'), 'url' => ['/ac-func']],
                        //['label' => Yii::t('app', 'Ac Role Funcs'), 'url' => ['/ac-role-func']],
                        //['label' => Yii::t('app', 'Ac User Roles'), 'url' => ['/ac-user-role']],
                    ],
                ],


                Yii::$app->user->isGuest ? (
                ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                    . Html::submitButton(
                        Yii::t('app', 'Logout  ({user})', ['user' => Yii::$app->user->identity->username]),
                        ['class' => 'btn btn-link']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    NavBar::end();
    ?>


    <div class="container">

        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">

                <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0)) : ?>

                <?= Html::a(Html::img("@web/img/Company3.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/entity-frm'])); ?><br>
                <?= Html::a(Html::img("@web/img/person2.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/person-frm'])); ?><br>
                <?= Html::a(Html::img("@web/img/kav.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/appl-gen/create-appl'])); ?><br>

                <?php endif; ?>

            </div>

            <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">

                <?php
                try {
                    echo Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]);
                } catch (Exception $e) {
                    echo $e->getMessage();
                } ?>

                <?= Yii::$app->session->getFlash('PersonVisit') ?>

                <?= $content ?>

            </div>

        </div>

<?php
/*

        <table style="float: left; width: 100%;">
            <tr>
                <td style="vertical-align: top;">
                <?= Html::a(Html::img("@web/img/Company3.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/entity-frm'])); ?><br>
                <?= Html::a(Html::img("@web/img/person2.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/person'])); ?><br>
                </td>
                <td style="vertical-align: top;">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>


                <?= $content ?>
                </td>
            </tr>
        </table>
*/
?>

    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::t('app', 'My Company') ?> <?= date('Y') ?></p>
    </div>
</footer>

<img src="/web/img/ajax-loader.gif" id="loading" style="display:none; position:fixed; top:50%; left:50%" />

<?php
$js = <<<EOL
$( document ).ajaxStart(function() {
  $( "#loading" ).show();
}).ajaxStop(function() {
  $( "#loading" ).hide();
});
EOL;

$this->registerJs($js, \yii\web\View::POS_READY);

?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
