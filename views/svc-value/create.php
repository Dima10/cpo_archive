<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SvcValue */
/* @var $svc array */
/* @var $svc_prop array */

$this->title = Yii::t('app', 'Create Svc Value');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-value-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'svc' => $svc,
        'svc_prop' => $svc_prop,
    ]) ?>

</div>
