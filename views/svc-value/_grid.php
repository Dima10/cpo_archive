<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SvcValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="svc-value-index-grid">

<?php Pjax::begin(); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'svcv_id',
            'svcv_svc_id',
            'svcv_svcp_id',
            'svcv_value',
            'svcv_create_user',
            'svcv_create_time',
            'svcv_create_ip',
            'svcv_update_user',
            'svcv_update_time',
            'svcv_update_ip',

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
