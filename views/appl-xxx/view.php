<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplXxx */

$this->title = $model->applxxx_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Xxxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-xxx-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applxxx_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applxxx_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applxxx_id',
            'applxxx_number',
            'applxxx_date',
            'applxxx_create_user',
            'applxxx_create_time',
            'applxxx_create_ip',
            'applxxx_update_user',
            'applxxx_update_time',
            'applxxx_update_ip',
        ],
    ]) ?>

</div>
