<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\Agreement */
/* @var $person array */
/* @var $company array */
/* @var $pattern array */

$this->title = Yii::t('app', 'Create Agreement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($person)[0], 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-frm-agreement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_dyn', [
        'model' => $model,
        'modelA' => $modelA,
        'person' => $person,
        'company' => $company,
        'pattern' => $pattern,
    ]) ?>

</div>
