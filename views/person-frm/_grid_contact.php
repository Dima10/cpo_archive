<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $person app\models\Person */
/* @var $addrbook array */
/* @var $contactType array */

?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Contact'), ['create-contact', 'prs_id' => $person->prs_id], ['class' => 'btn btn-success']) ?>
    </p>


<?php

Pjax::begin(); 
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update}',
                'buttons' => [
                    'update' =>
                        function($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                                ]);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($person){
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-contact', 'prs_id' => $person->prs_id, 'id' => $model->con_id]);
                            return $url;
                            }
                        }
            ],

            [
                'attribute' => 'con_id',
                'options' => ['width' => '100'],
            ],

            [
                'attribute' => 'con_ab_id',
                'label' => Yii::t('app', 'Con Ab ID'),
                'value' => function ($data) { return $data->conAb->ab_name; },
                'filter' => $addrbook,
                'visible' => false,
            ],
            [
                'attribute' => 'con_cont_id',
                'label' => Yii::t('app', 'Con Cont ID'),
                'value' => function ($data) { return $data->conCont->cont_name; },
                'filter' => $contactType,
                'options' => ['width' => '250'],
            ],
            
            'con_text:ntext',
            
            [
                'attribute' => 'con_create_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'con_create_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'con_create_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'con_update_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'con_update_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'con_update_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
        ],
    ]);
Pjax::end();

