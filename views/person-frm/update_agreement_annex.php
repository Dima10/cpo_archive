<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnex */
/* @var $agreement array */
/* @var $person array */
/* @var $account array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Annex'),
]) . $model->agra_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($person)[0], 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="person-frm-agreement-annex-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_annex', [
        'model' => $model,
        'agreement' => $agreement,
        'account' => $account,
        'person' => $person,
        'agreement_status' => $agreement_status,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
