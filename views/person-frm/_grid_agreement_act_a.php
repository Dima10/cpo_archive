<?php

use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="person-frm-agreement-act-a-index-grid">

<?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            'acta_id',
            [
                'attribute' => 'acta_ana_id',
                'label' => Yii::t('app', 'Acta Ana ID'),
                'value' =>  function ($data) {
                                return $data->actaAna->anaAgra->agra_number . ' / ' .$data->actaAna->anaSvc->svc_name .' / '.$data->actaAna->anaTrp->trp_name;
                            },
            ],

            'acta_qty',
            'acta_price',
            [
                'attribute' => 'acta_tax',
                'label' => Yii::t('app', 'Acta Tax'),
                'value' => function ($data) { return isset($data->aca_tax) ? $data->aca_tax : Yii::t('app', 'Without Tax'); },
            ],


            [
                'attribute' => 'acta_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'acta_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);


?>

</div>
