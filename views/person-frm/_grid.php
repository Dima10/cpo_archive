<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


Pjax::begin(); 

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last'),
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{view}',
            ],

            'prs_id',
            'prs_full_name',
            'prs_last_name',
            'prs_first_name',
            'prs_middle_name',
            'prs_pass_sex',
            'prs_birth_date',
            'prs_inn',
            'prs_pass_serial',
            'prs_pass_number',
            'prs_pass_issued_by',
            'prs_pass_date',
            'prs_connect_count',


            [
                'attribute' => 'stf_id',
                'label' => Yii::t('app', 'Contacts'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = '';
                        foreach ($data->contacts as $key => $value) {
                            switch  ($value->conCont->cont_type) {
                                case 0:
                                    $s1 = $value->conCont->cont_name .' '. $value->con_text;
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 1:
                                    $s1 = $value->conCont->cont_name .' '. Html::mailto($value->con_text);
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 2:
                                    $s1 = Html::a($value->conCont->cont_name, 'http://'.str_replace('http://', '', $value->con_text), ['target'=>'_blank']);
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 3:
                                    $s1 = Html::a($value->conCont->cont_name, 'skype:'.$value->con_text.'?call');
                                    $s .= Html::tag('div', $s1);
                                    break;
                                default:
                                    $s1 = $value->conCont->cont_name.' '.$value->con_text;
                                    $s .= Html::tag('div', $s1);
                                    break;
                            }

                        }
                        return $s;

                    },
            ],

            [
                'attribute' => 'prs_create_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'prs_create_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'prs_create_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'prs_update_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'prs_update_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'prs_update_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],

        ],
    ]);

Pjax::end();

