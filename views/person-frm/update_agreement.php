<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agreement */
/* @var $account array */
/* @var $company array */
/* @var $person array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement'),
]) . $model->agr_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($person)[0], 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="person-frm-agreement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement', [
        'model' => $model,
        'person' => $person,
        'account' => $account,
        'company' => $company,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
