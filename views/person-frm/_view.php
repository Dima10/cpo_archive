<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

?>

<div class="person-frm-update">

    <h1><?= Yii::t('app', 'ID') ?> <?= Html::encode($model->prs_id) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
