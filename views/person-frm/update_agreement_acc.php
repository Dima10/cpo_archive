<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAcc */
/* @var $company array */
/* @var $person array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Acc'),
]) . $model->aga_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($person)[0], 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="person-frm-agreement-acc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_acc', [
        'model' => $model,
        'person' => $person,
        'company' => $company,
        'agreement' => $agreement,
        'agreement_annex' => $agreement_annex,
        'agreement_status' => $agreement_status,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
