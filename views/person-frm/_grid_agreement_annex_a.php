<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="person-frm-agreement-annex-a-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            'ana_id',
            //'ana_agra_id',
            [
                'attribute' => 'ana_svc_id',
                'label' => Yii::t('app', 'Ana Svc ID'),
                'value' => function ($data) { return $data->anaSvc->svc_name; },
            ],
            [
                'attribute' => 'ana_trp_id',
                'label' => Yii::t('app', 'Ana Trp ID'),
                'value' => function ($data) { return isset($data->anaTrp) ? $data->anaTrp->trp_name : ''; },
            ],
            
            'ana_cost_a',
            'ana_price_a',
            'ana_qty',
            'ana_price',
            [
                'attribute' => 'ana_tax',
                'label' => Yii::t('app', 'Ana Tax'),
                'value' => function ($data) { return isset($data->ana_tax) ? $data->ana_tax : Yii::t('app', 'Without Tax'); },
            ],


            [
                'attribute' => 'ana_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'ana_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);

Pjax::end();


?>

</div>
