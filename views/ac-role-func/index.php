<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AcRoleFuncSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $acRole app\models\AcRole */

$this->title = Yii::t('app', 'Ac Role Funcs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'acl'), 'url' => ['site/acl']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ac-role-func-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <!--
    <p>
        <?= Html::a(Yii::t('app', 'Create Ac Role Func'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    -->

    <?= DetailView::widget([
        'model' => $acRole,
        'template' => '<tr><th style="width:220px;">{label}</th><td>{value}</td></tr>',
        'attributes' => [
            'acr_id',
            'acr_name',
        ],
    ]) ?>

    <hr>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'acRole' => $acRole,
    ]) ?>

</div>
