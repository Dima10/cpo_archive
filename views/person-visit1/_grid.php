<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonVisit1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="person-visit1-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'pv1_id',
            'pv1_pv_id',
            'pv1_pve_id',
            'pv1_create_user',
            'pv1_create_time',
            'pv1_create_ip',

        ],
    ]);

Pjax::end();


?>

</div>
