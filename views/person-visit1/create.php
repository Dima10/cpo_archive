<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonVisit1 */

$this->title = Yii::t('app', 'Create Person Visit1');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person Visit1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-visit1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
