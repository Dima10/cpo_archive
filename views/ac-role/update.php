<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AcRole */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Ac Role'),
]) . ' ' . $model->acr_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'acl'), 'url' => ['site/acl']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ac Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->acr_id, 'url' => ['view', 'id' => $model->acr_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ac-role-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
