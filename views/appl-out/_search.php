<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOutSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-out-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applout_id') ?>

    <?= $form->field($model, 'applout_number') ?>

    <?= $form->field($model, 'applout_date') ?>

    <?= $form->field($model, 'applout_pat_id') ?>

    <?= $form->field($model, 'applout_file_name') ?>

    <?php // echo $form->field($model, 'applout_file_data') ?>

    <?php // echo $form->field($model, 'applout_create_user') ?>

    <?php // echo $form->field($model, 'applout_create_time') ?>

    <?php // echo $form->field($model, 'applout_create_ip') ?>

    <?php // echo $form->field($model, 'applout_update_user') ?>

    <?php // echo $form->field($model, 'applout_update_time') ?>

    <?php // echo $form->field($model, 'applout_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
