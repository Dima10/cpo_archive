<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOut */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Out'),
]) . $model->applout_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Outs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applout_id, 'url' => ['view', 'id' => $model->applout_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-out-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
