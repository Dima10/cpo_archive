<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Account */
/* @var $ab array */
/* @var $bank array */

$this->title = Yii::t('app', 'Create Account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'entity' => $entity,
        'bank' => $bank,
    ]) ?>

</div>
