<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EntityType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Entity Type'),
]) . $model->entt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->entt_id, 'url' => ['view', 'id' => $model->entt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
