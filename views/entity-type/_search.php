<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EntityTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entity-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'entt_id') ?>

    <?= $form->field($model, 'entt_name') ?>

    <?= $form->field($model, 'entt_name_short') ?>

    <?= $form->field($model, 'entt_create_user') ?>

    <?= $form->field($model, 'entt_create_time') ?>

    <?php // echo $form->field($model, 'entt_create_ip') ?>

    <?php // echo $form->field($model, 'entt_update_user') ?>

    <?php // echo $form->field($model, 'entt_update_time') ?>

    <?php // echo $form->field($model, 'entt_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
