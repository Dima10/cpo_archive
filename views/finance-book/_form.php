<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-book-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);

    ?>

    <?php echo $form->field($model, 'fb_aca_id')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'fb_ab_id')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'fb_comp_id')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'fb_svc_id')->textInput(); ?>

    <?php echo $form->field($model, 'fb_fbt_id')->textInput(); ?>

    <?php echo $form->field($model, 'fb_sum')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'fb_tax')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'fb_aga_id')->textInput(); ?>

    <?php echo $form->field($model, 'fb_acc_id')->textInput(); ?>

    <?php echo $form->field($model, 'fb_fbs_id')->textInput(); ?>

    <?php echo $form->field($model, 'fb_date')->textInput(); ?>

    <?php echo $form->field($model, 'fb_payment')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'fb_comment')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
