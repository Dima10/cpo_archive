<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBook */

$this->title = $model->fb_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Books'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->fb_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->fb_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fb_id',
            'fb_svc_id',
            'fb_fbt_id',
            'fb_sum',
            'fb_tax',
            'fb_aga_id',
            'fb_acc_id',
            'fb_fbs_id',
            'fb_date',
            'fb_payment',
            'fb_comment',
            'fb_create_user',
            'fb_create_ip',
            'fb_create_time',
            'fb_update_user',
            'fb_update_ip',
            'fb_update_time',
        ],
    ]) ?>

</div>
