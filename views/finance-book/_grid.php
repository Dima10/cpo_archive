<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="finance-book-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],

            'fb_id',
            'svc_name',
            //'fbSvc.svc_name',
            [
                'attribute' => 'fbFbt.fbt_name',
                'label' => Yii::t('app', 'Fb Fbt ID'),
            ],
            'ab_name',
            //'fbAb.ab_name',
            [
                'attribute' => 'fbComp.comp_name',
                'label' => Yii::t('app', 'Fb Comp ID'),
            ],
            'fb_sum',
            'fb_tax',
            'aga_number',
            //'fbAga.aga_number',
            'fbAcc.acc_number',
            [
                'attribute' => 'fbFbs.fbs_name',
                'label' => Yii::t('app', 'Fb Fbs ID'),
            ],
            [
                'attribute' => 'fbPt.pt_name',
                'label' => Yii::t('app', 'Fb Pt ID'),
            ],

            'fb_date',
            'fb_payment',
            'fb_comment',

            [
                'attribute' => 'fb_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();


?>

</div>
