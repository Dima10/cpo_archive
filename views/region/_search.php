<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reg_id') ?>

    <?= $form->field($model, 'reg_cou_id') ?>

    <?= $form->field($model, 'reg_name') ?>

    <?= $form->field($model, 'reg_create_user') ?>

    <?= $form->field($model, 'reg_create_time') ?>

    <?php // echo $form->field($model, 'reg_create_ip') ?>

    <?php // echo $form->field($model, 'reg_update_user') ?>

    <?php // echo $form->field($model, 'reg_update_time') ?>

    <?php // echo $form->field($model, 'reg_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
