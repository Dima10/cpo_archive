<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementActA */

$this->title = $model->acta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Act As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-act-a-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->acta_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->acta_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'acta_id',
            'acta_act_id',
            'acta_ana_id',
            'acta_qty',
            'acta_price',
            'acta_tax',
            'acta_create_user',
            'acta_create_time',
            'acta_create_ip',
            'acta_update_user',
            'acta_update_time',
            'acta_update_ip',
        ],
    ]) ?>

</div>
