<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementActASearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="agreement-act-a-index-grid">

<?php


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'acta_id',
            'acta_act_id',
            'acta_ana_id',
            'acta_qty',
            'acta_price',
            'acta_tax',

            [
                'attribute' => 'acta_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);



?>

</div>
