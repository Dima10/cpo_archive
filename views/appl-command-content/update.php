<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommandContent */
/* @var $person array */
/* @var $program array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Command Content'),
]) . $model->applcmdc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Command Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applcmdc_id, 'url' => ['view', 'id' => $model->applcmdc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-command-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'person'  => $person,
        'program' => $program,
    ]) ?>

</div>
