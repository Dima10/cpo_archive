<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'acl'), 'url' => ['site/acl']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            'user_id',
            'user_name',
            //'user_pwd',
            'user_real',
            [
            'label' => Yii::t('app', 'Ac Roles'),
            'format' => 'raw',
            'value' =>  function ($data) {
                            $r = Html::tag('div', Html::a(Yii::t('app', 'Change'), ['ac-user-role/index', 'user_id' => $data->user_id], ['class' => 'btn btn-success']));
                            $val = array();
                            foreach ($data->userRoles as $key => $value) {
                                $val[] = $value->role->acr_name;
                            }
                            $r .= Html::ol($val);
                            return $r;
                        },
            ],
            'user_level',
            'user_authKey',

            // 'user_accessToken',

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
