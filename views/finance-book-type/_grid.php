<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceBookTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="finance-book-type-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'fbt_id',
            'fbt_name',
            'fbt_create_user',
            'fbt_create_ip',
            'fbt_create_time',
            'fbt_update_user',
            'fbt_update_ip',
            'fbt_update_time',

        ],
    ]);

Pjax::end();


?>

</div>
