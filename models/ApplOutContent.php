<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_out_content}}".
 *
 * @property integer $apploutc_id
 * @property integer $apploutc_applout_id
 * @property integer $apploutc_prs_id
 * @property string $apploutc_position
 * @property integer $apploutc_trp_id
 * @property integer $apploutc_ab_id
 * @property integer $apploutc_applcmd_id
 * @property integer $apploutc_applsx_id
 * @property integer $apploutc_apple_id
 * @property string $apploutc_number_upk
 * @property string $apploutc_create_user
 * @property string $apploutc_create_time
 * @property string $apploutc_create_ip
 * @property string $apploutc_update_user
 * @property string $apploutc_update_time
 * @property string $apploutc_update_ip
 *
 * @property Ab $apploutcAb
 * @property Person $apploutcPrs
 * @property ApplOut $apploutcApplout
 * @property TrainingProg $apploutcTrp
 *
 * @property ApplCommand $apploutcApplcmd
 * @property ApplSheetX $apploutcApplsx
 * @property ApplEnd $apploutcApplend
 */
class ApplOutContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_out_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apploutc_applout_id', 'apploutc_prs_id', 'apploutc_trp_id'], 'required'],
            [['apploutc_applout_id', 'apploutc_prs_id', 'apploutc_trp_id', 'apploutc_ab_id', 'apploutc_applcmd_id', 'apploutc_applsx_id', 'apploutc_apple_id'], 'integer'],
            [['apploutc_create_time', 'apploutc_update_time'], 'safe'],
            [['apploutc_position', 'apploutc_number_upk', 'apploutc_create_user', 'apploutc_create_ip', 'apploutc_update_user', 'apploutc_update_ip'], 'string', 'max' => 64],
            [['apploutc_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['apploutc_ab_id' => 'ab_id']],
            [['apploutc_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['apploutc_prs_id' => 'prs_id']],
            [['apploutc_applout_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplOut::class, 'targetAttribute' => ['apploutc_applout_id' => 'applout_id']],
            [['apploutc_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['apploutc_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'apploutc_id' => Yii::t('app', 'Apploutc ID'),
            'apploutc_applout_id' => Yii::t('app', 'Apploutc Applout ID'),
            'apploutc_prs_id' => Yii::t('app', 'Apploutc Prs ID'),
            'apploutc_trp_id' => Yii::t('app', 'Apploutc Trp ID'),
            'apploutc_ab_id' => Yii::t('app', 'Apploutc Ab ID'),
            'apploutc_applcmd_id' => Yii::t('app', 'Apploutc Applcmd ID'),
            'apploutc_applsx_id' => Yii::t('app', 'Apploutc Applsx ID'),
            'apploutc_apple_id' => Yii::t('app', 'Apploutc Apple ID'),
            'apploutc_number_upk' => Yii::t('app', 'Apploutc Number Upk'),
            'apploutc_create_user' => Yii::t('app', 'Apploutc Create User'),
            'apploutc_create_time' => Yii::t('app', 'Apploutc Create Time'),
            'apploutc_create_ip' => Yii::t('app', 'Apploutc Create Ip'),
            'apploutc_update_user' => Yii::t('app', 'Apploutc Update User'),
            'apploutc_update_time' => Yii::t('app', 'Apploutc Update Time'),
            'apploutc_update_ip' => Yii::t('app', 'Apploutc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutcApplend()
    {
        return $this->hasOne(ApplEnd::class, ['apple_id' => 'apploutc_apple_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutcApplsx()
    {
        return $this->hasOne(ApplSheetX::class, ['applsx_id' => 'apploutc_applsx_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutcApplcmd()
    {
        return $this->hasOne(ApplCommand::class, ['applcmd_id' => 'apploutc_applcmd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutcAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'apploutc_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutcPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'apploutc_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutcApplout()
    {
        return $this->hasOne(ApplOut::class, ['applout_id' => 'apploutc_applout_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutcTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'apploutc_trp_id']);
    }

    /**
     * @inheritdoc
     * @return ApplOutContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplOutContentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->apploutc_create_user = Yii::$app->user->identity->username;
            $this->apploutc_create_ip = Yii::$app->request->userIP;
            $this->apploutc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->apploutc_update_user = Yii::$app->user->identity->username;
            $this->apploutc_update_ip = Yii::$app->request->userIP;
            $this->apploutc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
