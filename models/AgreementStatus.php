<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_status}}".
 *
 * @property integer $ast_id
 * @property string $ast_name
 * @property string $ast_create_time
 * @property string $ast_create_ip
 * @property string $ast_update_user
 * @property string $ast_update_time
 * @property string $ast_update_ip
 * @property string $ast_create_user
 *
 * @property Agreement[] $agreements
 * @property AgreementAcc[] $agreementAccs
 * @property AgreementAct[] $agreementActs
 * @property AgreementAnnex[] $agreementAnnexes
 */
class AgreementStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ast_name'], 'required'],
            [['ast_create_time', 'ast_update_time'], 'safe'],
            [['ast_name', 'ast_create_ip', 'ast_update_user', 'ast_update_ip', 'ast_create_user'], 'string', 'max' => 64],
            [['ast_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ast_id' => Yii::t('app', 'Ast ID'),
            'ast_name' => Yii::t('app', 'Ast Name'),
            'ast_create_time' => Yii::t('app', 'Ast Create Time'),
            'ast_create_ip' => Yii::t('app', 'Ast Create Ip'),
            'ast_update_user' => Yii::t('app', 'Ast Update User'),
            'ast_update_time' => Yii::t('app', 'Ast Update Time'),
            'ast_update_ip' => Yii::t('app', 'Ast Update Ip'),
            'ast_create_user' => Yii::t('app', 'Ast Create User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::class, ['agr_ast_id' => 'ast_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAccs()
    {
        return $this->hasMany(AgreementAcc::class, ['aga_ast_id' => 'ast_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementActs()
    {
        return $this->hasMany(AgreementAct::class, ['act_ast_id' => 'ast_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAnnexes()
    {
        return $this->hasMany(AgreementAnnex::class, ['agra_ast_id' => 'ast_id']);
    }

    /**
     * @inheritdoc
     * @return AgreementStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementStatusQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->ast_create_user = Yii::$app->user->identity->username;
            $this->ast_create_ip = Yii::$app->request->userIP;
            $this->ast_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->ast_update_user = Yii::$app->user->identity->username;
            $this->ast_update_ip = Yii::$app->request->userIP;
            $this->ast_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
