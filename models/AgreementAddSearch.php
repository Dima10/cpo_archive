<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2017-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementAdd;

/**
 * AgreementAddSearch represents the model behind the search form about `app\models\AgreementAdd`.
 */
class AgreementAddSearch extends AgreementAdd
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agadd_id', 'agadd_agr_id'], 'integer'],
            [['agadd_number', 'agadd_date', 'agadd_create_user', 'agadd_create_time', 'agadd_create_ip', 'agadd_update_user', 'agadd_update_time', 'agadd_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgreementAdd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'agadd_id' => $this->agadd_id,
            'agadd_date' => $this->agadd_date,
            'agadd_agr_id' => $this->agadd_agr_id,
            'agadd_create_time' => $this->agadd_create_time,
            'agadd_update_time' => $this->agadd_update_time,
        ]);

        $query->andFilterWhere(['like', 'agadd_number', $this->agadd_number])
            ->andFilterWhere(['like', 'agadd_create_user', $this->agadd_create_user])
            ->andFilterWhere(['like', 'agadd_create_ip', $this->agadd_create_ip])
            ->andFilterWhere(['like', 'agadd_update_user', $this->agadd_update_user])
            ->andFilterWhere(['like', 'agadd_update_ip', $this->agadd_update_ip]);

        return $dataProvider;
    }
}
