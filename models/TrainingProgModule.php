<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_prog_module}}".
 *
 * @property integer $trpl_id
 * @property integer $trpl_trm_id
 * @property integer $trpl_trp_id
 * @property string $trpl_create_user
 * @property string $trpl_create_time
 * @property string $trpl_create_ip
 *
 * @property TrainingModule $trplTrm
 * @property TrainingProg $trplTrp
 */
class TrainingProgModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_prog_module}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trpl_trm_id', 'trpl_trp_id'], 'required'],
            [['trpl_trm_id', 'trpl_trp_id'], 'integer'],
            [['trpl_create_time'], 'safe'],
            [['trpl_create_user', 'trpl_create_ip'], 'string', 'max' => 64],
            [['trpl_trm_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingModule::class, 'targetAttribute' => ['trpl_trm_id' => 'trm_id']],
            [['trpl_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['trpl_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trpl_id' => Yii::t('app', 'Trpl ID'),
            'trpl_trm_id' => Yii::t('app', 'Trpl Trm ID'),
            'trpl_trp_id' => Yii::t('app', 'Trpl Trp ID'),
            'trpl_create_user' => Yii::t('app', 'Trpl Create User'),
            'trpl_create_time' => Yii::t('app', 'Trpl Create Time'),
            'trpl_create_ip' => Yii::t('app', 'Trpl Create Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrplTrm()
    {
        return $this->hasOne(TrainingModule::class, ['trm_id' => 'trpl_trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrplTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'trpl_trp_id']);
    }

    /**
     * @inheritdoc
     * @return TrainingProgModuleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingProgModuleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->trpl_create_user = Yii::$app->user->identity->username;
            $this->trpl_create_ip = Yii::$app->request->userIP;
            $this->trpl_create_time = date('Y-m-d H:i:s');
            return true;
        }
        return true;
    }

}
