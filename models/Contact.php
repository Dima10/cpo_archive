<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property integer $con_id
 * @property integer $con_ab_id
 * @property integer $con_cont_id
 * @property string $con_text
 * @property string $con_create_user
 * @property string $con_create_time
 * @property string $con_create_ip
 * @property string $con_update_user
 * @property string $con_update_time
 * @property string $con_update_ip
 *
 * @property ContactType $conCont
 * @property Ab $conAb
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['con_ab_id', 'con_cont_id'], 'required'],
            [['con_ab_id', 'con_cont_id'], 'integer'],
            [['con_text'], 'string'],
            [['con_create_time', 'con_update_time'], 'safe'],
            [['con_create_user', 'con_create_ip', 'con_update_user', 'con_update_ip'], 'string', 'max' => 64],
            [['con_cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactType::class, 'targetAttribute' => ['con_cont_id' => 'cont_id']],
            [['con_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['con_ab_id' => 'ab_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'con_id' => Yii::t('app', 'Con ID'),
            'con_ab_id' => Yii::t('app', 'Con Ab ID'),
            'con_cont_id' => Yii::t('app', 'Con Cont ID'),
            'con_text' => Yii::t('app', 'Con Text'),
            'con_create_user' => Yii::t('app', 'Create User'),
            'con_create_time' => Yii::t('app', 'Create Time'),
            'con_create_ip' => Yii::t('app', 'Create Ip'),
            'con_update_user' => Yii::t('app', 'Update User'),
            'con_update_time' => Yii::t('app', 'Update Time'),
            'con_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConCont()
    {
        return $this->hasOne(ContactType::class, ['cont_id' => 'con_cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'con_ab_id']);
    }

    /**
     * @inheritdoc
     * @return ContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContactQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->con_create_user = Yii::$app->user->identity->username;
            $this->con_create_ip = Yii::$app->request->userIP;
            $this->con_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->con_update_user = Yii::$app->user->identity->username;
            $this->con_update_ip = Yii::$app->request->userIP;
            $this->con_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
