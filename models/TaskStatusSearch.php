<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaskStatus;

/**
 * TaskStatusSearch represents the model behind the search form about `app\models\TaskStatus`.
 */
class TaskStatusSearch extends TaskStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tasks_id'], 'integer'],
            [['tasks_name', 'tasks_note', 'tasks_create_user', 'tasks_create_time', 'tasks_create_ip', 'tasks_update_user', 'tasks_update_time', 'tasks_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskStatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tasks_id' => $this->tasks_id,
            'tasks_create_time' => $this->tasks_create_time,
            'tasks_update_time' => $this->tasks_update_time,
        ]);

        $query->andFilterWhere(['like', 'tasks_name', $this->tasks_name])
            ->andFilterWhere(['like', 'tasks_note', $this->tasks_note])
            ->andFilterWhere(['like', 'tasks_create_user', $this->tasks_create_user])
            ->andFilterWhere(['like', 'tasks_create_ip', $this->tasks_create_ip])
            ->andFilterWhere(['like', 'tasks_update_user', $this->tasks_update_user])
            ->andFilterWhere(['like', 'tasks_update_ip', $this->tasks_update_ip]);

        return $dataProvider;
    }
}
