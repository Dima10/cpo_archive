<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingModulePerson;

/**
 * TrainingModulePersonSearch represents the model behind the search form about `app\models\TrainingModulePerson`.
 */
class TrainingModulePersonSearch extends TrainingModulePerson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tmp_id', 'tmp_prs_id', 'tmp_trm_id'], 'integer'],
            [['tmp_create_user', 'tmp_create_time', 'tmp_create_ip', 'tmp_update_user', 'tmp_update_time', 'tmp_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingModulePerson::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tmp_id' => $this->tmp_id,
            'tmp_prs_id' => $this->tmp_prs_id,
            'tmp_trm_id' => $this->tmp_trm_id,
            'tmp_create_time' => $this->tmp_create_time,
            'tmp_update_time' => $this->tmp_update_time,
        ]);

        $query->andFilterWhere(['like', 'tmp_create_user', $this->tmp_create_user])
            ->andFilterWhere(['like', 'tmp_create_ip', $this->tmp_create_ip])
            ->andFilterWhere(['like', 'tmp_update_user', $this->tmp_update_user])
            ->andFilterWhere(['like', 'tmp_update_ip', $this->tmp_update_ip]);

        return $dataProvider;
    }
}
