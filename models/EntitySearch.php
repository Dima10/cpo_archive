<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Entity;

/**
 * EntitySearch represents the model behind the search form about `app\models\Entity`.
 */
class EntitySearch extends Entity
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['ent_id', 'ent_entt_id', 'entc_id', 'ent_agent_id'],
                'integer'
            ],
            [
                [
                'ent_name', 'ent_name_short', 'ent_orgn', 'ent_inn', 'ent_kpp', 'ent_okpo',
                'ent_create_user', 'ent_create_time', 'ent_create_ip', 'ent_update_user', 'ent_update_time', 'ent_update_ip'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Entity::find()->leftJoin(EntityClassLnk::tableName(), 'ent_id = entcl_ent_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ent_id' => $this->ent_id,
            'entcl_entc_id' => $this->entc_id,
            'ent_entt_id' => $this->ent_entt_id,
            'ent_agent_id' => $this->ent_agent_id,
        ]);

        $query
            ->andFilterWhere(['like', 'ent_name', $this->ent_name])
            ->andFilterWhere(['like', 'ent_name_short', $this->ent_name_short])
            ->andFilterWhere(['like', 'ent_orgn', $this->ent_orgn])
            ->andFilterWhere(['like', 'ent_inn', $this->ent_inn])
            ->andFilterWhere(['like', 'ent_kpp', $this->ent_kpp])
            ->andFilterWhere(['like', 'ent_okpo', $this->ent_okpo])
            ->andFilterWhere(['like', 'ent_create_user', $this->ent_create_user])
            ->andFilterWhere(['like', 'ent_create_time', $this->ent_create_time])
            ->andFilterWhere(['like', 'ent_create_ip', $this->ent_create_ip])
            ->andFilterWhere(['like', 'ent_update_user', $this->ent_update_user])
            ->andFilterWhere(['like', 'ent_update_time', $this->ent_update_time])
            ->andFilterWhere(['like', 'ent_update_ip', $this->ent_update_ip])
        ;

        return $dataProvider;
    }
}
