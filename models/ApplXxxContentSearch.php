<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplXxxContent;

/**
 * ApplXxxContentSearch represents the model behind the search form about `app\models\ApplXxxContent`.
 */
class ApplXxxContentSearch extends ApplXxxContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applxxxc_id', 'applxxxc_applxxx_id', 'applxxxc_prs_id', 'applxxxc_ab_id', 'applxxxc_trp_id', 'applxxxc_passed'], 'integer'],
            [['applxxxc_create_user', 'applxxxc_create_time', 'applxxxc_create_ip', 'applxxxc_update_user', 'applxxxc_update_time', 'applxxxc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplXxxContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applxxxc_id' => $this->applxxxc_id,
            'applxxxc_applxxx_id' => $this->applxxxc_applxxx_id,
            'applxxxc_prs_id' => $this->applxxxc_prs_id,
            'applxxxc_ab_id' => $this->applxxxc_ab_id,
            'applxxxc_trp_id' => $this->applxxxc_trp_id,
            'applxxxc_passed' => $this->applxxxc_passed,
            'applxxxc_create_time' => $this->applxxxc_create_time,
            'applxxxc_update_time' => $this->applxxxc_update_time,
        ]);

        $query->andFilterWhere(['like', 'applxxxc_create_user', $this->applxxxc_create_user])
            ->andFilterWhere(['like', 'applxxxc_create_ip', $this->applxxxc_create_ip])
            ->andFilterWhere(['like', 'applxxxc_update_user', $this->applxxxc_update_user])
            ->andFilterWhere(['like', 'applxxxc_update_ip', $this->applxxxc_update_ip]);

        return $dataProvider;
    }
}
