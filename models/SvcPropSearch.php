<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SvcProp;

/**
 * SvcPropSearch represents the model behind the search form about `app\models\SvcProp`.
 */
class SvcPropSearch extends SvcProp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svcp_id'], 'integer'],
            [['svcp_name', 'svcp_create_user', 'svcp_create_time', 'svcp_create_ip', 'svcp_update_user', 'svcp_update_time', 'svcp_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcProp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'svcp_id' => $this->svcp_id,
            'svcp_create_time' => $this->svcp_create_time,
            'svcp_update_time' => $this->svcp_update_time,
        ]);

        $query->andFilterWhere(['like', 'svcp_name', $this->svcp_name])
            ->andFilterWhere(['like', 'svcp_create_user', $this->svcp_create_user])
            ->andFilterWhere(['like', 'svcp_create_ip', $this->svcp_create_ip])
            ->andFilterWhere(['like', 'svcp_update_user', $this->svcp_update_user])
            ->andFilterWhere(['like', 'svcp_update_ip', $this->svcp_update_ip]);

        return $dataProvider;
    }
}
