<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Company;

/**
 * CompanySearch represents the model behind the search form about `app\models\Company`.
 */
class CompanySearch extends Company
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_id', 'comp_ent_id'], 'integer'],
            [['comp_name', 'comp_note', 'comp_create_user', 'comp_create_time', 'comp_create_ip', 'comp_update_user', 'comp_update_time', 'comp_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'comp_id' => $this->comp_id,
            'comp_ent_id' => $this->comp_ent_id,
        ]);

        $query
            ->andFilterWhere(['like', 'comp_name', $this->comp_name])
            ->andFilterWhere(['like', 'comp_note', $this->comp_note])
            ->andFilterWhere(['like', 'comp_create_user', $this->comp_create_user])
            ->andFilterWhere(['like', 'comp_create_time', $this->comp_create_time])
            ->andFilterWhere(['like', 'comp_create_ip', $this->comp_create_ip])
            ->andFilterWhere(['like', 'comp_update_user', $this->comp_update_user])
            ->andFilterWhere(['like', 'comp_update_time', $this->comp_update_time])
            ->andFilterWhere(['like', 'comp_update_ip', $this->comp_update_ip])
            ;

        return $dataProvider;
    }
}
