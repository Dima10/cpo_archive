<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_acc_a}}".
 *
 * @property integer $aca_id
 * @property integer $aca_ana_id
 * @property integer $aca_aga_id
 * @property integer $aca_qty
 * @property string $aca_price
 * @property string $aca_tax
 * @property string $aca_create_user
 * @property string $aca_create_time
 * @property string $aca_create_ip
 * @property string $aca_update_user
 * @property string $aca_update_time
 * @property string $aca_update_ip
 *
 * @property AgreementAnnexA $acaAna
 * @property AgreementAcc $acaAga
 * @property FinanceBook[] $financeBooks
 * @property string $paySum
 */
class AgreementAccA extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_acc_a}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aca_aga_id', 'aca_ana_id'], 'required'],
            [['aca_aga_id', 'aca_ana_id', 'aca_qty'], 'integer'],
            [['aca_price', 'aca_tax'], 'number'],
            [['aca_create_time', 'aca_update_time'], 'safe'],
            [['aca_create_user', 'aca_create_ip', 'aca_update_user', 'aca_update_ip'], 'string', 'max' => 64],
            [['aca_ana_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAnnexA::class, 'targetAttribute' => ['aca_ana_id' => 'ana_id']],
            [['aca_aga_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAcc::class, 'targetAttribute' => ['aca_aga_id' => 'aga_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aca_id' => Yii::t('app', 'Aca ID'),
            'aca_aga_id' => Yii::t('app', 'Aca Aga ID'),
            'aca_ana_id' => Yii::t('app', 'Aca Ana ID'),
            'aca_qty' => Yii::t('app', 'Aca Qty'),
            'aca_price' => Yii::t('app', 'Aca Price'),
            'aca_tax' => Yii::t('app', 'Aca Tax'),
            'aca_create_user' => Yii::t('app', 'Aca Create User'),
            'aca_create_time' => Yii::t('app', 'Aca Create Time'),
            'aca_create_ip' => Yii::t('app', 'Aca Create Ip'),
            'aca_update_user' => Yii::t('app', 'Aca Update User'),
            'aca_update_time' => Yii::t('app', 'Aca Update Time'),
            'aca_update_ip' => Yii::t('app', 'Aca Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcaAna()
    {
        return $this->hasOne(AgreementAnnexA::class, ['ana_id' => 'aca_ana_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcaAga()
    {
        return $this->hasOne(AgreementAcc::class, ['aga_id' => 'aca_aga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBooks()
    {
        return $this->hasMany(FinanceBook::class, ['fb_aca_id' => 'aca_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaySum()
    {
        return FinanceBook::find()->where(['fb_aca_id' => $this->aca_id])->sum('fb_pay');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSum()
    {
        return FinanceBook::find()->where(['fb_aca_id' => $this->aca_id])->sum('fb_sum');
    }

    /**
     * Update linked FinanceBook object
     * @param boolean $reset Recreate FinanceBook object
     * @return boolean
     */
    public function financeBookUpdate($reset = true) {

        if ($reset) {
            if (!$fb = FinanceBook::find()->where(['fb_aca_id' => $this->aca_id])->one()) {
                $fb = new FinanceBook();
            }
            $fb->fb_svc_id = $this->acaAna->ana_svc_id;
            $fb->fb_aca_id = $this->aca_id;
            $fb->fb_fbt_id = 1;
            $fb->fb_pt_id = 1;
            $fb->fb_ab_id = $this->acaAga->aga_ab_id;
            $fb->fb_comp_id = $this->acaAga->aga_comp_id;
            $fb->fb_sum = $this->aca_price * $this->aca_qty;
            $fb->fb_tax = $this->aca_tax * $this->aca_qty;
            $fb->fb_aga_id = $this->aca_aga_id;
            $fb->fb_acc_id = $this->acaAga->agaAgr->agr_acc_id;
            $fb->fb_fbs_id = 1;
            $fb->fb_date = $this->acaAga->aga_date;
            return $fb->save();
        } else {
            if ($fb = FinanceBook::find()->where(['fb_aca_id' => $this->aca_id])->one()) {
                $fb->fb_svc_id = $this->acaAna->ana_svc_id;
                $fb->fb_aca_id = $this->aca_id;
                $fb->fb_ab_id = $this->acaAga->aga_ab_id;
                $fb->fb_comp_id = $this->acaAga->aga_comp_id;
                $fb->fb_sum = $this->aca_price * $this->aca_qty;
                $fb->fb_tax = $this->aca_tax * $this->aca_qty;
                $fb->fb_aga_id = $this->aca_aga_id;
                $fb->fb_acc_id = $this->acaAga->agaAgr->agr_acc_id;
                $fb->fb_date = $this->acaAga->aga_date;
                return $fb->save();
            } else {
                return $this->financeBookUpdate(true);
            }
        }
    }

    /**
     * @inheritdoc
     * @return AgreementAccAQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementAccAQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if ($insert) {
            $this->aca_create_user = Yii::$app->user->identity->username;
            $this->aca_create_ip = Yii::$app->request->userIP;
            $this->aca_create_time = date('Y-m-d H:i:s');
            return true;
        } else {

            $this->aca_update_user = Yii::$app->user->identity->username;
            $this->aca_update_ip = Yii::$app->request->userIP;
            $this->aca_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // Insert|Update a linked FinanceBook record
        if (!$fb = FinanceBook::find()->where(['fb_aca_id' => $this->aca_id])->one()) {
            $fb = new FinanceBook();
        }
        $fb->fb_svc_id = $this->acaAna->ana_svc_id;
        $fb->fb_aca_id = $this->aca_id;
        $fb->fb_fbt_id = 1;
        $fb->fb_pt_id = 1;
        $fb->fb_ab_id = $this->acaAga->aga_ab_id;
        $fb->fb_comp_id = $this->acaAga->aga_comp_id;
        $fb->fb_sum = $this->aca_price * $this->aca_qty;
        $fb->fb_tax = $this->aca_tax * $this->aca_qty;
        $fb->fb_aga_id = $this->aca_aga_id;
        $fb->fb_acc_id = $this->acaAga->agaAgr->agr_acc_id;
        $fb->fb_fbs_id = 1;
        $fb->fb_date = $this->acaAga->aga_date;
        $fb->save();

        /*
        $fb->fb_payment
        $fb->fb_comment
        */

    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach (FinanceBook::find()->where(['fb_aga_id' => $this->aca_aga_id])->all() as $key => $value) {
                $value->delete();
            }
            return true;
        } else {
            return false;
        }
    }


}
