<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementActA;

/**
 * AgreementActASearch represents the model behind the search form about `app\models\AgreementActA`.
 */
class AgreementActASearch extends AgreementActA
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acta_id', 'acta_act_id', 'acta_ana_id', 'acta_qty'], 'integer'],
            [['acta_price', 'acta_tax'], 'number'],
            [['acta_create_user', 'acta_create_time', 'acta_create_ip', 'acta_update_user', 'acta_update_time', 'acta_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgreementActA::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'acta_id' => $this->acta_id,
            'acta_act_id' => $this->acta_act_id,
            'acta_ana_id' => $this->acta_ana_id,
            'acta_qty' => $this->acta_qty,
            'acta_price' => $this->acta_price,
            'acta_tax' => $this->acta_tax,
            'acta_create_time' => $this->acta_create_time,
            'acta_update_time' => $this->acta_update_time,
        ]);

        $query->andFilterWhere(['like', 'acta_create_user', $this->acta_create_user])
            ->andFilterWhere(['like', 'acta_create_ip', $this->acta_create_ip])
            ->andFilterWhere(['like', 'acta_update_user', $this->acta_update_user])
            ->andFilterWhere(['like', 'acta_update_ip', $this->acta_update_ip]);

        return $dataProvider;
    }
}
