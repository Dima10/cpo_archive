<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplXxx;

/**
 * ApplXxxSearch represents the model behind the search form about `app\models\ApplXxx`.
 */
class ApplXxxSearch extends ApplXxx
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applxxx_id', 'applxxx_trt_id'], 'integer'],
            [['applxxx_number', 'applxxx_date', 'applxxx_create_user', 'applxxx_create_time', 'applxxx_create_ip', 'applxxx_update_user', 'applxxx_update_time', 'applxxx_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplXxx::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applxxx_id' => $this->applxxx_id,
            'applxxx_trt_id' => $this->applxxx_trt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'applxxx_date', $this->applxxx_date])
            ->andFilterWhere(['like', 'applxxx_create_time', $this->applxxx_create_time])
            ->andFilterWhere(['like', 'applxxx_update_time', $this->applxxx_update_time])
            ->andFilterWhere(['like', 'applxxx_number', $this->applxxx_number])
            ->andFilterWhere(['like', 'applxxx_create_user', $this->applxxx_create_user])
            ->andFilterWhere(['like', 'applxxx_create_ip', $this->applxxx_create_ip])
            ->andFilterWhere(['like', 'applxxx_update_user', $this->applxxx_update_user])
            ->andFilterWhere(['like', 'applxxx_update_ip', $this->applxxx_update_ip])
        ;

        return $dataProvider;
    }
}
