<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Svc;

/**
 * SvcSearch represents the model behind the search form about `app\models\Svc`.
 */
class SvcSearch extends Svc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svc_id', 'svc_svdt_id', 'svc_hour', 'svc_tax_flag', 'svc_payment_flag'], 'integer'],
            [['svc_name', 'svc_skill', 'svc_create_user', 'svc_create_time', 'svc_create_ip', 'svc_update_user', 'svc_update_time', 'svc_update_ip'], 'safe'],
            [['svc_price', 'svc_cost', 'svc_cost_ext'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Svc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'svc_id' => $this->svc_id,
            'svc_svdt_id' => $this->svc_svdt_id,
            'svc_hour' => $this->svc_hour,
            'svc_tax_flag' => $this->svc_tax_flag,
            'svc_payment_flag' => $this->svc_payment_flag,
        ]);

        $query
            ->andFilterWhere(['like', 'svc_price', $this->svc_price])
            ->andFilterWhere(['like', 'svc_cost', $this->svc_cost])
            ->andFilterWhere(['like', 'svc_cost_ext', $this->svc_cost_ext])
            ->andFilterWhere(['like', 'svc_name', $this->svc_name])
            ->andFilterWhere(['like', 'svc_skill', $this->svc_skill])
            ->andFilterWhere(['like', 'svc_create_user', $this->svc_create_user])
            ->andFilterWhere(['like', 'svc_create_time', $this->svc_create_time])
            ->andFilterWhere(['like', 'svc_create_ip', $this->svc_create_ip])
            ->andFilterWhere(['like', 'svc_update_user', $this->svc_update_user])
            ->andFilterWhere(['like', 'svc_update_time', $this->svc_update_time])
            ->andFilterWhere(['like', 'svc_update_ip', $this->svc_update_ip]);

        return $dataProvider;
    }
}
