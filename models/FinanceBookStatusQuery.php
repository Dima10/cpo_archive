<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FinanceBookStatus]].
 *
 * @see FinanceBookStatus
 */
class FinanceBookStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FinanceBookStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FinanceBookStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
