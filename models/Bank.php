<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%bank}}".
 *
 * @property integer $bank_id
 * @property string $bank_name
 * @property string $bank_bic
 * @property string $bank_account
 * @property string $bank_create_user
 * @property string $bank_create_time
 * @property string $bank_create_ip
 * @property string $bank_update_user
 * @property string $bank_update_time
 * @property string $bank_update_ip
 *
 * @property Account[] $accounts
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bank}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name', 'bank_bic'], 'required'],
            [['bank_create_time', 'bank_update_time'], 'safe'],
            [['bank_name'], 'string', 'max' => 128],
            [['bank_bic'], 'string', 'max' => 16],
            [['bank_account'], 'string', 'max' => 32],
            [['bank_create_user', 'bank_create_ip', 'bank_update_user', 'bank_update_ip'], 'string', 'max' => 64],
            [['bank_bic'], 'unique'],
            //[['bank_account'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bank_id' => Yii::t('app', 'Bank ID'),
            'bank_name' => Yii::t('app', 'Bank Name'),
            'bank_bic' => Yii::t('app', 'Bank Bic'),
            'bank_account' => Yii::t('app', 'Bank Account'),
            'bank_create_user' => Yii::t('app', 'Bank Create User'),
            'bank_create_time' => Yii::t('app', 'Bank Create Time'),
            'bank_create_ip' => Yii::t('app', 'Bank Create Ip'),
            'bank_update_user' => Yii::t('app', 'Bank Update User'),
            'bank_update_time' => Yii::t('app', 'Bank Update Time'),
            'bank_update_ip' => Yii::t('app', 'Bank Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::class, ['acc_bank_id' => 'bank_id']);
    }

    /**
     * @inheritdoc
     * @return BankQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BankQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->bank_create_user = Yii::$app->user->identity->username;
            $this->bank_create_ip = Yii::$app->request->userIP;
            $this->bank_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->bank_update_user = Yii::$app->user->identity->username;
            $this->bank_update_ip = Yii::$app->request->userIP;
            $this->bank_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
