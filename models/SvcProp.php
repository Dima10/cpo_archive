<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%svc_prop}}".
 *
 * @property integer $svcp_id
 * @property string $svcp_name
 * @property string $svcp_create_user
 * @property string $svcp_create_time
 * @property string $svcp_create_ip
 * @property string $svcp_update_user
 * @property string $svcp_update_time
 * @property string $svcp_update_ip
 *
 * @property SvcValue[] $svcValues
 */
class SvcProp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_prop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svcp_name'], 'required'],
            [['svcp_create_time', 'svcp_update_time'], 'safe'],
            [['svcp_name', 'svcp_create_user', 'svcp_create_ip', 'svcp_update_user', 'svcp_update_ip'], 'string', 'max' => 64],
            [['svcp_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'svcp_id' => Yii::t('app', 'Svcp ID'),
            'svcp_name' => Yii::t('app', 'Svcp Name'),
            'svcp_create_user' => Yii::t('app', 'Svcp Create User'),
            'svcp_create_time' => Yii::t('app', 'Svcp Create Time'),
            'svcp_create_ip' => Yii::t('app', 'Svcp Create Ip'),
            'svcp_update_user' => Yii::t('app', 'Svcp Update User'),
            'svcp_update_time' => Yii::t('app', 'Svcp Update Time'),
            'svcp_update_ip' => Yii::t('app', 'Svcp Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcValues()
    {
        return $this->hasMany(SvcValue::class, ['svcv_svcp_id' => 'svcp_id']);
    }

    /**
     * @inheritdoc
     * @return SvcPropQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcPropQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->svcp_create_user = Yii::$app->user->identity->username;
            $this->svcp_create_ip = Yii::$app->request->userIP;
            $this->svcp_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->svcp_update_user = Yii::$app->user->identity->username;
            $this->svcp_update_ip = Yii::$app->request->userIP;
            $this->svcp_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
