<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%event_result_type}}".
 *
 * @property integer $evrt_id
 * @property string $evrt_name
 * @property string $evrt_note
 * @property string $evrt_create_user
 * @property string $evrt_create_time
 * @property string $evrt_create_ip
 * @property string $evrt_update_user
 * @property string $evrt_update_time
 * @property string $evrt_update_ip
 *
 * @property Event[] $events
 */
class EventResultType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event_result_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['evrt_name'], 'required'],
            [['evrt_note'], 'string'],
            [['evrt_create_time', 'evrt_update_time'], 'safe'],
            [['evrt_name', 'evrt_create_user', 'evrt_create_ip', 'evrt_update_user', 'evrt_update_ip'], 'string', 'max' => 64],
            [['evrt_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'evrt_id' => Yii::t('app', 'Evrt ID'),
            'evrt_name' => Yii::t('app', 'Evrt Name'),
            'evrt_note' => Yii::t('app', 'Evrt Note'),
            'evrt_create_user' => Yii::t('app', 'Evrt Create User'),
            'evrt_create_time' => Yii::t('app', 'Evrt Create Time'),
            'evrt_create_ip' => Yii::t('app', 'Evrt Create Ip'),
            'evrt_update_user' => Yii::t('app', 'Evrt Update User'),
            'evrt_update_time' => Yii::t('app', 'Evrt Update Time'),
            'evrt_update_ip' => Yii::t('app', 'Evrt Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::class, ['event_evrt_id' => 'evrt_id']);
    }

    /**
     * @inheritdoc
     * @return EventResultTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventResultTypeQuery(get_called_class());
    }
}
