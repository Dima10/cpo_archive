<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%svc_doc_type}}".
 *
 * @property integer $svdt_id
 * @property string $svdt_name
 * @property string $svdt_create_user
 * @property string $svdt_create_time
 * @property string $svdt_create_ip
 * @property string $svdt_update_user
 * @property string $svdt_update_time
 * @property string $svdt_update_ip
 *
 * @property Svc[] $svcs
 */
class SvcDocType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_doc_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svdt_name'], 'required'],
            [['svdt_create_time', 'svdt_update_time'], 'safe'],
            [['svdt_name'], 'string', 'max' => 128],
            [['svdt_create_user', 'svdt_create_ip', 'svdt_update_user', 'svdt_update_ip'], 'string', 'max' => 64],
            [['svdt_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'svdt_id' => Yii::t('app', 'Svdt ID'),
            'svdt_name' => Yii::t('app', 'Svdt Name'),
            'svdt_create_user' => Yii::t('app', 'Svdt Create User'),
            'svdt_create_time' => Yii::t('app', 'Svdt Create Time'),
            'svdt_create_ip' => Yii::t('app', 'Svdt Create Ip'),
            'svdt_update_user' => Yii::t('app', 'Svdt Update User'),
            'svdt_update_time' => Yii::t('app', 'Svdt Update Time'),
            'svdt_update_ip' => Yii::t('app', 'Svdt Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcs()
    {
        return $this->hasMany(Svc::class, ['svc_svdt_id' => 'svdt_id']);
    }

    /**
     * @inheritdoc
     * @return SvcDocTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcDocTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->svdt_create_user = Yii::$app->user->identity->username;
            $this->svdt_create_ip = Yii::$app->request->userIP;
            $this->svdt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->svdt_update_user = Yii::$app->user->identity->username;
            $this->svdt_update_ip = Yii::$app->request->userIP;
            $this->svdt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
