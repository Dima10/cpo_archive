<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingProgModule;

/**
 * TrainingProgModuleSearch represents the model behind the search form about `app\models\TrainingProgModule`.
 */
class TrainingProgModuleSearch extends TrainingProgModule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trpl_id', 'trpl_trm_id', 'trpl_trp_id'], 'integer'],
            [['trpl_create_user', 'trpl_create_time', 'trpl_create_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingProgModule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trpl_id' => $this->trpl_id,
            'trpl_trm_id' => $this->trpl_trm_id,
            'trpl_trp_id' => $this->trpl_trp_id,
        ]);

        $query
            ->andFilterWhere(['like', 'trpl_create_user', $this->trpl_create_user])
            ->andFilterWhere(['like', 'trpl_create_time', $this->trpl_create_time])
            ->andFilterWhere(['like', 'trpl_create_ip', $this->trpl_create_ip])
            ;

        return $dataProvider;
    }
}
