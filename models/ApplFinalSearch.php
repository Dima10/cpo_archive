<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplFinal;

/**
 * ApplFinalSearch represents the model behind the search form about `app\models\ApplFinal`.
 */
class ApplFinalSearch extends ApplFinal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applf_reestr', 'applf_id', 'applf_svdt_id', 'applf_trt_id', 'applf_trp_id', 'applf_trp_hour'], 'integer'],
            [['applf_name_first', 'applf_name_last', 'applf_name_middle', 'applf_name_full', 'applf_cmd_date', 'applf_end_date', 'applf_number', 'applf_file_name',  'applf_file0_name', 'applf_create_user', 'applf_create_time', 'applf_create_ip', 'applf_update_user', 'applf_update_time', 'applf_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        if (!is_null($id)) {
            $query = ApplFinal::find()->leftJoin(ApplRequest::tableName(), 'applf_applr_id = applr_id')->where(['applr_ab_id' => $id]);
        } else {
            $query = ApplFinal::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applf_id' => $this->applf_id,
            'applf_reestr' => $this->applf_reestr,
            'applf_svdt_id' => $this->applf_svdt_id,
            'applf_trt_id' => $this->applf_trt_id,
            'applf_trp_id' => $this->applf_trp_id,
        ]);

        $query
            ->andFilterWhere(['like', 'applf_trp_hour', $this->applf_trp_hour])
            ->andFilterWhere(['like', 'applf_cmd_date', $this->applf_cmd_date])
            ->andFilterWhere(['like', 'applf_end_date', $this->applf_end_date])
            ->andFilterWhere(['like', 'applf_create_time', $this->applf_create_time])
            ->andFilterWhere(['like', 'applf_update_time', $this->applf_update_time])

            ->andFilterWhere(['like', 'applf_name_first', $this->applf_name_first])
            ->andFilterWhere(['like', 'applf_name_last', $this->applf_name_last])
            ->andFilterWhere(['like', 'applf_name_middle', $this->applf_name_middle])
            ->andFilterWhere(['like', 'applf_name_full', $this->applf_name_full])
            ->andFilterWhere(['like', 'applf_number', $this->applf_number])
            ->andFilterWhere(['like', 'applf_file_name', $this->applf_file_name])
            ->andFilterWhere(['like', 'applf_file0_name', $this->applf_file0_name])
            ->andFilterWhere(['like', 'applf_create_user', $this->applf_create_user])
            ->andFilterWhere(['like', 'applf_create_ip', $this->applf_create_ip])
            ->andFilterWhere(['like', 'applf_update_user', $this->applf_update_user])
            ->andFilterWhere(['like', 'applf_update_ip', $this->applf_update_ip]);

        return $dataProvider;
    }
}
