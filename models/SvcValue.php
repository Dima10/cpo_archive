<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%svc_value}}".
 *
 * @property integer $svcv_id
 * @property integer $svcv_svc_id
 * @property integer $svcv_svcp_id
 * @property string $svcv_value
 * @property string $svcv_create_user
 * @property string $svcv_create_time
 * @property string $svcv_create_ip
 * @property string $svcv_update_user
 * @property string $svcv_update_time
 * @property string $svcv_update_ip
 *
 * @property Svc $svcvSvc
 * @property SvcProp $svcvSvcp
 */
class SvcValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svcv_svc_id', 'svcv_svcp_id', 'svcv_value'], 'required'],
            [['svcv_svc_id', 'svcv_svcp_id'], 'integer'],
            [['svcv_create_time', 'svcv_update_time'], 'safe'],
            [['svcv_value'], 'string', 'max' => 256],
            [['svcv_create_user', 'svcv_create_ip', 'svcv_update_user', 'svcv_update_ip'], 'string', 'max' => 64],
            [['svcv_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['svcv_svc_id' => 'svc_id']],
            [['svcv_svcp_id'], 'exist', 'skipOnError' => true, 'targetClass' => SvcProp::class, 'targetAttribute' => ['svcv_svcp_id' => 'svcp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'svcv_id' => Yii::t('app', 'Svcv ID'),
            'svcv_svc_id' => Yii::t('app', 'Svcv Svc ID'),
            'svcv_svcp_id' => Yii::t('app', 'Svcv Svcp ID'),
            'svcv_value' => Yii::t('app', 'Svcv Value'),
            'svcv_create_user' => Yii::t('app', 'Svcv Create User'),
            'svcv_create_time' => Yii::t('app', 'Svcv Create Time'),
            'svcv_create_ip' => Yii::t('app', 'Svcv Create Ip'),
            'svcv_update_user' => Yii::t('app', 'Svcv Update User'),
            'svcv_update_time' => Yii::t('app', 'Svcv Update Time'),
            'svcv_update_ip' => Yii::t('app', 'Svcv Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcvSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'svcv_svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcvSvcp()
    {
        return $this->hasOne(SvcProp::class, ['svcp_id' => 'svcv_svcp_id']);
    }

    /**
     * @inheritdoc
     * @return SvcValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcValueQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->svcv_create_user = Yii::$app->user->identity->username;
            $this->svcv_create_ip = Yii::$app->request->userIP;
            $this->svcv_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->svcv_update_user = Yii::$app->user->identity->username;
            $this->svcv_update_ip = Yii::$app->request->userIP;
            $this->svcv_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
