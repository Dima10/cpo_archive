<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Account;

/**
 * AccountSearch represents the model behind the search form about `app\models\Account`.
 */
class AccountSearch extends Account
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_id', 'acc_ab_id', 'acc_bank_id'], 'integer'],
            [['acc_number', 'acc_create_user', 'acc_create_time', 'acc_create_ip', 'acc_update_user', 'acc_update_time', 'acc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Account::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'acc_id' => $this->acc_id,
            'acc_ab_id' => $this->acc_ab_id,
            'acc_bank_id' => $this->acc_bank_id,
            'acc_create_time' => $this->acc_create_time,
            'acc_update_time' => $this->acc_update_time,
        ]);

        $query->andFilterWhere(['like', 'acc_number', $this->acc_number])
            ->andFilterWhere(['like', 'acc_create_user', $this->acc_create_user])
            ->andFilterWhere(['like', 'acc_create_ip', $this->acc_create_ip])
            ->andFilterWhere(['like', 'acc_update_user', $this->acc_update_user])
            ->andFilterWhere(['like', 'acc_update_ip', $this->acc_update_ip]);

        return $dataProvider;
    }
}
