<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;


class Constant
{
    const MONTHS = [
        'January' => 'января',
        'February' => 'февраля',
        'March' => 'марта',
        'April' => 'апрель',
        'May' => 'Мая',
        'June' => 'июня',
        'July' => 'июля',
        'August' => 'августа',
        'September' => 'сентября',
        'October' => 'октября',
        'November' => 'ноября',
        'December' => 'декабря'
    ];

    const YES_NO = [
        0 => 'Нет',
        1 => 'Да',
    ];

    /**
     * Возвращаем доступные реестры
     * @return array
     */
    public static function reestr_val(): array
    {
        return [Yii::t('app', 'Main Reestr'), Yii::t('app', 'Add Reestr')];
    }


}
