<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_act_a}}".
 *
 * @property integer $acta_id
 * @property integer $acta_act_id
 * @property integer $acta_ana_id
 * @property integer $acta_qty
 * @property string $acta_price
 * @property string $acta_tax
 * @property string $acta_create_user
 * @property string $acta_create_time
 * @property string $acta_create_ip
 * @property string $acta_update_user
 * @property string $acta_update_time
 * @property string $acta_update_ip
 *
 * @property AgreementAct $actaAct
 * @property AgreementAnnexA $actaAna
 */
class AgreementActA extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_act_a}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acta_act_id', 'acta_ana_id'], 'required'],
            [['acta_act_id', 'acta_ana_id', 'acta_qty'], 'integer'],
            [['acta_price', 'acta_tax'], 'number'],
            [['acta_create_time', 'acta_update_time'], 'safe'],
            [['acta_create_user', 'acta_create_ip', 'acta_update_user', 'acta_update_ip'], 'string', 'max' => 64],
            [['acta_act_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAct::class, 'targetAttribute' => ['acta_act_id' => 'act_id']],
            [['acta_ana_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAnnexA::class, 'targetAttribute' => ['acta_ana_id' => 'ana_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'acta_id' => Yii::t('app', 'Acta ID'),
            'acta_act_id' => Yii::t('app', 'Acta Act ID'),
            'acta_ana_id' => Yii::t('app', 'Acta Ana ID'),
            'acta_qty' => Yii::t('app', 'Acta Qty'),
            'acta_price' => Yii::t('app', 'Acta Price'),
            'acta_tax' => Yii::t('app', 'Acta Tax'),
            'acta_create_user' => Yii::t('app', 'Acta Create User'),
            'acta_create_time' => Yii::t('app', 'Acta Create Time'),
            'acta_create_ip' => Yii::t('app', 'Acta Create Ip'),
            'acta_update_user' => Yii::t('app', 'Acta Update User'),
            'acta_update_time' => Yii::t('app', 'Acta Update Time'),
            'acta_update_ip' => Yii::t('app', 'Acta Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActaAct()
    {
        return $this->hasOne(AgreementAct::class, ['act_id' => 'acta_act_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActaAna()
    {
        return $this->hasOne(AgreementAnnexA::class, ['ana_id' => 'acta_ana_id']);
    }

    /**
     * @inheritdoc
     * @return AgreementActAQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementActAQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->acta_create_user = Yii::$app->user->identity->username;
            $this->acta_create_ip = Yii::$app->request->userIP;
            $this->acta_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->acta_update_user = Yii::$app->user->identity->username;
            $this->acta_update_ip = Yii::$app->request->userIP;
            $this->acta_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
