<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%event_type}}".
 *
 * @property integer $evt_id
 * @property string $evt_name
 * @property string $evt_note
 * @property string $evt_create_user
 * @property string $evt_create_time
 * @property string $evt_create_ip
 * @property string $evt_update_user
 * @property string $evt_update_time
 * @property string $evt_update_ip
 *
 * @property Event[] $events
 */
class EventType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['evt_name'], 'required'],
            [['evt_note'], 'string'],
            [['evt_create_time', 'evt_update_time'], 'safe'],
            [['evt_name', 'evt_create_user', 'evt_create_ip', 'evt_update_user', 'evt_update_ip'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'evt_id' => Yii::t('app', 'Evt ID'),
            'evt_name' => Yii::t('app', 'Evt Name'),
            'evt_note' => Yii::t('app', 'Evt Note'),
            'evt_create_user' => Yii::t('app', 'Evt Create User'),
            'evt_create_time' => Yii::t('app', 'Evt Create Time'),
            'evt_create_ip' => Yii::t('app', 'Evt Create Ip'),
            'evt_update_user' => Yii::t('app', 'Evt Update User'),
            'evt_update_time' => Yii::t('app', 'Evt Update Time'),
            'evt_update_ip' => Yii::t('app', 'Evt Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::class, ['event_evt_id' => 'evt_id']);
    }

    /**
     * @inheritdoc
     * @return EventTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventTypeQuery(get_called_class());
    }
}
