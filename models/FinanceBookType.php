<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%finance_book_type}}".
 *
 * @property integer $fbt_id
 * @property string $fbt_name
 * @property string $fbt_create_user
 * @property string $fbt_create_ip
 * @property string $fbt_create_time
 * @property string $fbt_update_user
 * @property string $fbt_update_ip
 * @property string $fbt_update_time
 *
 * @property FinanceBook[] $financeBooks
 */
class FinanceBookType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%finance_book_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fbt_name'], 'required'],
            [['fbt_create_time', 'fbt_update_time'], 'safe'],
            [['fbt_name', 'fbt_create_user', 'fbt_create_ip', 'fbt_update_user', 'fbt_update_ip'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fbt_id' => Yii::t('app', 'Fbt ID'),
            'fbt_name' => Yii::t('app', 'Fbt Name'),
            'fbt_create_user' => Yii::t('app', 'Fbt Create User'),
            'fbt_create_ip' => Yii::t('app', 'Fbt Create Ip'),
            'fbt_create_time' => Yii::t('app', 'Fbt Create Time'),
            'fbt_update_user' => Yii::t('app', 'Fbt Update User'),
            'fbt_update_ip' => Yii::t('app', 'Fbt Update Ip'),
            'fbt_update_time' => Yii::t('app', 'Fbt Update Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBooks()
    {
        return $this->hasMany(FinanceBook::class, ['fb_fbt_id' => 'fbt_id']);
    }

    /**
     * @inheritdoc
     * @return FinanceBookTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FinanceBookTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->fbt_create_user = Yii::$app->user->identity->username;
            $this->fbt_create_ip = Yii::$app->request->userIP;
            $this->fbt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->fbt_update_user = Yii::$app->user->identity->username;
            $this->fbt_update_ip = Yii::$app->request->userIP;
            $this->fbt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
