<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplSheetContent;

/**
 * ApplSheetContentSearch represents the model behind the search form about `app\models\ApplSheetContent`.
 */
class ApplSheetContentSearch extends ApplSheetContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applsc_id', 'applsc_appls_id', 'applsc_trq_id', 'applsc_tra_id', 'applsc_tra_variant'], 'integer'],
            [['applsc_trq_question', 'applsc_tra_answer', 'applsc_create_user', 'applsc_create_time', 'applsc_create_ip', 'applsc_update_user', 'applsc_update_time', 'applsc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplSheetContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applsc_id' => $this->applsc_id,
            'applsc_appls_id' => $this->applsc_appls_id,
            'applsc_trq_id' => $this->applsc_trq_id,
            'applsc_tra_id' => $this->applsc_tra_id,
            'applsc_tra_variant' => $this->applsc_tra_variant,
            'applsc_create_time' => $this->applsc_create_time,
            'applsc_update_time' => $this->applsc_update_time,
        ]);

        $query
            ->andFilterWhere(['like', 'applsc_trq_question', $this->applsc_trq_question])
            ->andFilterWhere(['like', 'applsc_tra_answer', $this->applsc_tra_answer])
            ->andFilterWhere(['like', 'applsc_create_user', $this->applsc_create_user])
            ->andFilterWhere(['like', 'applsc_create_ip', $this->applsc_create_ip])
            ->andFilterWhere(['like', 'applsc_update_user', $this->applsc_update_user])
            ->andFilterWhere(['like', 'applsc_update_ip', $this->applsc_update_ip])
        ;

        return $dataProvider;
    }
}
