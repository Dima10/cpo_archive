<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceBook;

/**
 * FinanceBookSearch represents the model behind the search form about `app\models\FinanceBook`.
 */
class FinanceBookSearch extends FinanceBook
{
    public $ab_name;
    public $aga_number;
    public $svc_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fb_id', 'fb_svc_id', 'fb_ab_id', 'fb_comp_id', 'fb_fbt_id', 'fb_aga_id', 'fb_acc_id', 'fb_fbs_id'], 'integer'],
            [['fb_sum', 'fb_tax', 'fb_pay'], 'number'],

            [['ab_name', 'aga_number', 'svc_name'], 'string'],

            [['fb_date', 'fb_payment', 'fb_comment', 'fb_create_user', 'fb_create_ip', 'fb_create_time', 'fb_update_user', 'fb_update_ip', 'fb_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBook::find()
            ->leftJoin(Svc::tableName(), 'fb_svc_id = svc_id')
            ->leftJoin(Ab::tableName(), 'fb_ab_id = ab_id')
            ->leftJoin(AgreementAcc::tableName(), 'fb_aga_id = aga_id')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fb_id' => $this->fb_id,
            'fb_svc_id' => $this->fb_svc_id,
            'fb_fbt_id' => $this->fb_fbt_id,
            'fb_ab_id' => $this->fb_ab_id,
            'fb_comp_id' => $this->fb_comp_id,
            'fb_aca_id' => $this->fb_aca_id,
            'fb_aga_id' => $this->fb_aga_id,
            'fb_acc_id' => $this->fb_acc_id,
            'fb_fbs_id' => $this->fb_fbs_id,
        ]);

        $query
            ->andFilterWhere(['like', 'ab_name', $this->ab_name])
            ->andFilterWhere(['like', 'svc_name', $this->svc_name])
            ->andFilterWhere(['like', 'aga_number', $this->aga_number])

            ->andFilterWhere(['like', 'fb_sum', $this->fb_sum])
            ->andFilterWhere(['like', 'fb_tax', $this->fb_tax])
            ->andFilterWhere(['like', 'fb_pay', $this->fb_pay])
            ->andFilterWhere(['like', 'fb_date', $this->fb_date])
            ->andFilterWhere(['like', 'fb_payment', $this->fb_payment])
            ->andFilterWhere(['like', 'fb_comment', $this->fb_comment])
            ->andFilterWhere(['like', 'fb_create_user', $this->fb_create_user])
            ->andFilterWhere(['like', 'fb_create_time', $this->fb_create_user])
            ->andFilterWhere(['like', 'fb_create_ip', $this->fb_create_ip])
            ->andFilterWhere(['like', 'fb_update_user', $this->fb_update_user])
            ->andFilterWhere(['like', 'fb_update_time', $this->fb_update_user])
            ->andFilterWhere(['like', 'fb_update_ip', $this->fb_update_ip])
        ;

        return $dataProvider;
    }
}
