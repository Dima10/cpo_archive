<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

class UserI extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{

    public $id;
    public $username;
    public $password;
    public $level;
    public $authKey;
    public $accessToken;


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        //return isset(self::$user[$id]) ? new static(self::$user[$id]) : null;

        $model = (new \yii\db\Query())
            ->select('*')
            ->from('person')
            ->where('UPPER(prs_connect_user)=UPPER(:id)', [':id' => $id])
            ->all();

        foreach ($model as $user) {
            $us = array(
                'id'        => $user["prs_connect_user"],
                'username'  => $user["prs_connect_user"],
                'password'  => $user["prs_connect_pwd"],
                'level'     => -1,
                'authKey'   => $user["prs_connect_link"],
            );
            return new static($us);
        }

        $model = (new \yii\db\Query())
            ->select('*')
            ->from('user')
            ->where('user_id=:user_id', [':user_id' => $id])
            ->all();

        foreach ($model as $user) {
            if ($user['user_id'] == $id) {
                $us = array(
                         'id'           =>$user["user_id"], 
                         'username'     =>$user["user_name"], 
                         'password'     =>$user["user_pwd"], 
                         'level'        =>$user["user_level"], 
                         'authKey'      =>$user["user_authKey"], 
                         'accessToken'  =>$user["user_accessToken"],
                );
                return new static($us);
            }
        }
        return null;

    }

     /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

        $model = (new \yii\db\Query())
            ->select('*')
            ->from('person')
            ->where([
                'prs_connect_link' => $token
            ])
            ->all();

        foreach ($model as $user) {
            if ($user['accessToken'] === $token) {
                $us = array(
                    'id'           => $user["prs_connect_user"],
                    'username'     => $user["prs_connect_user"],
                    'password'     => $user["prs_connect_pwd"],
                    'level'        => -1,
                    'authKey'      => $user["prs_connect_link"],
                );
                return new static($us);
            }
        }

        $model = (new \yii\db\Query())
            ->select('*')
            ->from('user')
            ->where([
                    'user_accessToken' => $token
                    ])
            ->all();

        foreach ($model as $user) {
            if ($user['accessToken'] === $token) {
                $us = array(
                         'id'           =>$user["user_id"], 
                         'username'     =>$user["user_name"], 
                         'password'     =>$user["user_pwd"], 
                         'level'        =>$user["user_level"], 
                         'authKey'      =>$user["user_authKey"], 
                         'accessToken'  =>$user["user_accessToken"],
                );
                return new static($us);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $model = (new \yii\db\Query())
            ->select('*')
            ->from('person')
            ->where('UPPER(prs_connect_user)=UPPER(:user)', [':user' => $username])
            ->all();

        foreach ($model as $user) {
            $us = array(
                'id'           => $user["prs_connect_user"],
                'username'     => $user["prs_connect_user"],
                'password'     => $user["prs_connect_pwd"],
                'level'        => -1,
                'authKey'      => $user["prs_connect_link"],
            );
            return new static($us);
        }

        $model = (new \yii\db\Query())
                ->select('*')
                ->from('user')
                ->where('UPPER(user_name)=UPPER(:user_name)', [':user_name' => $username])
                ->all();

        foreach ($model as $user) {
            $us = array(
                'id'           =>$user["user_id"],
                'username'     =>$user["user_name"],
                'password'     =>$user["user_pwd"],
                'level'        =>$user["user_level"],
                'authKey'      =>$user["user_authKey"],
                'accessToken'  =>$user["user_accessToken"],
            );
            return new static($us);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function getRole()
    {
        return $this->role;
    }


    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if ($this->level < 0) {
            if (!empty($password) && !empty($this->password)) {
                return $password == $this->password;
            }
            return false;
        } else {
            return password_verify($password, $this->password);
            /*
            echo password_hash("GfhjkmFlvbyf", PASSWORD_BCRYPT, [12]);
            */
        }
    }
}


