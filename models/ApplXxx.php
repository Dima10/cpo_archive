<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_xxx}}".
 *
 * Протокол
 *
 * @property integer $applxxx_id
 * @property string $applxxx_number
 * @property string $applxxx_date
 * @property integer $applxxx_trt_id
 * @property integer $applxxx_svdt_id

 * @property integer $applxxx_pat_id
 * @property string $applxxx_file_name
 * @property resource $applxxx_file_data

 * @property string $applxxx_create_user
 * @property string $applxxx_create_time
 * @property string $applxxx_create_ip
 * @property string $applxxx_update_user
 * @property string $applxxx_update_time
 * @property string $applxxx_update_ip
 *
 * @property ApplXxxContent[] $applXxxContents
 * @property TrainingType $applsTrt
 * @property SvcDocType $applsSvdt
 * @property Pattern $applsPat
 *
 */
class ApplXxx extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_xxx}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applxxx_number', 'applxxx_date', 'applxxx_trt_id'], 'required'],
            [['applxxx_date', 'applxxx_create_time', 'applxxx_update_time'], 'safe'],
            [['applxxx_trt_id', 'applxxx_pat_id', 'applxxx_svdt_id'], 'integer'],
            [['applxxx_file_name'], 'string'],
            [['applxxx_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx, pdf'],

            [['applxxx_number'], 'unique'],

            [['applxxx_number', 'applxxx_create_user', 'applxxx_create_ip', 'applxxx_update_user', 'applxxx_update_ip'], 'string', 'max' => 64],
            [['applxxx_trt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applxxx_trt_id' => 'trt_id']],
            [['applxxx_svdt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['applxxx_svdt_id' => 'svdt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applxxx_id' => Yii::t('app', 'Applxxx ID'),
            'applxxx_number' => Yii::t('app', 'Applxxx Number'),
            'applxxx_date' => Yii::t('app', 'Applxxx Date'),
            'applxxx_trt_id' => Yii::t('app', 'Applxxx Trt ID'),
            'applxxx_svdt_id' => Yii::t('app', 'Applxxx Svdt ID'),
            'applxxx_pat_id' => Yii::t('app', 'Applxxx Pat ID'),
            'applxxx_file_name' => Yii::t('app', 'Applxxx File Name'),
            'applxxx_file_data' => Yii::t('app', 'Applxxx File Data'),
            'applxxx_create_user' => Yii::t('app', 'Applxxx Create User'),
            'applxxx_create_time' => Yii::t('app', 'Applxxx Create Time'),
            'applxxx_create_ip' => Yii::t('app', 'Applxxx Create Ip'),
            'applxxx_update_user' => Yii::t('app', 'Applxxx Update User'),
            'applxxx_update_time' => Yii::t('app', 'Applxxx Update Time'),
            'applxxx_update_ip' => Yii::t('app', 'Applxxx Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplxxxSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'applxxx_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplxxxTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applxxx_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplxxxPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applxxx_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplXxxContents()
    {
        return $this->hasMany(ApplXxxContent::class, ['applxxxc_applxxx_id' => 'applxxx_id']);
    }

    /**
     * @inheritdoc
     * @return ApplXxxQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplXxxQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applxxx_create_user = Yii::$app->user->identity->username;
            $this->applxxx_create_ip = Yii::$app->request->userIP;
            $this->applxxx_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applxxx_update_user = Yii::$app->user->identity->username;
            $this->applxxx_update_ip = Yii::$app->request->userIP;
            $this->applxxx_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
