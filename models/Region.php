<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%region}}".
 *
 * @property integer $reg_id
 * @property integer $reg_cou_id
 * @property string $reg_name
 * @property string $reg_create_user
 * @property string $reg_create_time
 * @property string $reg_create_ip
 * @property string $reg_update_user
 * @property string $reg_update_time
 * @property string $reg_update_ip
 *
 * @property Address[] $addresses
 * @property City[] $cities
 * @property Country $regCou
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_cou_id', 'reg_name'], 'required'],
            [['reg_cou_id'], 'integer'],
            [['reg_create_time', 'reg_update_time'], 'safe'],
            [['reg_name'], 'string', 'max' => 128],
            [['reg_create_user', 'reg_create_ip', 'reg_update_user', 'reg_update_ip'], 'string', 'max' => 64],
            [['reg_cou_id', 'reg_name'], 'unique', 'targetAttribute' => ['reg_cou_id', 'reg_name'], 'message' => 'The combination of Reg Cou ID and Reg Name has already been taken.'],
            [['reg_cou_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['reg_cou_id' => 'cou_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reg_id' => Yii::t('app', 'Reg ID'),
            'reg_cou_id' => Yii::t('app', 'Reg Cou ID'),
            'reg_name' => Yii::t('app', 'Reg Name'),
            'reg_create_user' => Yii::t('app', 'Create User'),
            'reg_create_time' => Yii::t('app', 'Create Time'),
            'reg_create_ip' => Yii::t('app', 'Create Ip'),
            'reg_update_user' => Yii::t('app', 'Update User'),
            'reg_update_time' => Yii::t('app', 'Update Time'),
            'reg_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['add_reg_id' => 'reg_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['city_reg_id' => 'reg_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegCou()
    {
        return $this->hasOne(Country::class, ['cou_id' => 'reg_cou_id']);
    }

    /**
     * @inheritdoc
     * @return RegionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RegionQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->reg_create_user = Yii::$app->user->identity->username;
            $this->reg_create_ip = Yii::$app->request->userIP;
            $this->reg_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->reg_update_user = Yii::$app->user->identity->username;
            $this->reg_update_ip = Yii::$app->request->userIP;
            $this->reg_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
