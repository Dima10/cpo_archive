<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "{{%pattern}}".
 *
 * @property integer $pat_id
 * @property string $pat_name
 * @property integer $pat_patt_id
 * @property integer $pat_svdt_id
 * @property integer $pat_trt_id
 * @property string $pat_code
 * @property string $pat_fname
 * @property resource $pat_fdata
 * @property string $pat_note
 * @property string $pat_create_user
 * @property string $pat_create_time
 * @property string $pat_create_ip
 * @property string $pat_update_user
 * @property string $pat_update_time
 * @property string $pat_update_ip
 *
 * @property AgreementAcc[] $agreementAccs
 * @property AgreementAct[] $agreementActs
 * @property AgreementAnnex[] $agreementAnnexes
 * @property PatternType $patPatt
 * @property SvcDocType $patSvdt
 * @property TrainingType $patTrt
 *
 */
class Pattern extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pattern}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pat_name', 'pat_patt_id'], 'required'],
            [['pat_patt_id', 'pat_svdt_id', 'pat_trt_id'], 'integer'],
            [['pat_fdata', 'pat_note'], 'string'],
            [['pat_create_time', 'pat_update_time'], 'safe'],
            [['pat_code'], 'string', 'max' => 16],
            [['pat_name'], 'string', 'max' => 128],

            [['pat_fname'], 'string', 'max' => 256],
            [['pat_fdata'], 'file', 'skipOnEmpty' => true, 'extensions' => 'rtf, docx'],

            [['pat_create_user', 'pat_create_ip', 'pat_update_user', 'pat_update_ip'], 'string', 'max' => 64],
            [['pat_patt_id'], 'exist', 'skipOnError' => true, 'targetClass' => PatternType::class, 'targetAttribute' => ['pat_patt_id' => 'patt_id']],
            //[['pat_trt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['pat_trt_id' => 'trt_id']],
            //[['pat_svdt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['pat_svdt_id' => 'svdt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pat_id' => Yii::t('app', 'Pat ID'),
            'pat_name' => Yii::t('app', 'Pat Name'),
            'pat_patt_id' => Yii::t('app', 'Pat Patt ID'),
            'pat_svdt_id' => Yii::t('app', 'Pat Svdt ID'),
            'pat_trt_id' => Yii::t('app', 'Pat Trt ID'),
            'pat_code' => Yii::t('app', 'Pat Code'),
            'pat_fname' => Yii::t('app', 'Pat Fname'),
            'pat_fdata' => Yii::t('app', 'Pat Fdata'),
            'pat_note' => Yii::t('app', 'Pat Note'),
            'pat_create_user' => Yii::t('app', 'Pat Create User'),
            'pat_create_time' => Yii::t('app', 'Pat Create Time'),
            'pat_create_ip' => Yii::t('app', 'Pat Create Ip'),
            'pat_update_user' => Yii::t('app', 'Pat Update User'),
            'pat_update_time' => Yii::t('app', 'Pat Update Time'),
            'pat_update_ip' => Yii::t('app', 'Pat Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAccs()
    {
        return $this->hasMany(AgreementAcc::class, ['aga_pat_id' => 'pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementActs()
    {
        return $this->hasMany(AgreementAct::class, ['act_pat_id' => 'pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAnnexes()
    {
        return $this->hasMany(AgreementAnnex::class, ['agra_pat_id' => 'pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatPatt()
    {
        return $this->hasOne(PatternType::class, ['patt_id' => 'pat_patt_id']);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'pat_trt_id']);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'pat_svdt_id']);

    }

    /**
     * @inheritdoc
     * @return PatternQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatternQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if ($this->pat_svdt_id == 0) {
            $this->pat_svdt_id = null;
        }
        if ($this->pat_trt_id == 0) {
            $this->pat_trt_id = null;
        }

        if ($insert) {
            $this->pat_create_user = Yii::$app->user->identity->username;
            $this->pat_create_ip = Yii::$app->request->userIP;
            $this->pat_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->pat_update_user = Yii::$app->user->identity->username;
            $this->pat_update_ip = Yii::$app->request->userIP;
            $this->pat_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     * @throws Exception if not found Pattern
     */
    public static function findOne($condition)
    {
        $pattern = parent::findOne($condition);
        if (is_null($pattern)) {
            throw new \yii\base\Exception(Yii::t('app', 'Pattern not found'));
        }
        return $pattern;
    }

}
