<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%person_visit1}}".
 *
 * @property int $pv1_id
 * @property int $pv1_pv_id
 * @property int $pv1_pve_id
 * @property string $pv1_create_user
 * @property string $pv1_create_time
 * @property string $pv1_create_ip
 *
 * @property PersonVisitEvent $pv1Pve
 * @property PersonVisit $pv1Pv
 */
class PersonVisit1 extends \yii\db\ActiveRecord
{

    const EVENT_PERSON_VISIT = 'event_person_visit1';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%person_visit1}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pv1_pv_id', 'pv1_pve_id', 'pv1_create_ip'], 'required'],
            [['pv1_pv_id', 'pv1_pve_id'], 'integer'],
            [['pv1_create_time'], 'safe'],
            [['pv1_create_user', 'pv1_create_ip'], 'string', 'max' => 64],
            [['pv1_pve_id'], 'exist', 'skipOnError' => true, 'targetClass' => PersonVisitEvent::class, 'targetAttribute' => ['pv1_pve_id' => 'pve_id']],
            [['pv1_pv_id'], 'exist', 'skipOnError' => true, 'targetClass' => PersonVisit::class, 'targetAttribute' => ['pv1_pv_id' => 'pv_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pv1_id' => Yii::t('app', 'Pv1 ID'),
            'pv1_pv_id' => Yii::t('app', 'Pv1 Pv ID'),
            'pv1_pve_id' => Yii::t('app', 'Pv1 Pve ID'),
            'pv1_create_user' => Yii::t('app', 'Pv1 Create User'),
            'pv1_create_time' => Yii::t('app', 'Pv1 Create Time'),
            'pv1_create_ip' => Yii::t('app', 'Pv1 Create Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPv1Pve()
    {
        return $this->hasOne(PersonVisitEvent::class, ['pve_id' => 'pv1_pve_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPv1Pv()
    {
        return $this->hasOne(PersonVisit::class, ['pv_id' => 'pv1_pv_id']);
    }

    /**
     * {@inheritdoc}
     * @return PersonVisit1Query the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonVisit1Query(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->pt_create_user = Yii::$app->user->identity->username;
            $this->pt_create_ip = Yii::$app->request->userIP;
            $this->pt_create_time = date('Y-m-d H:i:s');
            return true;
        }
        return true;
    }

    /**
     * Loging event
     * @param \yii\base\Event $event
     */

    public static function updateState($event)
    {
        if ((!Yii::$app->user->isGuest) && ($prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username])))
        {
            if (isset(Yii::$app->session[PersonVisit::class]))
            {
                if ($pv = PersonVisit::findOne(Yii::$app->session[PersonVisit::class])) {
                    if (is_array($event->data)) {
                        $pv1 = new PersonVisit1();
                        $pv1->pv1_pv_id = $pv->pv_id;
                        $pv1->pv1_pve_id = $event->data['pve_id'];
                        if (!$pv1->save()) {
                            Yii::$app->session->setFlash('PersonVisit', json_encode($pv1->errors, JSON_UNESCAPED_UNICODE));
                        }
                    }
                }
            }
        }
    }
}
