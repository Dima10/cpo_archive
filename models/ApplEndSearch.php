<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplEnd;

/**
 * ApplEndSearch represents the model behind the search form about `app\models\ApplEnd`.
 */
class ApplEndSearch extends ApplEnd
{
    public $applsx_number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apple_id', 'apple_applsx_id', 'apple_trt_id'], 'integer'],
            [['apple_number', 'apple_date', 'apple_create_user', 'apple_create_time', 'apple_create_ip', 'apple_update_user', 'apple_update_time', 'apple_update_ip'], 'safe'],
            [['applsx_number'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplEnd::find()->leftJoin(ApplSheetX::tableName(), 'apple_applsx_id = applsx_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'apple_id' => $this->apple_id,
            'apple_trt_id' => $this->apple_trt_id,
            'apple_applsx_id' => $this->apple_applsx_id,
        ]);

        $query
            ->andFilterWhere(['like', 'apple_number', $this->apple_number])
            ->andFilterWhere(['like', 'applsx_number', $this->applsx_number])
            ->andFilterWhere(['like', 'apple_date', $this->apple_date])
            ->andFilterWhere(['like', 'apple_create_user', $this->apple_create_user])
            ->andFilterWhere(['like', 'apple_create_ip', $this->apple_create_ip])
            ->andFilterWhere(['like', 'apple_create_time', $this->apple_create_time])
            ->andFilterWhere(['like', 'apple_update_user', $this->apple_update_user])
            ->andFilterWhere(['like', 'apple_update_ip', $this->apple_update_ip])
            ->andFilterWhere(['like', 'apple_update_time', $this->apple_update_time])
        ;

        return $dataProvider;
    }
}
