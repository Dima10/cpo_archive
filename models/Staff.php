<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%staff}}".
 *
 * @property integer $stf_id
 * @property integer $stf_ent_id
 * @property integer $stf_prs_id
 * @property string $stf_position
 * @property string $stf_create_user
 * @property string $stf_create_time
 * @property string $stf_create_ip
 * @property string $stf_update_user
 * @property string $stf_update_time
 * @property string $stf_update_ip
 *
 * @property Entity $stfEnt
 * @property Person $stfPrs
 * @property Contact[] $contacts
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%staff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stf_ent_id', 'stf_prs_id'], 'required'],
            [['stf_ent_id', 'stf_prs_id'], 'integer'],
            [['stf_create_time', 'stf_update_time'], 'safe'],
            [['stf_position'], 'string', 'max' => 128],
            [['stf_create_user', 'stf_create_ip', 'stf_update_user', 'stf_update_ip'], 'string', 'max' => 64],
            [['stf_ent_id', 'stf_prs_id'], 'unique', 'targetAttribute' => ['stf_ent_id', 'stf_prs_id'], 'message' => Yii::t('app', 'The combination of Stf Ent ID and Stf Prs ID has already been taken.')],
            [['stf_ent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::class, 'targetAttribute' => ['stf_ent_id' => 'ent_id']],
            [['stf_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['stf_prs_id' => 'prs_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stf_id' => Yii::t('app', 'Stf ID'),
            'stf_ent_id' => Yii::t('app', 'Stf Ent ID'),
            'stf_prs_id' => Yii::t('app', 'Stf Prs ID'),
            'stf_position' => Yii::t('app', 'Stf Position'),
            'stf_create_user' => Yii::t('app', 'Create User'),
            'stf_create_time' => Yii::t('app', 'Create Time'),
            'stf_create_ip' => Yii::t('app', 'Create Ip'),
            'stf_update_user' => Yii::t('app', 'Update User'),
            'stf_update_time' => Yii::t('app', 'Update Time'),
            'stf_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStfEnt()
    {
        return $this->hasOne(Entity::class, ['ent_id' => 'stf_ent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStfPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'stf_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::class, ['con_ab_id' => 'stf_prs_id']);
    }

    /**
     * @inheritdoc
     * @return StaffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StaffQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->stf_create_user = Yii::$app->user->identity->username;
            $this->stf_create_ip = Yii::$app->request->userIP;
            $this->stf_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->stf_update_user = Yii::$app->user->identity->username;
            $this->stf_update_ip = Yii::$app->request->userIP;
            $this->stf_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
