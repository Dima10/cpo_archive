<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventType;

/**
 * EventTypeSearch represents the model behind the search form about `app\models\EventType`.
 */
class EventTypeSearch extends EventType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['evt_id'], 'integer'],
            [['evt_name', 'evt_note', 'evt_create_user', 'evt_create_time', 'evt_create_ip', 'evt_update_user', 'evt_update_time', 'evt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'evt_id' => $this->evt_id,
            'evt_create_time' => $this->evt_create_time,
            'evt_update_time' => $this->evt_update_time,
        ]);

        $query->andFilterWhere(['like', 'evt_name', $this->evt_name])
            ->andFilterWhere(['like', 'evt_note', $this->evt_note])
            ->andFilterWhere(['like', 'evt_create_user', $this->evt_create_user])
            ->andFilterWhere(['like', 'evt_create_ip', $this->evt_create_ip])
            ->andFilterWhere(['like', 'evt_update_user', $this->evt_update_user])
            ->andFilterWhere(['like', 'evt_update_ip', $this->evt_update_ip]);

        return $dataProvider;
    }
}
