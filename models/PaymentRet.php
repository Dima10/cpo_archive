<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%payment_ret}}".
 *
 * @property integer $ptr_id
 * @property integer $ptr_ab_id
 * @property integer $ptr_acc_id
 * @property integer $ptr_comp_id
 * @property integer $ptr_aga_id
 * @property integer $ptr_aca_id
 * @property integer $ptr_fb_id
 * @property integer $ptr_fbs_id
 * @property integer $ptr_pt_id
 * @property integer $ptr_svc_id
 * @property string $ptr_svc_sum
 * @property string $ptr_svc_tax
 * @property string $ptr_date_pay
 * @property string $ptr_comment
 * @property string $ptr_create_user
 * @property string $ptr_create_time
 * @property string $ptr_create_ip
 * @property string $ptr_update_user
 * @property string $ptr_update_time
 * @property string $ptr_update_ip
 *
 * @property FinanceBook[] $financeBooks
 * @property Account[] $accounts
 * @property Ab[] $abs
 * @property Ab $ptrAb
 * @property AgreementAccA $ptrAca
 * @property Svc $ptrSvc
 * @property Company $ptrComp
 * @property Account $ptrAcc
 * @property FinanceBook $ptrFb
 * @property FinanceBookStatus $ptrFbs
 * @property PaymentType $ptrPt
 * @property AgreementAcc $ptrAga
 */
class PaymentRet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_ret}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ptr_ab_id', 'ptr_comp_id', 'ptr_fbs_id', 'ptr_pt_id', 'ptr_svc_id', 'ptr_svc_sum', 'ptr_svc_tax', 'ptr_date_pay'], 'required'],

            [['ptr_ab_id', 'ptr_acc_id', 'ptr_comp_id', 'ptr_aga_id', 'ptr_aca_id', 'ptr_fb_id', 'ptr_fbs_id', 'ptr_pt_id', 'ptr_svc_id'], 'integer'],
            [['ptr_svc_sum', 'ptr_svc_tax'], 'number'],
            [['ptr_date_pay', 'ptr_create_time', 'ptr_update_time'], 'safe'],

            [['ptr_comment'], 'string', 'max' => 1024],
            [['ptr_create_user', 'ptr_create_ip', 'ptr_update_user', 'ptr_update_ip'], 'string', 'max' => 64],
            [['ptr_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['ptr_ab_id' => 'ab_id']],
            [['ptr_aca_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAccA::class, 'targetAttribute' => ['ptr_aca_id' => 'aca_id']],
            [['ptr_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['ptr_svc_id' => 'svc_id']],
            [['ptr_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['ptr_comp_id' => 'comp_id']],
            [['ptr_acc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['ptr_acc_id' => 'acc_id']],
            [['ptr_fb_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBook::class, 'targetAttribute' => ['ptr_fb_id' => 'fb_id']],
            [['ptr_fbs_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBookStatus::class, 'targetAttribute' => ['ptr_fbs_id' => 'fbs_id']],
            [['ptr_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::class, 'targetAttribute' => ['ptr_pt_id' => 'pt_id']],
            [['ptr_aga_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAcc::class, 'targetAttribute' => ['ptr_aga_id' => 'aga_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ptr_id' => Yii::t('app', 'Ptr ID'),
            'ptr_ab_id' => Yii::t('app', 'Ptr Ab ID'),
            'ptr_acc_id' => Yii::t('app', 'Ptr Acc ID'),
            'ptr_comp_id' => Yii::t('app', 'Ptr Comp ID'),
            'ptr_aga_id' => Yii::t('app', 'Ptr Aga ID'),
            'ptr_aca_id' => Yii::t('app', 'Ptr Aca ID'),
            'ptr_fb_id' => Yii::t('app', 'Ptr Fb ID'),
            'ptr_fbs_id' => Yii::t('app', 'Ptr Fbs ID'),
            'ptr_pt_id' => Yii::t('app', 'Ptr Pt ID'),
            'ptr_svc_id' => Yii::t('app', 'Ptr Svc ID'),
            'ptr_svc_sum' => Yii::t('app', 'Ptr Svc Sum'),
            'ptr_svc_tax' => Yii::t('app', 'Ptr Svc Tax'),
            'ptr_date_pay' => Yii::t('app', 'Ptr Date Pay'),
            'ptr_comment' => Yii::t('app', 'Ptr Comment'),
            'ptr_create_user' => Yii::t('app', 'Ptr Create User'),
            'ptr_create_time' => Yii::t('app', 'Ptr Create Time'),
            'ptr_create_ip' => Yii::t('app', 'Ptr Create Ip'),
            'ptr_update_user' => Yii::t('app', 'Ptr Update User'),
            'ptr_update_time' => Yii::t('app', 'Ptr Update Time'),
            'ptr_update_ip' => Yii::t('app', 'Ptr Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBooks()
    {
        return $this->hasMany(FinanceBook::class, ['fb_ptr_id' => 'ptr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBook()
    {
        return $this->hasOne(FinanceBook::class, ['fb_ptr_id' => 'ptr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::class, ['acc_ab_id' => 'ptr_ab_id']);
    }

    /**
     * @return array app\models\Ab
     */
    public function getAbs()
    {
        return
            Ab::find()
                ->where(['ab_id' => $this->ptr_ab_id])
                ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'ptr_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'ptr_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'ptr_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrAca()
    {
        return $this->hasOne(AgreementAccA::class, ['aca_id' => 'ptr_aca_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'ptr_svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrFbs()
    {
        return $this->hasOne(FinanceBookStatus::class, ['fbs_id' => 'ptr_fbs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrPt()
    {
        return $this->hasOne(PaymentType::class, ['pt_id' => 'ptr_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrAga()
    {
        return $this->hasOne(AgreementAcc::class, ['aga_id' => 'ptr_aga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPtrFb()
    {
        return $this->hasOne(FinanceBook::class, ['fb_id' => 'ptr_fb_id']);
    }

    /**
     * @inheritdoc
     * @return PaymentRetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentRetQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->ptr_create_user = Yii::$app->user->identity->username;
            $this->ptr_create_ip = Yii::$app->request->userIP;
            $this->ptr_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->ptr_update_user = Yii::$app->user->identity->username;
            $this->ptr_update_ip = Yii::$app->request->userIP;
            $this->ptr_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function financeBookUpdate()
    {
        // Insert|Update a linked FinanceBook record
        if (!$fb = FinanceBook::find()->where(['fb_ptr_id' => $this->ptr_id])->one()) {
            $fb = new FinanceBook();
            //$fb->fb_fbs_id = 1;
        }
        $fb->fb_fbs_id = $this->ptr_fbs_id;
        $fb->fb_pt_id = $this->ptr_pt_id;
        $fb->fb_svc_id = $this->ptrSvc->svc_id;
        $fb->fb_ptr_id = $this->ptr_id;
        $fb->fb_fbt_id = 2;
        $fb->fb_ab_id = $this->ptr_ab_id;
        $fb->fb_comp_id = $this->ptr_comp_id;
        $fb->fb_sum = $this->ptr_svc_sum;
        $fb->fb_tax = $this->ptr_svc_tax;
        $fb->fb_aga_id = $this->ptrAca->aca_aga_id;
        $fb->fb_acc_id = $this->ptr_acc_id;
        $fb->fb_date = $this->ptr_date_pay;
        if ($fb->save()) {
            $this->ptr_fb_id = $fb->fb_id;
            $this->save(true);
        }
        /*
        $fb->fb_payment
        $fb->fb_comment
        */

    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach (FinanceBook::find()->where(['fb_ptr_id' => $this->ptr_id])->all() as $key => $value) {
                $value->delete();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Cache modifying object
     * Read all objects
     * @return PaymentRet[]
     */
    public static function cacheObjects(): array
    {
        if (Yii::$app->session->has('PaymentRetCacheObject')) {
            return Yii::$app->session['PaymentRetCacheObject'];
        }
        return [];
    }

    /**
     * Cache modifying object
     * Read object
     * @param integer $id
     * @return PaymentRet|null
     */
    public static function cacheObject($id)
    {
        if (isset(Yii::$app->session['PaymentRetCacheObject'][$id]) && Yii::$app->session['PaymentRetCacheObject'][$id] instanceof PaymentRet) {
            return  Yii::$app->session['PaymentRetCacheObject'][$id];
        }
        return null;
    }

    /**
     * Cache modifying object
     * Update object in cache
     * @param PaymentRet $obj
     * @return boolean
     */
    public static function cacheUpdate(PaymentRet $obj)
    {
        $ob = Yii::$app->session['PaymentRetCacheObject'];
        $ob[$obj->ptr_id] = $obj;
        Yii::$app->session['PaymentRetCacheObject'] = $ob;
        return true;
    }

    /**
     * Cache modifying object
     * Delete object from cache
     * @param PaymentRet $obj
     * @return boolean
     */
    public static function cacheDelete(PaymentRet $obj)
    {
        if ($obj instanceof PaymentRet) {
            $ob = Yii::$app->session['PaymentRetCacheObject'];
            if (isset($ob[$obj->ptr_id]))
                unset($ob[$obj->ptr_id]);
            Yii::$app->session['PaymentRetCacheObject'] = $ob;
            return true;
        }
        return false;
    }

    /**
     * Cache modifying object
     * Clear cache
     */
    public static function cacheClear()
    {
        Yii::$app->session['PaymentRetCacheObject'] = [];
    }

    /**
     * Cache modifying object
     * Apply cache to database
     */
    public static function cacheApply()
    {
        foreach (PaymentRet::cacheObjects() as $key => $obj) {
            if ($obj instanceof PaymentRet) {
                if (PaymentRet::findOne($obj->ptr_id)) {
                    $obj->save();
                    $obj->financeBookUpdate();
                } else {
                    $ptr = new PaymentRet();
                    $ptr->ptr_aca_id = $obj-> ptr_aca_id;
                    $ptr->ptr_aga_id = $obj-> ptr_aga_id;
                    $ptr->ptr_ab_id = $obj-> ptr_ab_id;
                    $ptr->ptr_acc_id = $obj-> ptr_acc_id;
                    $ptr->ptr_fbs_id = $obj->ptr_fbs_id;
                    $ptr->ptr_pt_id = $obj->ptr_pt_id;
                    $ptr->ptr_svc_id = $obj->ptr_svc_id;
                    $ptr->ptr_svc_sum = $obj->ptr_svc_sum;
                    $ptr->ptr_svc_tax = $obj->ptr_svc_tax;
                    $ptr->ptr_date_pay = $obj->ptr_date_pay;
                    if ($ptr->save()) {
                        $ptr->financeBookUpdate();
                    }
                }
            }
        }
        PaymentRet::cacheClear();
    }

    /**
     * Set next status
     */
    public function updatePay()
    {
        $this->cacheApply();
        if (!$fb = FinanceBook::find()->where(['fb_ptr_id' => $this->ptr_id])->one()) {
            die();
        }
        $fb->fb_fbs_id = 2;
        $fb->fb_pay = $this->ptr_svc_sum;
        if ($fb->save()) {
            $this->ptr_fbs_id = $fb->fb_fbs_id;
            $this->save();
        }
    }

}
