<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_answer}}".
 *
 * @property integer $tra_id
 * @property integer $tra_trq_id
 * @property string $tra_answer
 * @property integer $tra_variant
 * @property string $tra_file_name
 * @property resource $tra_file_data
 * @property string $tra_create_user
 * @property string $tra_create_time
 * @property string $tra_create_ip
 * @property string $tra_update_user
 * @property string $tra_update_time
 * @property string $tra_update_ip
 *
 * @property TrainingQuestion $traTrq
 */
class TrainingAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_answer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tra_trq_id', 'tra_answer', 'tra_variant'], 'required'],
            [['tra_trq_id', 'tra_variant'], 'integer'],
            [['tra_answer', 'tra_file_data'], 'string'],
            [['tra_file_name'], 'string', 'max' => 256],
            [['tra_create_time', 'tra_update_time'], 'safe'],
            [['tra_create_user', 'tra_create_ip', 'tra_update_user', 'tra_update_ip'], 'string', 'max' => 64],
            [['tra_trq_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingQuestion::class, 'targetAttribute' => ['tra_trq_id' => 'trq_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tra_id' => Yii::t('app', 'Tra ID'),
            'tra_trq_id' => Yii::t('app', 'Tra Trq ID'),
            'tra_answer' => Yii::t('app', 'Tra Answer'),
            'tra_variant' => Yii::t('app', 'Tra Variant'),
            'tra_file_name' => Yii::t('app', 'Tra File Name'),
            'tra_file_data' => Yii::t('app', 'Tra File Data'),
            'tra_create_user' => Yii::t('app', 'Tra Create User'),
            'tra_create_time' => Yii::t('app', 'Tra Create Time'),
            'tra_create_ip' => Yii::t('app', 'Tra Create Ip'),
            'tra_update_user' => Yii::t('app', 'Tra Update User'),
            'tra_update_time' => Yii::t('app', 'Tra Update Time'),
            'tra_update_ip' => Yii::t('app', 'Tra Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraTrq()
    {
        return $this->hasOne(TrainingQuestion::class, ['trq_id' => 'tra_trq_id']);
    }

    /**
     * @inheritdoc
     * @return TrainingAnswerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingAnswerQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->tra_create_user = Yii::$app->user->identity->username;
            $this->tra_create_ip = Yii::$app->request->userIP;
            $this->tra_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->tra_update_user = Yii::$app->user->identity->username;
            $this->tra_update_ip = Yii::$app->request->userIP;
            $this->tra_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
