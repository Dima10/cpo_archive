<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_end_content}}".
 *
 * @property integer $applec_id
 * @property integer $applec_apple_id
 * @property integer $applec_prs_id
 * @property integer $applec_trp_id
 * @property string $applec_number
 * @property string $applec_create_user
 * @property string $applec_create_time
 * @property string $applec_create_ip
 * @property string $applec_update_user
 * @property string $applec_update_time
 * @property string $applec_update_ip
 *
 * @property ApplEnd $applecApple
 * @property TrainingProg $applecTrp
 * @property Person $applecPrs
 */
class ApplEndContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_end_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applec_apple_id', 'applec_prs_id', 'applec_trp_id'], 'required'],
            [['applec_apple_id', 'applec_prs_id', 'applec_trp_id'], 'integer'],
            [['applec_create_time', 'applec_update_time'], 'safe'],
            [['applec_number', 'applec_create_user', 'applec_create_ip', 'applec_update_user', 'applec_update_ip'], 'string', 'max' => 64],
            [['applec_apple_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplEnd::class, 'targetAttribute' => ['applec_apple_id' => 'apple_id']],
            [['applec_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['applec_trp_id' => 'trp_id']],
            [['applec_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['applec_prs_id' => 'prs_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applec_id' => Yii::t('app', 'Applec ID'),
            'applec_apple_id' => Yii::t('app', 'Applec Apple ID'),
            'applec_prs_id' => Yii::t('app', 'Applec Prs ID'),
            'applec_trp_id' => Yii::t('app', 'Applec Trp ID'),
            'applec_number' => Yii::t('app', 'Applec Number'),
            'applec_create_user' => Yii::t('app', 'Applec Create User'),
            'applec_create_time' => Yii::t('app', 'Applec Create Time'),
            'applec_create_ip' => Yii::t('app', 'Applec Create Ip'),
            'applec_update_user' => Yii::t('app', 'Applec Update User'),
            'applec_update_time' => Yii::t('app', 'Applec Update Time'),
            'applec_update_ip' => Yii::t('app', 'Applec Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplecApple()
    {
        return $this->hasOne(ApplEnd::class, ['apple_id' => 'applec_apple_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplecTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applec_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplecPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'applec_prs_id']);
    }

    /**
     * @inheritdoc
     * @return ApplEndContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplEndContentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applec_create_user = Yii::$app->user->identity->username;
            $this->applec_create_ip = Yii::$app->request->userIP;
            $this->applec_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applec_update_user = Yii::$app->user->identity->username;
            $this->applec_update_ip = Yii::$app->request->userIP;
            $this->applec_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
