<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%svc}}".
 *
 * @property integer $svc_id
 * @property string $svc_name
 * @property integer $svc_svdt_id
 * @property string $svc_skill
 * @property string $svc_hour
 * @property string $svc_price
 * @property string $svc_cost
 * @property string $svc_cost_ext
 * @property integer $svc_payment_flag
 * @property integer $svc_tax_flag
 * @property integer $svc_ab_id
 * @property string $svc_create_user
 * @property string $svc_create_time
 * @property string $svc_create_ip
 * @property string $svc_update_user
 * @property string $svc_update_time
 * @property string $svc_update_ip
 *
 * @property SvcValue[] $svcValues
 * @property SvcProg[] $svcProgs
 * @property Ab $svcAb
 * @property SvcDocType $svcSvdt
 */
class Svc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svc_name'], 'required'],
            [['svc_svdt_id', 'svc_tax_flag', 'svc_payment_flag', 'svc_ab_id'], 'integer'],
            [['svc_price', 'svc_cost', 'svc_cost_ext'], 'number'],
            [['svc_create_time', 'svc_update_time'], 'safe'],
            [['svc_name'], 'string', 'max' => 128],
            [['svc_hour'], 'string', 'max' => 16],
            [['svc_skill', 'svc_create_user', 'svc_create_ip', 'svc_update_user', 'svc_update_ip'], 'string', 'max' => 64],
            [['svc_name'], 'unique'],
            [['svc_svdt_id'], 'exist', 'skipOnError' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['svc_svdt_id' => 'svdt_id']],
            [['svc_ab_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['svc_ab_id' => 'ab_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'svc_id' => Yii::t('app', 'Svc ID'),
            'svc_name' => Yii::t('app', 'Svc Name'),
            'svc_svdt_id' => Yii::t('app', 'Svc Svdt ID'),
            'svc_skill' => Yii::t('app', 'Svc Skill'),
            'svc_hour' => Yii::t('app', 'Svc Hour'),
            'svc_price' => Yii::t('app', 'Svc Price'),
            'svc_cost' => Yii::t('app', 'Svc Cost'),
            'svc_cost_ext' => Yii::t('app', 'Svc Cost Ext'),
            'svc_tax_flag' => Yii::t('app', 'Svc Tax Flag'),
            'svc_payment_flag' => Yii::t('app', 'Svc Payment Flag'),
            'svc_ab_id' => Yii::t('app', 'Svc Ab ID'), // Поставщик услуг по умолчанию
            'svc_create_user' => Yii::t('app', 'Svc Create User'),
            'svc_create_time' => Yii::t('app', 'Svc Create Time'),
            'svc_create_ip' => Yii::t('app', 'Svc Create Ip'),
            'svc_update_user' => Yii::t('app', 'Svc Update User'),
            'svc_update_time' => Yii::t('app', 'Svc Update Time'),
            'svc_update_ip' => Yii::t('app', 'Svc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'svc_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcValues()
    {
        return $this->hasMany(SvcValue::class, ['svcv_svc_id' => 'svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcProgs()
    {
        return $this->hasMany(SvcProg::class, ['sprog_svc_id' => 'svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'svc_ab_id']);
    }

    /**
     * @inheritdoc
     * @return SvcQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->svc_create_user = Yii::$app->user->identity->username;
            $this->svc_create_ip = Yii::$app->request->userIP;
            $this->svc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->svc_update_user = Yii::$app->user->identity->username;
            $this->svc_update_ip = Yii::$app->request->userIP;
            $this->svc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
