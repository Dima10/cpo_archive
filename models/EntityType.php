<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%entity_type}}".
 *
 * @property integer $entt_id
 * @property string $entt_name
 * @property string $entt_name_short
 * @property string $entt_create_user
 * @property string $entt_create_time
 * @property string $entt_create_ip
 * @property string $entt_update_user
 * @property string $entt_update_time
 * @property string $entt_update_ip
 *
 * @property Entity[] $entities
 */
class EntityType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entity_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entt_name', 'entt_name_short'], 'required'],
            [['entt_create_time', 'entt_update_time'], 'safe'],
            [['entt_name'], 'string', 'max' => 128],
            [['entt_create_user', 'entt_create_ip', 'entt_update_user', 'entt_update_ip'], 'string', 'max' => 64],
            [['entt_name_short'], 'string', 'max' => 64],
            [['entt_name'], 'unique'],
            [['entt_name_short'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entt_id' => Yii::t('app', 'Entt ID'),
            'entt_name' => Yii::t('app', 'Entt Name'),
            'entt_name_short' => Yii::t('app', 'Entt Name Short'),
            'entt_create_user' => Yii::t('app', 'Create User'),
            'entt_create_time' => Yii::t('app', 'Create Time'),
            'entt_create_ip' => Yii::t('app', 'Create Ip'),
            'entt_update_user' => Yii::t('app', 'Update User'),
            'entt_update_time' => Yii::t('app', 'Update Time'),
            'entt_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasMany(Entity::class, ['ent_entt_id' => 'entt_id']);
    }

    /**
     * @inheritdoc
     * @return EntityTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EntityTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->entt_create_user = Yii::$app->user->identity->username;
            $this->entt_create_ip = Yii::$app->request->userIP;
            $this->entt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->entt_update_user = Yii::$app->user->identity->username;
            $this->entt_update_ip = Yii::$app->request->userIP;
            $this->entt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
