<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calendar;

/**
 * CalendarSearch represents the model behind the search form about `app\models\Calendar`.
 */
class CalendarSearch extends Calendar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cal_id', 'cal_holiday'], 'integer'],
            [['cal_date', 'cal_create_user', 'cal_create_time', 'cal_create_ip', 'cal_update_user', 'cal_update_time', 'cal_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calendar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'cal_date' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cal_id' => $this->cal_id,
            'cal_date' => $this->cal_date,
            'cal_holiday' => $this->cal_holiday,
            'cal_create_time' => $this->cal_create_time,
            'cal_update_time' => $this->cal_update_time,
        ]);

        $query->andFilterWhere(['like', 'cal_create_user', $this->cal_create_user])
            ->andFilterWhere(['like', 'cal_create_ip', $this->cal_create_ip])
            ->andFilterWhere(['like', 'cal_update_user', $this->cal_update_user])
            ->andFilterWhere(['like', 'cal_update_ip', $this->cal_update_ip]);

        return $dataProvider;
    }
}
