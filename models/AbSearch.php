<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ab;

/**
 * AbSearch represents the model behind the search form about `app\models\Ab`.
 */
class AbSearch extends Ab
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ab_id', 'ab_type'], 'integer'],
            [['ab_name', 'ab_create_user', 'ab_create_time', 'ab_create_ip', 'ab_update_user', 'ab_update_time', 'ab_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ab::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ab_id' => $this->ab_id,
            'ab_type' => $this->ab_type,
            'ab_create_time' => $this->ab_create_time,
            'ab_update_time' => $this->ab_update_time,
        ]);

        $query->andFilterWhere(['like', 'ab_name', $this->ab_name])
            ->andFilterWhere(['like', 'ab_create_user', $this->ab_create_user])
            ->andFilterWhere(['like', 'ab_create_ip', $this->ab_create_ip])
            ->andFilterWhere(['like', 'ab_update_user', $this->ab_update_user])
            ->andFilterWhere(['like', 'ab_update_ip', $this->ab_update_ip]);

        return $dataProvider;
    }
}
