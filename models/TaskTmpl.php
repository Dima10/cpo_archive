<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%task_tmpl}}".
 *
 * @property integer $tasktp_id
 * @property integer $tasktp_event_id
 * @property integer $tasktp_taskt_id
 * @property integer $tasktp_date_add
 * @property string $tasktp_note
 * @property string $tasktp_create_user
 * @property string $tasktp_create_time
 * @property string $tasktp_create_ip
 * @property string $tasktp_update_user
 * @property string $tasktp_update_time
 * @property string $tasktp_update_ip
 *
 * @property Event $tasktpEvent
 * @property TaskType $tasktpTaskt
 */
class TaskTmpl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task_tmpl}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tasktp_event_id', 'tasktp_taskt_id', 'tasktp_date_add'], 'required'],
            [['tasktp_event_id', 'tasktp_taskt_id', 'tasktp_date_add'], 'integer'],
            [['tasktp_note'], 'string'],
            [['tasktp_create_time', 'tasktp_update_time'], 'safe'],
            [['tasktp_create_user', 'tasktp_create_ip', 'tasktp_update_user', 'tasktp_update_ip'], 'string', 'max' => 64],
            [['tasktp_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['tasktp_event_id' => 'event_id']],
            [['tasktp_taskt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::class, 'targetAttribute' => ['tasktp_taskt_id' => 'taskt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tasktp_id' => Yii::t('app', 'Tasktp ID'),
            'tasktp_event_id' => Yii::t('app', 'Tasktp Event ID'),
            'tasktp_taskt_id' => Yii::t('app', 'Tasktp Taskt ID'),
            'tasktp_date_add' => Yii::t('app', 'Tasktp Date Add'),
            'tasktp_note' => Yii::t('app', 'Tasktp Note'),
            'tasktp_create_user' => Yii::t('app', 'Tasktp Create User'),
            'tasktp_create_time' => Yii::t('app', 'Tasktp Create Time'),
            'tasktp_create_ip' => Yii::t('app', 'Tasktp Create Ip'),
            'tasktp_update_user' => Yii::t('app', 'Tasktp Update User'),
            'tasktp_update_time' => Yii::t('app', 'Tasktp Update Time'),
            'tasktp_update_ip' => Yii::t('app', 'Tasktp Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasktpEvent()
    {
        return $this->hasOne(Event::class, ['event_id' => 'tasktp_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasktpTaskt()
    {
        return $this->hasOne(TaskType::class, ['taskt_id' => 'tasktp_taskt_id']);
    }

    /**
     * @inheritdoc
     * @return TaskTmplQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskTmplQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->tasktp_create_user = Yii::$app->user->identity->username;
            $this->tasktp_create_ip = Yii::$app->request->userIP;
            $this->tasktp_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->tasktp_update_user = Yii::$app->user->identity->username;
            $this->tasktp_update_ip = Yii::$app->request->userIP;
            $this->tasktp_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
