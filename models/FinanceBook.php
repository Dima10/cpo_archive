<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%finance_book}}".
 *
 * @property integer $fb_id
 * @property integer $fb_fb_id
 * @property integer $fb_aca_id
 * @property integer $fb_payt_id
 * @property integer $fb_ptra_id
 * @property integer $fb_svc_id
 * @property integer $fb_fbt_id
 * @property integer $fb_ab_id
 * @property integer $fb_comp_id
 * @property string $fb_sum
 * @property string $fb_tax
 * @property string $fb_pay
 * @property integer $fb_aga_id
 * @property integer $fb_acc_id
 * @property integer $fb_fbs_id
 * @property integer $fb_pt_id
 * @property string $fb_date
 * @property string $fb_payment
 * @property string $fb_comment
 * @property string $fb_create_user
 * @property string $fb_create_ip
 * @property string $fb_create_time
 * @property string $fb_update_user
 * @property string $fb_update_ip
 * @property string $fb_update_time
 *
 * @property FinanceBookStatus $fbFbs
 * @property Svc $fbSvc
 * @property AgreementAcc $fbAga
 * @property AgreementAccA $fbAca
 * @property Account $fbAcc
 * @property FinanceBookType $fbFbt
 * @property PaymentType $fbPt
 * @property PaymentTo $fbPayt
 * @property PaymentRet $fbPtr
 */
class FinanceBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%finance_book}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fb_svc_id', 'fb_fbt_id', 'fb_ab_id', 'fb_comp_id', 'fb_sum', 'fb_tax', 'fb_aga_id', 'fb_fbs_id'], 'required'],
            [['fb_fb_id', 'fb_svc_id', 'fb_ab_id', 'fb_comp_id', 'fb_fbt_id', 'fb_aga_id', 'fb_acc_id', 'fb_fbs_id', 'fb_pt_id', 'fb_payt_id', 'fb_ptr_id'], 'integer'],
            [['fb_sum', 'fb_tax', 'fb_pay'], 'number'],
            [['fb_date', 'fb_create_time', 'fb_update_time'], 'safe'],
            [['fb_payment'], 'string', 'max' => 256],
            [['fb_comment'], 'string', 'max' => 1024],
            [['fb_create_user', 'fb_create_ip', 'fb_update_user', 'fb_update_ip'], 'string', 'max' => 64],
            [['fb_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['fb_ab_id' => 'ab_id']],
            [['fb_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['fb_comp_id' => 'comp_id']],
            [['fb_fbs_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBookStatus::class, 'targetAttribute' => ['fb_fbs_id' => 'fbs_id']],
            [['fb_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['fb_svc_id' => 'svc_id']],
            [['fb_aga_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAcc::class, 'targetAttribute' => ['fb_aga_id' => 'aga_id']],
            [['fb_aca_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAccA::class, 'targetAttribute' => ['fb_aca_id' => 'aca_id']],
            [['fb_acc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['fb_acc_id' => 'acc_id']],
            [['fb_fbt_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBookType::class, 'targetAttribute' => ['fb_fbt_id' => 'fbt_id']],
            [['fb_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::class, 'targetAttribute' => ['fb_pt_id' => 'pt_id']],
            [['fb_payt_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentTo::class, 'targetAttribute' => ['fb_payt_id' => 'payt_id']],
            [['fb_ptr_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentRet::class, 'targetAttribute' => ['fb_ptr_id' => 'ptr_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fb_id' => Yii::t('app', 'Fb ID'),
            'fb_fb_id' => Yii::t('app', 'Fb Fb ID'),

            'fb_aca_id' => Yii::t('app', 'Fb Aca ID'),
            'fb_payt_id' => Yii::t('app', 'Fb Payt ID'),
            'fb_ptr_id' => Yii::t('app', 'Fb Ptr ID'),

            'fb_svc_id' => Yii::t('app', 'Fb Svc ID'),
            'fb_fbt_id' => Yii::t('app', 'Fb Fbt ID'),
            'fb_ab_id' => Yii::t('app', 'Fb Ab ID'),
            'fb_comp_id' => Yii::t('app', 'Fb Comp ID'),
            'fb_sum' => Yii::t('app', 'Fb Sum'),
            'fb_tax' => Yii::t('app', 'Fb Tax'),
            'fb_pay' => Yii::t('app', 'Fb Pay'),
            'fb_aga_id' => Yii::t('app', 'Fb Aga ID'),
            'fb_acc_id' => Yii::t('app', 'Fb Acc ID'),
            'fb_fbs_id' => Yii::t('app', 'Fb Fbs ID'),
            'fb_pt_id' => Yii::t('app', 'Fb Pt ID'),
            'fb_date' => Yii::t('app', 'Fb Date'),
            'fb_payment' => Yii::t('app', 'Fb Payment'),
            'fb_comment' => Yii::t('app', 'Fb Comment'),
            'fb_create_user' => Yii::t('app', 'Fb Create User'),
            'fb_create_ip' => Yii::t('app', 'Fb Create Ip'),
            'fb_create_time' => Yii::t('app', 'Fb Create Time'),
            'fb_update_user' => Yii::t('app', 'Fb Update User'),
            'fb_update_ip' => Yii::t('app', 'Fb Update Ip'),
            'fb_update_time' => Yii::t('app', 'Fb Update Time'),

            'svc_name' => Yii::t('app', 'Svc Name'),
            'ab_name' => Yii::t('app', 'Ab Name'),
            'aga_number' => Yii::t('app', 'Aga Number'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbFbs()
    {
        return $this->hasOne(FinanceBookStatus::class, ['fbs_id' => 'fb_fbs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'fb_svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbAga()
    {
        return $this->hasOne(AgreementAcc::class, ['aga_id' => 'fb_aga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbAca()
    {
        return $this->hasOne(AgreementAccA::class, ['aca_id' => 'fb_aca_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'fb_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbFbt()
    {
        return $this->hasOne(FinanceBookType::class, ['fbt_id' => 'fb_fbt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbPt()
    {
        return $this->hasOne(PaymentType::class, ['pt_id' => 'fb_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'fb_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'fb_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbPayt()
    {
        return $this->hasOne(PaymentTo::class, ['payt_id' => 'fb_payt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFbPtr()
    {
        return $this->hasOne(PaymentRet::class, ['ptr_id' => 'fb_ptr_id']);
    }


    //---------------------------------------------------------------------
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvc_name()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'fb_svc_id'])->one()->svc_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAb_name()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'fb_ab_id'])->one()->ab_name;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAga_number()
    {
        return $this->hasOne(AgreementAcc::class, ['aga_id' => 'fb_aga_id'])->one()->aga_number;
    }
    //---------------------------------------------------------------------


    /**
     * @inheritdoc
     * @return FinanceBookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FinanceBookQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if ( floatval($this->fb_sum) <= floatval($this->fb_pay)) {
            $this->fb_fbs_id = 2;
        } else {
            $this->fb_fbs_id = 1;
        }

        if ($insert) {
            $this->fb_pt_id = 1;
            $this->fb_create_user = Yii::$app->user->identity->username;
            $this->fb_create_ip = Yii::$app->request->userIP;
            $this->fb_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->fb_update_user = Yii::$app->user->identity->username;
            $this->fb_update_ip = Yii::$app->request->userIP;
            $this->fb_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach (FinanceBook::find()->where(['fb_fb_id' => $this->fb_id])->all() as $key => $obj) {
                $obj->delete();
            }
            return true;
        } else {
            return false;
        }
    }

}
