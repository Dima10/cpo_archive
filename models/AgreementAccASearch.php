<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementAccA;

/**
 * AgreementAccASearch represents the model behind the search form about `app\models\AgreementAccA`.
 */
class AgreementAccASearch extends AgreementAccA
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aca_id', 'aca_aga_id', 'aca_qty'], 'integer'],
            [['aca_price', 'aca_tax'], 'number'],
            [['aca_create_user', 'aca_create_time', 'aca_create_ip', 'aca_update_user', 'aca_update_time', 'aca_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgreementAccA::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'aca_id' => $this->aca_id,
            'aca_aga_id' => $this->aca_aga_id,
            'aca_qty' => $this->aca_qty,
            'aca_price' => $this->aca_price,
            'aca_tax' => $this->aca_tax,
        ]);

        $query
            ->andFilterWhere(['like', 'aca_qty', $this->aca_qty])
            ->andFilterWhere(['like', 'aca_price', $this->aca_price])
            ->andFilterWhere(['like', 'aca_tax', $this->aca_tax])
            ->andFilterWhere(['like', 'aca_create_user', $this->aca_create_user])
            ->andFilterWhere(['like', 'aca_create_time', $this->aca_create_time])
            ->andFilterWhere(['like', 'aca_create_ip', $this->aca_create_ip])
            ->andFilterWhere(['like', 'aca_update_user', $this->aca_update_user])
            ->andFilterWhere(['like', 'aca_update_time', $this->aca_update_time])
            ->andFilterWhere(['like', 'aca_update_ip', $this->aca_update_ip])
        ;

        return $dataProvider;
    }
}
