<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%contact_type}}".
 *
 * @property integer $cont_id
 * @property string $cont_name
 * @property string $cont_type
 * @property string $cont_create_user
 * @property string $cont_create_time
 * @property string $cont_create_ip
 * @property string $cont_update_user
 * @property string $cont_update_time
 * @property string $cont_update_ip
 *
 * @property Contact[] $contacts
 */
class ContactType extends \yii\db\ActiveRecord
{

    public static $contact_type = [0 => 'phone', 1 => 'e-mail', 2 => 'site', 3 => 'skype'];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contact_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_name', 'cont_type'], 'required'],
            [['cont_create_time', 'cont_update_time'], 'safe'],
            [['cont_name', 'cont_create_user', 'cont_create_ip', 'cont_update_user', 'cont_update_ip'], 'string', 'max' => 64],
            [['cont_name'], 'unique'],
            [['cont_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => Yii::t('app', 'Cont ID'),
            'cont_name' => Yii::t('app', 'Cont Name'),
            'cont_type' => Yii::t('app', 'Cont Type'),
            'cont_create_user' => Yii::t('app', 'Create User'),
            'cont_create_time' => Yii::t('app', 'Create Time'),
            'cont_create_ip' => Yii::t('app', 'Create Ip'),
            'cont_update_user' => Yii::t('app', 'Update User'),
            'cont_update_time' => Yii::t('app', 'Update Time'),
            'cont_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::class, ['con_cont_id' => 'cont_id']);
    }

    /**
     * @inheritdoc
     * @return ContactTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContactTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->cont_create_user = Yii::$app->user->identity->username;
            $this->cont_create_ip = Yii::$app->request->userIP;
            $this->cont_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->cont_update_user = Yii::$app->user->identity->username;
            $this->cont_update_ip = Yii::$app->request->userIP;
            $this->cont_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
