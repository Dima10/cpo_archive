<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaskTmpl;

/**
 * TaskTmplSearch represents the model behind the search form about `app\models\TaskTmpl`.
 */
class TaskTmplSearch extends TaskTmpl
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tasktp_id', 'tasktp_event_id', 'tasktp_taskt_id', 'tasktp_date_add'], 'integer'],
            [['tasktp_note', 'tasktp_create_user', 'tasktp_create_time', 'tasktp_create_ip', 'tasktp_update_user', 'tasktp_update_time', 'tasktp_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskTmpl::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tasktp_id' => $this->tasktp_id,
            'tasktp_event_id' => $this->tasktp_event_id,
            'tasktp_taskt_id' => $this->tasktp_taskt_id,
            'tasktp_date_add' => $this->tasktp_date_add,
            'tasktp_create_time' => $this->tasktp_create_time,
            'tasktp_update_time' => $this->tasktp_update_time,
        ]);

        $query->andFilterWhere(['like', 'tasktp_note', $this->tasktp_note])
            ->andFilterWhere(['like', 'tasktp_create_user', $this->tasktp_create_user])
            ->andFilterWhere(['like', 'tasktp_create_ip', $this->tasktp_create_ip])
            ->andFilterWhere(['like', 'tasktp_update_user', $this->tasktp_update_user])
            ->andFilterWhere(['like', 'tasktp_update_ip', $this->tasktp_update_ip]);

        return $dataProvider;
    }
}
