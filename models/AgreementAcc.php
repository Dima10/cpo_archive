<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_acc}}".
 *
 * Счета на оплату
 *
 * @property integer $aga_id
 * @property string $aga_number
 * @property string $aga_date
 * @property integer $aga_ab_id
 * @property integer $aga_comp_id
 * @property integer $aga_agr_id
 * @property integer $aga_agra_id
 * @property string $aga_comment
 * @property string $aga_sum
 * @property string $aga_tax
 * @property integer $aga_ast_id 
 * @property integer $aga_pat_id 
 * @property resource $aga_data
 * @property string $aga_fdata
 * @property resource $aga_data_sign
 * @property string $aga_fdata_sign
 * @property string $aga_create_user
 * @property string $aga_create_time
 * @property string $aga_create_ip
 * @property string $aga_update_user
 * @property string $aga_update_time
 * @property string $aga_update_ip
 *
 * @property Ab $agaAb
 * @property Company $agaComp
 * @property Agreement $agaAgr
 * @property AgreementAnnex $agaAgra
 * @property Pattern $agaPat
 * @property AgreementStatus $agaAst 
 * @property AgreementAccA[] $agreementAccAs
 */

class AgreementAcc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_acc}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aga_number', 'aga_date', 'aga_agr_id', 'aga_ab_id', 'aga_comp_id'], 'required'],
            [['aga_date', 'aga_create_time', 'aga_update_time'], 'safe'],
            [['aga_ab_id', 'aga_comp_id', 'aga_agr_id', 'aga_agra_id', 'aga_pat_id', 'aga_ast_id'], 'integer'],
            [['aga_sum', 'aga_tax'], 'number'],
            [['aga_comment'], 'string', 'max' => 1024],

            //[['aga_data', 'aga_data_sign'], 'string'],
            [['aga_data', 'aga_data_sign'], 'file', 'skipOnEmpty' => true, 'extensions' => 'rtf, docx, pdf'],

            [['aga_number'], 'string', 'max' => 32],
            [['aga_fdata', 'aga_fdata_sign'], 'string', 'max' => 256],
            [['aga_create_user', 'aga_create_ip', 'aga_update_user', 'aga_update_ip'], 'string', 'max' => 64],
            [['aga_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['aga_ab_id' => 'ab_id']],
            [['aga_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['aga_comp_id' => 'comp_id']],

            [['aga_number', 'aga_agr_id', 'aga_comp_id'], 'unique', 'targetAttribute' => ['aga_number', 'aga_agr_id', 'aga_comp_id'], 'message' => Yii::t('app', 'The combination of Aga Number and Aga Agr ID and Aga Comp ID has already been taken.')],

            [['aga_ast_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementStatus::class, 'targetAttribute' => ['aga_ast_id' => 'ast_id']],
            [['aga_agr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['aga_agr_id' => 'agr_id']],
            //[['aga_agra_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAnnex::class, 'targetAttribute' => ['aga_agra_id' => 'agra_id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aga_id' => Yii::t('app', 'Aga ID'),
            'aga_number' => Yii::t('app', 'Aga Number'),
            'aga_date' => Yii::t('app', 'Aga Date'),
            'aga_ab_id' => Yii::t('app', 'Aga Ab ID'),
            'aga_comp_id' => Yii::t('app', 'Aga Comp ID'),
            'aga_agr_id' => Yii::t('app', 'Aga Agr ID'),
            'aga_agra_id' => Yii::t('app', 'Aga Agra ID'),
            'aga_comment' => Yii::t('app', 'Aga Comment'),
            'aga_sum' => Yii::t('app', 'Aga Sum'),
            'aga_tax' => Yii::t('app', 'Aga Tax'),
            'aga_pat_id' => Yii::t('app', 'Aga Pat ID'), 
            'aga_ast_id' => Yii::t('app', 'Aga Ast ID'), 
            'aga_data' => Yii::t('app', 'Aga Data'),
            'aga_fdata' => Yii::t('app', 'Aga Fdata'),
            'aga_data_sign' => Yii::t('app', 'Aga Data Sign'),
            'aga_fdata_sign' => Yii::t('app', 'Aga Fdata Sign'),
            'aga_create_user' => Yii::t('app', 'Aga Create User'),
            'aga_create_time' => Yii::t('app', 'Aga Create Time'),
            'aga_create_ip' => Yii::t('app', 'Aga Create Ip'),
            'aga_update_user' => Yii::t('app', 'Aga Update User'),
            'aga_update_time' => Yii::t('app', 'Aga Update Time'),
            'aga_update_ip' => Yii::t('app', 'Aga Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'aga_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'aga_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaAgr()
    {
        return $this->hasOne(Agreement::class, ['agr_id' => 'aga_agr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaAgra()
    {
        return $this->hasOne(AgreementAnnex::class, ['agra_id' => 'aga_agra_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'aga_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaAst() 
    { 
        return $this->hasOne(AgreementStatus::class, ['ast_id' => 'aga_ast_id']); 
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAccAs()
    {
        return $this->hasMany(AgreementAccA::class, ['aca_aga_id' => 'aga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::class, ['pay_inv_id' => 'aga_id']);
    }

    /**
     * @inheritdoc
     * @return AgreementAccQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementAccQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {

        if (!parent::beforeSave($insert)) return false;
        if ($insert) {

            if ($this->aga_agr_id == 0) {
                $this->aga_agr_id = null;
            }

            if ($this->aga_agra_id == 0) {
                $this->aga_agra_id = null;
            }                   

            $this->aga_create_user = Yii::$app->user->identity->username;
            $this->aga_create_ip = Yii::$app->request->userIP;
            $this->aga_create_time = date('Y-m-d H:i:s');
            return true;
        } else {

            if ($this->aga_agr_id == 0) {
                $this->aga_agr_id = null;
            }

            if ($this->aga_agra_id == 0) {
                $this->aga_agra_id = null;
            }

            $this->aga_update_user = Yii::$app->user->identity->username;
            $this->aga_update_ip = Yii::$app->request->userIP;
            $this->aga_update_time = date('Y-m-d H:i:s');
            return true;
        }

    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach (AgreementAccA::find()->where(['aca_aga_id' => $this->aga_id])->all() as $key => $value) {
                $value->delete();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * update status $this->aga_ast_id
     */
    public function updateStatus() {
        $st1 = AgreementStatus::find()->where(['like', 'ast_sys', 'P'])->one();
        $st2 = AgreementStatus::find()->where(['like', 'ast_sys', 'A'])->one();
        $st1 = isset($st1) ? $st1->ast_id : null;
        $st2 = isset($st2) ? $st2->ast_id : null;

        $fbSum = FinanceBook::find()->where(['fb_aga_id' => $this->aga_id])->sum('fb_pay');
        if ($fbSum >= $this->aga_sum) {
            $this->aga_ast_id = $st1;
            $this->save(false);
        } else {
            $this->aga_ast_id = $st2;
            $this->save(false);
        }

    }

}
