<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_main}}".
 *
 * @property integer $applm_id
 * @property integer $applm_reestr
 * @property integer $applm_applrc_id
 * @property integer $applm_prs_id
 * @property string $applm_position
 * @property integer $applm_svc_id
 * @property integer $applm_trp_id
 * @property integer $applm_ab_id
 * @property integer $applm_comp_id
 * @property integer $applm_trt_id
 * @property integer $applm_svdt_id
 * @property integer $applm_applr_id
 * @property string $applm_applr_date
 * @property integer $applm_applcmd_id
 * @property string $applm_applcmd_date
 * @property string $applm_date_upk
 * @property string $applm_date_start
 * @property integer $applm_apple_id
 * @property string $applm_apple_date
 * @property string $applm_number_upk
 * @property string $applm_appls_date
 * @property string $applm_applsx_date
 * @property string $applm_appls0_date
 * @property string $applm_appls0x_date
 * @property string $applm_agr_number
 * @property string $applm_agr_date
 * @property string $applm_applxxx_date
 * @property string $applm_create_user
 * @property string $applm_create_time
 * @property string $applm_create_ip
 * @property string $applm_update_user
 * @property string $applm_update_time
 * @property string $applm_update_ip
 *
 * @property Svc $applmSvc
 * @property SvcDocType $applmSvdt
 * @property TrainingProg $applmTrp
 * @property Person $applmPrs
 * @property Ab $applmAb
 * @property Company $applmComp
 * @property TrainingType $applmTrt
 * @property ApplRequestContent $applmApplrc
 */
class ApplMain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_main}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applm_reestr', 'applm_applrc_id', 'applm_prs_id', 'applm_svc_id', 'applm_trp_id', 'applm_ab_id', 'applm_comp_id', 'applm_trt_id', 'applm_svdt_id', 'applm_applr_id', 'applm_applcmd_id', 'applm_apple_id'], 'integer'],
            [['applm_prs_id'], 'required'],
            [['applm_applr_date', 'applm_applcmd_date', 'applm_date_upk', 'applm_date_start', 'applm_apple_date', 'applm_appls_date', 'applm_applsx_date', 'applm_appls0_date', 'applm_appls0x_date', 'applm_agr_date', 'applm_applxxx_date', 'applm_create_time', 'applm_update_time'], 'safe'],
            [['applm_position', 'applm_number_upk', 'applm_agr_number', 'applm_create_user', 'applm_create_ip', 'applm_update_user', 'applm_update_ip'], 'string', 'max' => 64],

            [['applm_position', 'applm_number_upk', 'applm_create_user', 'applm_create_ip', 'applm_update_user', 'applm_update_ip'], 'string', 'max' => 64],
            [['applm_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['applm_svc_id' => 'svc_id']],
            [['applm_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['applm_prs_id' => 'prs_id']],
            [['applm_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['applm_ab_id' => 'ab_id']],
            [['applm_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['applm_comp_id' => 'comp_id']],
            [['applm_trt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applm_trt_id' => 'trt_id']],
            [['applm_svdt_id'], 'exist', 'skipOnError' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['applm_svdt_id' => 'svdt_id']],
            [['applm_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['applm_trp_id' => 'trp_id']],
            [['applm_applrc_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplRequestContent::class, 'targetAttribute' => ['applm_applrc_id' => 'applrc_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applm_id' => Yii::t('app', 'Applm ID'),
            'applm_reestr' => Yii::t('app', 'Applm Reestr'),
            'applm_applrc_id' => Yii::t('app', 'Applm Applrc ID'),
            'applm_prs_id' => Yii::t('app', 'Applm Prs ID'),
            'applm_position' => Yii::t('app', 'Applm Position'),
            'applm_svc_id' => Yii::t('app', 'Applm Svc ID'),
            'applm_trp_id' => Yii::t('app', 'Applm Trp ID'),
            'applm_ab_id' => Yii::t('app', 'Applm Ab ID'),
            'applm_comp_id' => Yii::t('app', 'Applm Comp ID'),
            'applm_trt_id' => Yii::t('app', 'Applm Trt ID'),
            'applm_svdt_id' => Yii::t('app', 'Applm Svdt ID'),
            'applm_applr_id' => Yii::t('app', 'Applm Applr ID'),
            'applm_applr_date' => Yii::t('app', 'Applm Applr Date'),
            'applm_applcmd_id' => Yii::t('app', 'Applm Applcmd ID'),
            'applm_applcmd_date' => Yii::t('app', 'Applm Applcmd Date'),
            'applm_date_upk' => Yii::t('app', 'Applm Date Upk'),
            'applm_date_start' => Yii::t('app', 'Applm Date Start'),
            'applm_apple_id' => Yii::t('app', 'Applm Apple ID'),
            'applm_apple_date' => Yii::t('app', 'Applm Apple Date'),
            'applm_number_upk' => Yii::t('app', 'Applm Number Upk'),
            'applm_appls_date' => Yii::t('app', 'Applm Appls Date'),
            'applm_applsx_date' => Yii::t('app', 'Applm Applsx Date'),
            'applm_appls0_date' => Yii::t('app', 'Applm Appls0 Date'),
            'applm_appls0x_date' => Yii::t('app', 'Applm Appls0x Date'),
            'applm_agr_number' => Yii::t('app', 'Applm Agr Number'),
            'applm_agr_date' => Yii::t('app', 'Applm Agr Date'),
            'applm_applxxx_date' => Yii::t('app', 'Applm Applxxx Date'),
            'applm_create_user' => Yii::t('app', 'Applm Create User'),
            'applm_create_time' => Yii::t('app', 'Applm Create Time'),
            'applm_create_ip' => Yii::t('app', 'Applm Create Ip'),
            'applm_update_user' => Yii::t('app', 'Applm Update User'),
            'applm_update_time' => Yii::t('app', 'Applm Update Time'),
            'applm_update_ip' => Yii::t('app', 'Applm Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'applm_svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'applm_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmApplrc()
    {
        return $this->hasOne(ApplRequestContent::class, ['applrc_id' => 'applm_applrc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applm_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'applm_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applm_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'applm_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplmTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applm_trt_id']);
    }

    /**
     * @inheritdoc
     * @return ApplMainQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplMainQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applm_create_user = Yii::$app->user->identity->username;
            $this->applm_create_ip = Yii::$app->request->userIP;
            $this->applm_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applm_update_user = Yii::$app->user->identity->username;
            $this->applm_update_ip = Yii::$app->request->userIP;
            $this->applm_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
