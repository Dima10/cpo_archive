<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "{{%appl_request}}".
 *
 * Заявка на обучение
 *
 * @property integer $applr_id
 * @property integer $applr_reestr
 * @property integer $applr_id_day
 * @property string $applr_number
 * @property string $applr_date
 * @property integer $applr_ab_id
 * @property integer $applr_comp_id

 * @property integer $applr_pat_id
 * @property string $applr_file_name
 * @property resource $applr_file_data
 * @property integer $applr_agra_id
 * @property string $applr_appls_date
 * @property string $applr_apple_date
 * @property string $applr_applsx_date
 * @property string $applr_applc_date
 * @property integer $applr_trt_id
 * @property integer $applr_ast_id
 * @property integer $applr_flag

 * @property string $applr_create_user
 * @property string $applr_create_time
 * @property string $applr_create_ip
 * @property string $applr_update_user
 * @property string $applr_update_time
 * @property string $applr_update_ip
 *
 * @property Ab $applrAb
 * @property Company $applrComp
 * @property TrainingType $applrTrt
 * @property AgreementStatus $applrAst
 * @property AgreementAnnex $applrAgra
 * @property ApplRequestContent[] $applRequestContents
 * @property Pattern $applrPat
 * @property ApplSh[] $applShes
 * @property ApplSheet[] $applSheets
 *
 */
class ApplRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applr_date', 'applr_ab_id', 'applr_comp_id', 'applr_pat_id', 'applr_reestr'], 'required'],

            [['applr_file_name'], 'string', 'max' => 256],
            [['applr_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx'],


            [['applr_date', 'applr_appls_date', 'applr_apple_date', 'applr_applsx_date', 'applr_applc_date', 'applr_create_time', 'applr_update_time'], 'safe'],
            [['applr_reestr', 'applr_id_day', 'applr_ab_id', 'applr_comp_id', 'applr_agra_id', 'applr_trt_id', 'applr_ast_id', 'applr_flag'], 'integer'],

            [['applr_number'], 'unique'],

            [['applr_number', 'applr_create_user', 'applr_create_ip', 'applr_update_user', 'applr_update_ip'], 'string', 'max' => 64],

            [['applr_appls_date', 'applr_apple_date', 'applr_applsx_date', 'applr_applc_date'], 'string'],


            [['applr_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['applr_ab_id' => 'ab_id']],
            [['applr_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['applr_comp_id' => 'comp_id']],
            //[['applr_agra_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => AgreementAnnex::class, 'targetAttribute' => ['applr_agra_id' => 'agra_id']],
            [['applr_trt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applr_trt_id' => 'trt_id']],
            [['applr_ast_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementStatus::class, 'targetAttribute' => ['applr_ast_id' => 'ast_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applr_id' => Yii::t('app', 'Applr ID'),
            'applr_reestr' => Yii::t('app', 'Applr Reestr'),
            'applr_number' => Yii::t('app', 'Applr Number'),
            'applr_date' => Yii::t('app', 'Applr Date'),
            'applr_ab_id' => Yii::t('app', 'Applr Ab ID'),
            'applr_comp_id' => Yii::t('app', 'Applr Comp ID'),

            'applr_pat_id' => Yii::t('app', 'Applr Pat ID'),
            'applr_file_name' => Yii::t('app', 'Applr File Name'),
            'applr_file_data' => Yii::t('app', 'Applr File Data'),

            'applr_agra_id' => Yii::t('app', 'Applr Agra ID'),
            'applr_appls_date' => Yii::t('app', 'Applr Appls Date'),
            'applr_apple_date' => Yii::t('app', 'Applr Apple Date'),
            'applr_applsx_date' => Yii::t('app', 'Applr Applsx Date'),
            'applr_applc_date' => Yii::t('app', 'Applr Applc Date'),
            'applr_trt_id' => Yii::t('app', 'Applr Trt ID'),
            'applr_ast_id' => Yii::t('app', 'Applr Ast ID'),
            'applr_flag' => Yii::t('app', 'Applr Flag'),
            'applr_create_user' => Yii::t('app', 'Applr Create User'),
            'applr_create_time' => Yii::t('app', 'Applr Create Time'),
            'applr_create_ip' => Yii::t('app', 'Applr Create Ip'),
            'applr_update_user' => Yii::t('app', 'Applr Update User'),
            'applr_update_time' => Yii::t('app', 'Applr Update Time'),
            'applr_update_ip' => Yii::t('app', 'Applr Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applr_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrAst()
    {
        return $this->hasOne(AgreementStatus::class, ['ast_id' => 'applr_ast_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applr_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applr_pat_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'applr_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrAgra()
    {
        return $this->hasOne(AgreementAnnex::class, ['agra_id' => 'applr_agra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplRequestContents()
    {
        return $this->hasMany(ApplRequestContent::class, ['applrc_applr_id' => 'applr_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplShes()
    {
        return $this->hasMany(ApplSh::class, ['appls_applr_id' => 'applr_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplSheets()
    {
        return $this->hasMany(ApplSheet::class, ['appls_applr_id' => 'applr_id']);
    }

    /**
     * @inheritdoc
     * @return ApplRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplRequestQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($this->applr_agra_id == 0) {
            $this->applr_agra_id = null;
        };

        if ($insert) {
            $this->applr_create_user = Yii::$app->user->identity->username;
            $this->applr_create_ip = Yii::$app->request->userIP;
            $this->applr_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applr_update_user = Yii::$app->user->identity->username;
            $this->applr_update_ip = Yii::$app->request->userIP;
            $this->applr_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($st = AgreementStatus::find()->where(['ast_id' => $this->applr_ast_id])->andWhere(['like', 'ast_sys', 'M'])->one()) {

            $this->updateMain(true);

            foreach ($this->applRequestContents as $content) {
                foreach ($content->applrcPrs->contacts as $contact) {
                    if ($contact->conCont->cont_type==1) {
                        UserMail::sendMail(1, $contact->con_text,
                            [
                                'link' => $content->applrcPrs->prs_connect_link,
                                'login' => $content->applrcPrs->prs_connect_user,
                                'password' => $content->applrcPrs->prs_connect_pwd,
                                'programma' => $content->applrcTrp->trp_name,
                                ':personal' => $this->applr_flag,
                                ':reestr' => $this->applr_reestr,
                            ]);
                    }
                }
            }

        } else {
            $this->updateMain(false);
        }
    }

    /**
     * @inheritdoc
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->updateMain(false);
            ApplRequestContent::deleteAll(['applrc_applr_id' => $this->applr_id]);
            return true;
        }
        return false;
    }


    /**
     * create|delete ApplMain
     * @param boolean $flag
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function updateMain($flag) {
        if ($this->applr_flag == 0) return;

            $cont = ApplRequestContent::find()->where(['applrc_applr_id' => $this->applr_id])->all();
        if ($flag) {
            foreach ($cont as $key => $value) {
                if (!$obj = ApplMain::find()->where(['applm_applrc_id' => $value->applrc_id])->one()) {
                    $obj = new ApplMain();
                }
                $obj->applm_applrc_id = $value->applrc_id;
                $obj->applm_ab_id = $this->applr_ab_id;
                $obj->applm_comp_id = $this->applr_comp_id;
                $obj->applm_prs_id = $value->applrc_prs_id;
                $obj->applm_position = $value->applrc_position;
                $obj->applm_svc_id = $value->applrc_svc_id;
                $obj->applm_trp_id = $value->applrc_trp_id;
                $obj->applm_trt_id = $this->applr_trt_id;
                $obj->applm_svdt_id = $value->applrcSvc->svc_svdt_id;


                $obj->applm_date_upk = Calendar::workDate($value->applrc_date_upk);
                if ($this->applr_flag) {
                    $obj->applm_number_upk =
                        'ПК' .
                        \DateTime::createFromFormat('Y-m-d', $obj->applm_date_upk)->format('y/m') .
                        '-' .
                        substr(uniqid(), 0, 5); // ПКмм/гг-ХХХХХ
                }

                $obj->applm_apple_date = $obj->applm_date_upk;

                // • Дата приказа о зачислении = Дата итогового документа – Рабочие дни
                // { Округление вверх до целого { (Кол-во часов Программы / 6 ) * Случайное число { от 1 до 1,2 } } }
                $prog = TrainingProg::findOne($value->applrc_trp_id);
                $days = $prog->trp_hour/6.00;
                $days = round(empty($days) ? 1 : $days * rand(1, 1.2));
                $obj->applm_applcmd_date = Calendar::workDatePeriod($obj->applm_date_upk, $days, false, false);

                // • Дата промежуточного тестирования = Сдвиг вперед на ближайший рабочий день { округление вверх до целого { ( Дата итогового документа + Дата приказа о зачислении ) / 2 } }

                $days = round(date_diff(\DateTime::createFromFormat('Y-m-d', $obj->applm_date_upk), \DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date))->days / 2);
                $obj->applm_appls0_date = Calendar::workDate(\DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date)->modify("+$days day")->format('Y-m-d'));
                // • Дата ведомости промежуточного тестирования = Дата промежуточного тестирования
                $obj->applm_appls0x_date = $obj->applm_appls0_date;


                $obj->applm_appls_date = $obj->applm_date_upk;
                $obj->applm_applsx_date = $obj->applm_appls_date;


                $obj->applm_applr_date = Calendar::workDate(\DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date)->modify('-3 day')->format('Y-m-d'));

                if (!$obj->save()) {
                    throw new Exception(json_encode($obj->errors, JSON_UNESCAPED_UNICODE));
                }
                //Yii::debug(json_encode($obj->errors, JSON_UNESCAPED_UNICODE));
            }
        } else {
            foreach ($cont as $key => $value) {
                $objs = ApplMain::find()
                    ->where([
                        'applm_applrc_id' => $value->applrc_id
                        ])->all();
                foreach ($objs as $k => $v) {
                    $v->delete();
                }
            }
        }
    }
}
