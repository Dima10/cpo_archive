<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_module_person_mail}}".
 *
 * @property integer $tmpm_id
 * @property integer $tmpm_prs_id
 * @property integer $tmpm_trm_id
 * @property string $tmpm_create_user
 * @property string $tmpm_create_time
 * @property string $tmpm_create_ip
 *
 * @property TrainingModule $tmpmTrm
 * @property Person $tmpmPrs
 */
class TrainingModulePersonMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_module_person_mail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tmpm_prs_id', 'tmpm_trm_id'], 'required'],
            [['tmpm_prs_id', 'tmpm_trm_id'], 'integer'],
            [['tmpm_create_time'], 'safe'],
            [['tmpm_create_user', 'tmpm_create_ip'], 'string', 'max' => 64],
            [['tmpm_trm_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingModule::class, 'targetAttribute' => ['tmpm_trm_id' => 'trm_id']],
            [['tmpm_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['tmpm_prs_id' => 'prs_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tmpm_id' => Yii::t('app', 'Tmpm ID'),
            'tmpm_prs_id' => Yii::t('app', 'Tmpm Prs ID'),
            'tmpm_trm_id' => Yii::t('app', 'Tmpm Trm ID'),
            'tmpm_create_user' => Yii::t('app', 'Tmpm Create User'),
            'tmpm_create_time' => Yii::t('app', 'Tmpm Create Time'),
            'tmpm_create_ip' => Yii::t('app', 'Tmpm Create Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmpmTrm()
    {
        return $this->hasOne(TrainingModule::class, ['trm_id' => 'tmpm_trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmpmPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'tmpm_prs_id']);
    }

    /**
     * @inheritdoc
     * @return TrainingModulePersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingModulePersonQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->tmpm_create_user = Yii::$app->user->identity->username;
            $this->tmpm_create_ip = Yii::$app->request->userIP;
            $this->tmpm_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            return true;
        }
    }
}
