<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%task_type}}".
 *
 * @property integer $taskt_id
 * @property string $taskt_name
 * @property string $taskt_note
 * @property string $taskt_create_user
 * @property string $taskt_create_time
 * @property string $taskt_create_ip
 * @property string $taskt_update_user
 * @property string $taskt_update_time
 * @property string $taskt_update_ip
 *
 * @property Task[] $tasks
 * @property TaskTmpl[] $taskTmpls
 */
class TaskType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskt_name', 'taskt_note'], 'required'],
            [['taskt_note'], 'string'],
            [['taskt_create_time', 'taskt_update_time'], 'safe'],
            [['taskt_name', 'taskt_create_user', 'taskt_create_ip', 'taskt_update_user', 'taskt_update_ip'], 'string', 'max' => 64],
            [['taskt_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskt_id' => Yii::t('app', 'Taskt ID'),
            'taskt_name' => Yii::t('app', 'Taskt Name'),
            'taskt_note' => Yii::t('app', 'Taskt Note'),
            'taskt_create_user' => Yii::t('app', 'Taskt Create User'),
            'taskt_create_time' => Yii::t('app', 'Taskt Create Time'),
            'taskt_create_ip' => Yii::t('app', 'Taskt Create Ip'),
            'taskt_update_user' => Yii::t('app', 'Taskt Update User'),
            'taskt_update_time' => Yii::t('app', 'Taskt Update Time'),
            'taskt_update_ip' => Yii::t('app', 'Taskt Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::class, ['task_taskt_id' => 'taskt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTmpls()
    {
        return $this->hasMany(TaskTmpl::class, ['tasktp_taskt_id' => 'taskt_id']);
    }

    /**
     * @inheritdoc
     * @return TaskTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->taskt_create_user = Yii::$app->user->identity->username;
            $this->taskt_create_ip = Yii::$app->request->userIP;
            $this->taskt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->taskt_update_user = Yii::$app->user->identity->username;
            $this->taskt_update_ip = Yii::$app->request->userIP;
            $this->taskt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
