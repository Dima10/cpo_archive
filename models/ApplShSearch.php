<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplSh;

/**
 * ApplShSearch represents the model behind the search form about `app\models\ApplSh`.
 */
class ApplShSearch extends ApplSh
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appls_id', 'appls_prs_id', 'appls_trp_id', 'appls_trt_id', 'appls_score_max', 'appls_score', 'appls_passed'], 'integer'],
            [['appls_magic'], 'string'],
            [['appls_number', 'appls_date', 'appls_create_user', 'appls_create_time', 'appls_create_ip', 'appls_update_user', 'appls_update_time', 'appls_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        if (!is_null($id)) {
            $query = ApplSh::find()
                ->leftJoin(Person::tableName(), 'appls_prs_id = prs_id')
                ->leftJoin(Staff::tableName(), 'stf_prs_id = prs_id')
                ->where(['stf_ent_id' => $id]);
        } else {
            $query = ApplSh::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'appls_id' => $this->appls_id,
            'appls_prs_id' => $this->appls_prs_id,
            'appls_magic' => $this->appls_magic,
            'appls_trp_id' => $this->appls_trp_id,
            'appls_score_max' => $this->appls_score_max,
            'appls_score' => $this->appls_score,
            'appls_passed' => $this->appls_passed,
        ]);

        $query
            ->andFilterWhere(['like', 'appls_date', $this->appls_date])
            ->andFilterWhere(['like', 'appls_create_time', $this->appls_create_time])
            ->andFilterWhere(['like', 'appls_update_time', $this->appls_update_time])
            ->andFilterWhere(['like', 'appls_number', $this->appls_number])
            ->andFilterWhere(['like', 'appls_create_user', $this->appls_create_user])
            ->andFilterWhere(['like', 'appls_create_time', $this->appls_create_time])
            ->andFilterWhere(['like', 'appls_create_ip', $this->appls_create_ip])
            ->andFilterWhere(['like', 'appls_update_user', $this->appls_update_user])
            ->andFilterWhere(['like', 'appls_update_time', $this->appls_update_time])
            ->andFilterWhere(['like', 'appls_update_ip', $this->appls_update_ip])
        ;

        return $dataProvider;
    }
}
